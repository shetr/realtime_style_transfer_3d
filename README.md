
# Real-Time Style Transfer to 3D Models Using Deep Convolutional Networks

This project is implementation part of my bachleor thesis at CTU FEE <https://dspace.cvut.cz/handle/10467/99219>.
It's executing style transfer on render of a 3D model with pre-trained neural network.
You can move with the camera and change the scene and see results of the style transfer.
Neural network is pretrained in PyTorch using [Few-Shot-Patch-Based-Training](https://github.com/OndrejTexler/Few-Shot-Patch-Based-Training).
The application is rendering with OpenGL abstraction from my library [Morph Engine](https://gitlab.com/shetr/morphengine)
and the evaluation of neural network is done with PyTorch C++ API and Cuda-OpenGL interop. There is also GLSL implementation of the neural network evaluation, but it was abandoned at some point.

Here is a little demonstration on my GTX 950M. The response of style transfer evaluation is not ideal (0.5 s). With reduction of neural network architecture we can achive 10 FPS, but it's possible that on some modern powerful graphics card the application would be able to evaluate it every frame.

![Demo](demo.gif)


## Clone

Clone project with all submodules:

```bash
git clone --recursive --depth=1 https://gitlab.com/shetr/realtime_style_transfer_3d.git
```

Or update submodules after clone:

```bash
git clone --depth=1 https://gitlab.com/shetr/realtime_style_transfer_3d.git
```

### All

```bash
git submodule update --init --recursive
```

### Selectively for Windows

```bash
git submodule update --init scripts
git submodule update --init --recursive extern/protobuf
git submodule update --init --recursive extern/morphengine
git submodule update --init --recursive extern/torch_cuda_cpp_windows
```

### Selectively for Linux

```bash
git submodule update --init scripts
git submodule update --init --recursive extern/protobuf
git submodule update --init --recursive extern/morphengine
git submodule update --init --recursive extern/torch_cuda_cpp_linux
```

## Build

### Configure cmake project

```bash
# debug config
cmake -S . -B build/Debug -DCMAKE_BUILD_TYPE=Debug
# release config
cmake -S . -B build/Release -DCMAKE_BUILD_TYPE=Release
# deploy config
cmake -S . -B build/Deploy -DCMAKE_BUILD_TYPE=Release -DENABLE_DEPLOY=true
# profile debug config
cmake -S . -B build/ProfileDebug -DCMAKE_BUILD_TYPE=Debug -DENABLE_PROFILING=true
# profile release config
cmake -S . -B build/ProfileRelease -DCMAKE_BUILD_TYPE=Release -DENABLE_PROFILING=true
```

### Build cmake project

For fast build is recommended adding `-j N` to end, where N is number of threads.

```bash
# debug build all
cmake --build build/Debug
# release build all
cmake --build build/Release
# deploy build all
cmake --build build/Deploy
# profile debug build all
cmake --build build/ProfileDebug
# profile release build all
cmake --build build/ProfileRelease


# debug build specific target
cmake --build build/Debug --target your_target
# release build specific target
cmake --build build/Release --target your_target
# deploy build specific target
cmake --build build/Deploy --target your_target
# profile debug build specific target
cmake --build build/ProfileDebug --target your_target
# profile release build specific target
cmake --build build/ProfileRelease --target your_target
```
