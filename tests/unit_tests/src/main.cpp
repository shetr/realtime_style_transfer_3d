
#include <gtest/gtest.h>

#include "GlobalState.hpp"

int main(int argc, char **argv) {
    TestsGlobalState::Init();
    ::testing::InitGoogleTest(&argc, argv);
    int tests_res = RUN_ALL_TESTS();
    TestsGlobalState::Release();
    return tests_res;
}