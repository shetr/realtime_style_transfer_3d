#include <gtest/gtest.h>
#include <NeuralNet/Layers/Add.hpp>
#include <GlobalState.hpp>
#include <TestingUtils.hpp>

using namespace Morph;

TEST(LayersAdd, basicData) {
    style_transfer::Add add;
    add.set_id1(0);
    add.set_id2(0);
    
    AddProgram addProgram(TestsGlobalState::neuralNetResources(), add);

    array<float, 8> inputValues1 = {
        1, 2,
        3, 4,

        5, 6,
        7, 8
    };
    array<float, 8> inputValues2 = {
         9, 10,
        11, 12,

        13, 14,
        15, 16
    };

    Texture3D inputData1 = TestsUtils::CreateTexture3D(uvec3(2), inputValues1.data());
    Texture3D inputData2 = TestsUtils::CreateTexture3D(uvec3(2), inputValues2.data());
    Texture3D outputData = TestsUtils::CreateTexture3D(uvec3(2));

    addProgram.Evaluate(TestsGlobalState::context(), inputData1, inputData2, outputData);

    array<float, 8> expectedOutputValues = {
        10, 12,
        14, 16,

        18, 20,
        22, 24
    };
    array<float, 8> actualOutputValues;
    outputData.GetData(actualOutputValues.data());

    ASSERT_PRED3(TestsUtils::CompareFloatsN<8>, actualOutputValues, expectedOutputValues, TestsUtils::precision());
}