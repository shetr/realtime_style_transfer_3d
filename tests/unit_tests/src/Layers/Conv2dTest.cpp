#include <gtest/gtest.h>
#include <NeuralNet/Layers/Conv2d.hpp>
#include <NeuralNet/Utils.hpp>
#include <GlobalState.hpp>
#include <TestingUtils.hpp>

#include <iostream>
#include <spdlog/spdlog.h>
#include <Morph.hpp>

using namespace Morph;
using namespace style_transfer;
using namespace magic_enum::ostream_operators;

vector3d<float> ComputeConv2dGPU(const Conv2d& conv2d, const vector3d<float>& inputValues)
{
    Conv2dProgram conv2dProgram(TestsGlobalState::context(), TestsGlobalState::neuralNetResources(), conv2d, false);

    uvec3 outputDim =  uvec3(inputValues.dim().x / conv2d.stride_x(), inputValues.dim().y / conv2d.stride_y(), conv2d.out_channels());
    Texture3D inputData = TestsUtils::CreateTexture3D(inputValues.dim(), inputValues.data());
    Texture3D outputData = TestsUtils::CreateTexture3D(outputDim);

    conv2dProgram.Evaluate(TestsGlobalState::context(), inputData, outputData);

    vector3d<float> outputValues(outputDim);
    outputData.GetData(outputValues.data());
    return outputValues;
}

void TestConv2d(const Conv2d& conv2d, const vector3d<float>& inputValues)
{
    vector3d<float> expectedOutputValues = ComputeConv2d(conv2d, inputValues);
    vector3d<float> actualOutputValues = ComputeConv2dGPU(conv2d, inputValues);

    EXPECT_EQ(actualOutputValues.dim(), expectedOutputValues.dim()) <<
        " in_ch:" << conv2d.in_channels() << ", out_ch:" << conv2d.out_channels() << 
        ", kernel:" << conv2d.kernel_size_x() << ", stride" << conv2d.stride_x();
    EXPECT_PRED3(TestsUtils::CompareFloats, actualOutputValues.flattend(), expectedOutputValues.flattend(), TestsUtils::precision()) <<
        " in_ch:" << conv2d.in_channels() << ", out_ch:" << conv2d.out_channels() << 
        ", kernel:" << conv2d.kernel_size_x() << ", stride" << conv2d.stride_x();
}

void TestRandomConv2D(Conv2d& conv2d, vector3d<float>& inputValues, int testCount)
{
    google::protobuf::RepeatedField<float>& conv_weights = *conv2d.mutable_weights();
    google::protobuf::RepeatedField<float>& conv_biases = *conv2d.mutable_biases();

    for(int t = 0; t < testCount; ++t) {
        TestsUtils::GenRandFloats(conv_weights.begin(), conv_weights.end());
        TestsUtils::GenRandFloats(conv_biases.begin(), conv_biases.end());
        TestsUtils::GenRandFloats(inputValues.begin(), inputValues.end());
        TestConv2d(conv2d, inputValues);
    }
}

TEST(LayersConv2d, basicData) {
    Conv2d conv2d = TestsUtils::CretateUniformConv2d(2, 4, uvec2(3), uvec2(2), true, 1.0f / 27.0f, 0.05f);

    vector3d<float> inputValues(uvec3(4, 4, 2));
    for(int i = 0; i < inputValues.size(); i++){
        inputValues[i] = (float)i / 8.0f;
    }
    
    TestConv2d(conv2d, inputValues);
}

TEST(LayersConv2d, randomData_i1to4_o1to4_s1to2_k1to5) {
    for(uint in_channels = 1; in_channels <= 4; ++in_channels) {
        for(uint out_channels = 1; out_channels <= 4; ++ out_channels) {
            for(uint stride = 1; stride <= 2; ++stride) {
                for(uint kernel_size = 1; kernel_size <= 5; kernel_size += 2) {
                    Conv2d conv2d = TestsUtils::CretateUniformConv2d(in_channels, out_channels, uvec2(kernel_size), uvec2(stride), true, 0, 0);
                    vector3d<float> inputValues(uvec3(16, 16, conv2d.in_channels()), 0);
                    TestRandomConv2D(conv2d, inputValues, 10);
                }
            }
        }
    }
}

TEST(LayersConv2d, randomData_i32to64_o32to64_s1to2_k1to5) {
    for(uint in_channels = 32; in_channels <= 64; in_channels*=2) {
        for(uint out_channels = 32; out_channels <= 64; out_channels*=2) {
            for(uint stride = 1; stride <= 2; ++stride) {
                for(uint kernel_size = 1; kernel_size <= 5; kernel_size += 2) {
                    Conv2d conv2d = TestsUtils::CretateUniformConv2d(in_channels, out_channels, uvec2(kernel_size), uvec2(stride), true, 0, 0);
                    vector3d<float> inputValues(uvec3(4, 4, conv2d.in_channels()), 0);
                    TestRandomConv2D(conv2d, inputValues, 10);
                }
            }
        }
    }
}


TEST(LayersConv2d, basicData_i1_o1_s1_k1) {
    Conv2d conv2d = TestsUtils::CretateUniformConv2d(1, 1, uvec2(1), uvec2(1), true, 2, 1);
    vector3d<float> inputValues(uvec3(2, 2, 1), {0, 1, 2, 3});
    vector3d<float> expectedValues(uvec3(2, 2, 1), {1, 3, 5, 7});
    vector3d<float> outputValues = ComputeConv2dGPU(conv2d, inputValues);
    EXPECT_EQ(outputValues.dim(), uvec3(2, 2, 1));
    EXPECT_PRED3(TestsUtils::CompareFloats, outputValues.flattend(), expectedValues.flattend(), TestsUtils::precision());
}

TEST(LayersConv2d, basicData_i1_o1_s2_k1) {
    Conv2d conv2d = TestsUtils::CretateUniformConv2d(1, 1, uvec2(1), uvec2(2), true, 2, 1);
    vector3d<float> inputValues(uvec3(4, 2, 1), {0, 1, 2, 3, 4, 5, 6, 7});
    vector3d<float> expectedValues(uvec3(2, 1, 1), {1, 5});
    vector3d<float> outputValues = ComputeConv2dGPU(conv2d, inputValues);
    EXPECT_EQ(outputValues.dim(), uvec3(2, 1, 1));
    EXPECT_PRED3(TestsUtils::CompareFloats, outputValues.flattend(), expectedValues.flattend(), TestsUtils::precision());
}

TEST(LayersConv2d, basicData_i1_o2_s1_k1) {
    Conv2d conv2d = TestsUtils::CretateConv2d(1, 2, uvec2(1), {1, 2}, {1, 0});
    vector3d<float> inputValues(uvec3(2, 2, 1), {0, 1, 2, 3});
    vector3d<float> expectedValues(uvec3(2, 2, 2), {1, 2, 3, 4, 0, 2, 4, 6});
    vector3d<float> outputValues = ComputeConv2dGPU(conv2d, inputValues);
    EXPECT_EQ(outputValues.dim(), uvec3(2, 2, 2));
    EXPECT_PRED3(TestsUtils::CompareFloats, outputValues.flattend(), expectedValues.flattend(), TestsUtils::precision());
}

TEST(LayersConv2d, basicData_i2_o1_s1_k1) {
    Conv2d conv2d = TestsUtils::CretateConv2d(2, 1, uvec2(1), {2, 1}, {1});
    vector3d<float> inputValues(uvec3(2, 2, 2), {0, 1, 2, 3, 4, 5, 6, 7});
    vector3d<float> expectedValues(uvec3(2, 2, 1), {5, 8, 11, 14});
    vector3d<float> outputValues = ComputeConv2dGPU(conv2d, inputValues);
    EXPECT_EQ(outputValues.dim(), uvec3(2, 2, 1));
    EXPECT_PRED3(TestsUtils::CompareFloats, outputValues.flattend(), expectedValues.flattend(), TestsUtils::precision());
}

TEST(LayersConv2d, basicData_i2_o2_s1_k1) {
    Conv2d conv2d = TestsUtils::CretateConv2d(2, 2, uvec2(1), {1, 2, 2, 1}, {0, 1});
    vector3d<float> inputValues(uvec3(2, 2, 2), {0, 1, 2, 3, 4, 5, 6, 7});
    vector3d<float> expectedValues(uvec3(2, 2, 2), {8, 11, 14, 17, 5, 8, 11, 14});
    vector3d<float> outputValues = ComputeConv2dGPU(conv2d, inputValues);
    EXPECT_EQ(outputValues.dim(), uvec3(2, 2, 2));
    EXPECT_PRED3(TestsUtils::CompareFloats, outputValues.flattend(), expectedValues.flattend(), TestsUtils::precision());
}

TEST(LayersConv2d, basicData_i2_o2_s2_k1) {
    Conv2d conv2d = TestsUtils::CretateConv2d(2, 2, uvec2(1), {1, 2, 2, 1}, {0, 1}, uvec2(2));
    vector3d<float> inputValues(uvec3(2, 2, 2), {0, 1, 2, 3, 4, 5, 6, 7});
    vector3d<float> expectedValues(uvec3(1, 1, 2), {8, 5});
    vector3d<float> outputValues = ComputeConv2dGPU(conv2d, inputValues);
    EXPECT_EQ(outputValues.dim(), uvec3(1, 1, 2));
    EXPECT_PRED3(TestsUtils::CompareFloats, outputValues.flattend(), expectedValues.flattend(), TestsUtils::precision());
}

TEST(LayersConv2d, basicData_i1_o1_s1_k3) {
    Conv2d conv2d = TestsUtils::CretateConv2d(1, 1, uvec2(3), 
        {0, 1, 2, 
         3, 4, 5, 
         6, 7, 8}, 
        {1}
    );
    vector3d<float> inputValues(uvec3(2, 2, 1), 
        {0, 1, 
         2, 3}
    );
    vector3d<float> expectedValues(uvec3(2, 2, 1), 
        {44, 38, 
         26, 20}
    );
    vector3d<float> outputValues = ComputeConv2dGPU(conv2d, inputValues);
    EXPECT_EQ(outputValues.dim(), uvec3(2, 2, 1));
    EXPECT_PRED3(TestsUtils::CompareFloats, outputValues.flattend(), expectedValues.flattend(), TestsUtils::precision());
}

TEST(LayersConv2d, basicData_i1_o2_s1_k3) {
    Conv2d conv2d = TestsUtils::CretateConv2d(1, 2, uvec2(3), 
        { 0,  1,  2, 
          3,  4,  5, 
          6,  7,  8,

          0, -1, -2, 
         -3, -4, -5, 
         -6, -7, -8}, 
        {1, -1}
    );
    vector3d<float> inputValues(uvec3(2, 2, 1), 
        {0, 1, 
         2, 3}
    );
    vector3d<float> expectedValues(uvec3(2, 2, 2), 
        { 44,  38, 
          26,  20,
         
         -44, -38, 
         -26, -20}
    );
    vector3d<float> outputValues = ComputeConv2dGPU(conv2d, inputValues);
    EXPECT_EQ(outputValues.dim(), uvec3(2, 2, 2));
    EXPECT_PRED3(TestsUtils::CompareFloats, outputValues.flattend(), expectedValues.flattend(), TestsUtils::precision());
}

TEST(LayersConv2d, basicData_i2_o1_s1_k3) {
    Conv2d conv2d = TestsUtils::CretateConv2d(2, 1, uvec2(3), 
        {9, 10, 11,
        12, 13, 14,
        15, 16, 17,
        
         0, 1, 2, 
         3, 4, 5, 
         6, 7, 8}, 
        {1}
    );
    vector3d<float> inputValues(uvec3(2, 2, 2), 
        {0, 1, 
         2, 3,
         
         4, 5,
         6, 7}
    );
    vector3d<float> expectedValues(uvec3(2, 2, 1), 
        {13*0+14*1+16*2+17*3 + 4*4+5*5+7*6+8*7 + 1, 12*0+13*1+15*2+16*3 + 3*4+4*5+6*6+7*7 + 1, 
         10*0+11*1+13*2+14*3 + 1*4+2*5+4*6+5*7 + 1,  9*0+10*1+12*2+13*3 + 0*4+1*5+3*6+4*7 + 1}
    );
    vector3d<float> outputValues = ComputeConv2dGPU(conv2d, inputValues);
    EXPECT_EQ(outputValues.dim(), uvec3(2, 2, 1));
    EXPECT_PRED3(TestsUtils::CompareFloats, outputValues.flattend(), expectedValues.flattend(), TestsUtils::precision());
}

TEST(LayersConv2d, basicData_i2_o2_s1_k3) {
    Conv2d conv2d = TestsUtils::CretateConv2d(2, 2, uvec2(3), 
        {9, 10, 11,
        12, 13, 14,
        15, 16, 17,
        
         0, 1, 2, 
         3, 4, 5, 
         6, 7, 8,
         
         -9, -10, -11,
        -12, -13, -14,
        -15, -16, -17,
        
          0, -1, -2, 
         -3, -4, -5, 
         -6, -7, -8}, 
        {1, 2}
    );
    vector3d<float> inputValues(uvec3(2, 2, 2), 
        {0, 1, 
         2, 3,
         
         4, 5,
         6, 7}
    );
    vector3d<float> expectedValues(uvec3(2, 2, 2), 
        {13*0+14*1+16*2+17*3 + 4*4+5*5+7*6+8*7 + 1, 12*0+13*1+15*2+16*3 + 3*4+4*5+6*6+7*7 + 1, 
         10*0+11*1+13*2+14*3 + 1*4+2*5+4*6+5*7 + 1,  9*0+10*1+12*2+13*3 + 0*4+1*5+3*6+4*7 + 1,

         -(13*0+14*1+16*2+17*3 + 4*4+5*5+7*6+8*7) + 2, -(12*0+13*1+15*2+16*3 + 3*4+4*5+6*6+7*7) + 2, 
         -(10*0+11*1+13*2+14*3 + 1*4+2*5+4*6+5*7) + 2, -( 9*0+10*1+12*2+13*3 + 0*4+1*5+3*6+4*7) + 2}
    );
    vector3d<float> outputValues = ComputeConv2dGPU(conv2d, inputValues);
    EXPECT_EQ(outputValues.dim(), uvec3(2, 2, 2));
    EXPECT_PRED3(TestsUtils::CompareFloats, outputValues.flattend(), expectedValues.flattend(), TestsUtils::precision());
}

TEST(LayersConv2d, basicData_i1_o1_s2_k3) {
    Conv2d conv2d = TestsUtils::CretateConv2d(1, 1, uvec2(3), 
        {0, 1, 2, 
         3, 4, 5, 
         6, 7, 8}, 
        {1},
        uvec2(2)
    );
    vector3d<float> inputValues(uvec3(4, 4, 1), 
        { 0,  1,  2,  3,
          4,  5,  6,  7,
          8,  9, 10, 11,
         12, 13, 14, 15}
    );
    vector3d<float> expectedValues(uvec3(2, 2, 1), 
        {4*0+5*1+7*4+8*5 + 1, 3*1+4*2+5*3+6*5+7*6+8*7 + 1, 
         1*4+2*5+4*8+5*9+7*12+8*13 + 1, 0*5+1*6+2*7+3*9+4*10+5*11+6*13+7*14+8*15 + 1}
    );
    vector3d<float> outputValues = ComputeConv2dGPU(conv2d, inputValues);
    EXPECT_EQ(outputValues.dim(), uvec3(2, 2, 1));
    EXPECT_PRED3(TestsUtils::CompareFloats, outputValues.flattend(), expectedValues.flattend(), TestsUtils::precision());
}

TEST(LayersConv2d, basicData_i1_o2_s2_k3) {
    Conv2d conv2d = TestsUtils::CretateConv2d(1, 2, uvec2(3), 
        { 0,  1,  2, 
          3,  4,  5, 
          6,  7,  8,

          0, -1, -2, 
         -3, -4, -5, 
         -6, -7, -8}, 
        {1, -1},
        uvec2(2)
    );
    vector3d<float> inputValues(uvec3(4, 4, 1), 
        { 0,  1,  2,  3,
          4,  5,  6,  7,
          8,  9, 10, 11,
         12, 13, 14, 15}
    );
    vector3d<float> expectedValues(uvec3(2, 2, 2), 
        {4*0+5*1+7*4+8*5 + 1, 3*1+4*2+5*3+6*5+7*6+8*7 + 1, 
         1*4+2*5+4*8+5*9+7*12+8*13 + 1, 0*5+1*6+2*7+3*9+4*10+5*11+6*13+7*14+8*15 + 1,
         
         -(4*0+5*1+7*4+8*5 + 1), -(3*1+4*2+5*3+6*5+7*6+8*7 + 1), 
         -(1*4+2*5+4*8+5*9+7*12+8*13 + 1), -(0*5+1*6+2*7+3*9+4*10+5*11+6*13+7*14+8*15 + 1)}
    );
    vector3d<float> outputValues = ComputeConv2dGPU(conv2d, inputValues);
    EXPECT_EQ(outputValues.dim(), uvec3(2, 2, 2));
    EXPECT_PRED3(TestsUtils::CompareFloats, outputValues.flattend(), expectedValues.flattend(), TestsUtils::precision());
}

TEST(LayersConv2d, basicData_i2_o1_s2_k3) {
    Conv2d conv2d = TestsUtils::CretateConv2d(2, 1, uvec2(3), 
        {9, 10, 11,
        12, 13, 14,
        15, 16, 17,
        
         0, 1, 2, 
         3, 4, 5, 
         6, 7, 8}, 
        {1},
        uvec2(2)
    );
    vector3d<float> inputValues(uvec3(4, 4, 2), 
        { 0,  1,  2,  3,
          4,  5,  6,  7,
          8,  9, 10, 11,
         12, 13, 14, 15,
         
         16, 17, 18, 19,
         20, 21, 22, 23,
         24, 25, 26, 27,
         28, 29, 30, 31}
    );
    vector3d<float> expectedValues(uvec3(2, 2, 1), 
        {13*0+14*1+16*4+17*5 + 4*16+5*17+7*20+8*21 + 1,
         12*1+13*2+14*3+15*5+16*6+17*7 + 3*17+4*18+5*19+6*21+7*22+8*23 + 1, 
         10*4+11*5+13*8+14*9+16*12+17*13 + 1*20+2*21+4*24+5*25+7*28+8*29 + 1,
         9*5+10*6+11*7+12*9+13*10+14*11+15*13+16*14+17*15 + 0*21+1*22+2*23+3*25+4*26+5*27+6*29+7*30+8*31 + 1}
    );
    vector3d<float> outputValues = ComputeConv2dGPU(conv2d, inputValues);
    EXPECT_EQ(outputValues.dim(), uvec3(2, 2, 1));
    EXPECT_PRED3(TestsUtils::CompareFloats, outputValues.flattend(), expectedValues.flattend(), TestsUtils::precision());
}

TEST(LayersConv2d, basicData_i2_o2_s2_k3) {
    Conv2d conv2d = TestsUtils::CretateConv2d(2, 2, uvec2(3), 
        {9, 10, 11,
        12, 13, 14,
        15, 16, 17,
        
         0, 1, 2, 
         3, 4, 5, 
         6, 7, 8,
         
         -9, -10, -11,
        -12, -13, -14,
        -15, -16, -17,
        
          0, -1, -2, 
         -3, -4, -5, 
         -6, -7, -8},
        {1, 2},
        uvec2(2)
    );
    vector3d<float> inputValues(uvec3(4, 4, 2), 
        { 0,  1,  2,  3,
          4,  5,  6,  7,
          8,  9, 10, 11,
         12, 13, 14, 15,
         
         16, 17, 18, 19,
         20, 21, 22, 23,
         24, 25, 26, 27,
         28, 29, 30, 31}
    );
    vector3d<float> expectedValues(uvec3(2, 2, 2), 
        {13*0+14*1+16*4+17*5 + 4*16+5*17+7*20+8*21 + 1,
         12*1+13*2+14*3+15*5+16*6+17*7 + 3*17+4*18+5*19+6*21+7*22+8*23 + 1, 
         10*4+11*5+13*8+14*9+16*12+17*13 + 1*20+2*21+4*24+5*25+7*28+8*29 + 1,
         9*5+10*6+11*7+12*9+13*10+14*11+15*13+16*14+17*15 + 0*21+1*22+2*23+3*25+4*26+5*27+6*29+7*30+8*31 + 1,
         -(13*0+14*1+16*4+17*5 + 4*16+5*17+7*20+8*21) + 2,
         -(12*1+13*2+14*3+15*5+16*6+17*7 + 3*17+4*18+5*19+6*21+7*22+8*23) + 2,
         -(10*4+11*5+13*8+14*9+16*12+17*13 + 1*20+2*21+4*24+5*25+7*28+8*29) + 2,
         -(9*5+10*6+11*7+12*9+13*10+14*11+15*13+16*14+17*15 + 0*21+1*22+2*23+3*25+4*26+5*27+6*29+7*30+8*31) + 2}
    );
    vector3d<float> outputValues = ComputeConv2dGPU(conv2d, inputValues);
    EXPECT_EQ(outputValues.dim(), uvec3(2, 2, 2));
    EXPECT_PRED3(TestsUtils::CompareFloats, outputValues.flattend(), expectedValues.flattend(), TestsUtils::precision());
}