#include <iostream>

#include <spdlog/spdlog.h>

#include <Morph.hpp>
#include <Window/Manager.hpp>
#include <Graphics/Context.hpp>
#include <Graphics/Uniforms.hpp>
#include <Resource/GraphicsProgramCompiler.hpp>
#include <Resource/Common.hpp>
#include <Resource/ResourceManager.hpp>
#include <Data/ShaderTransform.hpp>
#include <Data/Transform.hpp>
#include <Data/Camera.hpp>
#include <Data/PBR.hpp>
#include <ImGui/ImGui.hpp>
#include <Profile/GraphicsTimer.hpp>

#include <limits>

#include <NeuralNet/ModelPrinter.hpp>
#include <NeuralNet/Resources.hpp>
#include <NeuralNet/Layers.hpp>

using namespace magic_enum::ostream_operators;

namespace Morph {

struct CameraData
{
    PerspectiveCamera3D camera;
    Transform transform;
};

struct ObjectData
{
    Transform transform;
};

uint idot(uvec3 a, uvec3 b)
{
    return a.x*b.x + a.y*b.y +a.z*b.z;
}

class Application
{
private:
    unique<WindowManager> m_windowManager;
    MethodAttacher<WindowManagerError, Application> m_windowManagerErrorAttacher;
    MethodAttacher<WindowSizeEvent, Application> m_windowSizeEventAttacher;
    MethodAttacher<WindowFramebufferSizeEvent, Application> m_windowFramebufferSizeEventAttacher;
    MethodAttacher<KeyEvent, Application> m_keyEventAttacher;
    MethodAttacher<ScrollEvent, Application> m_scrollEventAttacher;
    WindowID m_windowID;
    unique<GraphicsContext> m_context;

    float m_renderTimer = 0;
    CameraData m_cameraData;
    ObjectData m_objectData;
    vec3 m_envGlobalAmbient = vec3(0.01);
    Material m_material;
    uint m_lightCount = 1;
    Light m_light;
    MaterialTex m_textures;
    bool m_showLight = true;
    float m_moveSpeed = 1;
    float m_rotSpeed = 1;

    bool m_makeScreenshot = false;
    int m_screenshotNum = 0;
    CustomFramebuffer m_sceneFramebuffer;

    Texture3D m_shaderData1;
    Texture3D m_shaderData2;
    Texture3D m_shaderData3;
    bool m_useLayer = true;
public:
    Application() {

        m_windowManager = unique<WindowManager>(WindowManager::Get());

        m_windowManagerErrorAttacher = {m_windowManager.get(), this, &Application::OnWindowManagerError};
        m_windowSizeEventAttacher = {m_windowManager.get(), this, &Application::OnWindowSizeEvent};
        m_keyEventAttacher = {m_windowManager.get(), this, &Application::OnKeyEvent};
        m_scrollEventAttacher = {m_windowManager.get(), this, &Application::OnScrollEvent};

        m_windowID = value_or_panic(m_windowManager->CreateWindow(ivec2(512,512), "layers test"), "failed to create window");
    }

    void Run() {

        Window& window = value_or_panic(m_windowManager->GetWindow(m_windowID), "window unexpectedly destroyed");
        m_context = unique<GraphicsContext>(value_or_panic(GraphicsContext::MakeCurrent(window)));
        DefaultFramebuffer& defaultFramebuffer = m_context->GetDefaultFramebuffer();
        defaultFramebuffer.SetSwapWaitInterval(0);

        GraphicsSettings graphicsSettings = {
            MultisampleEnabledSetting(true),
            DepthTestEnabledSetting(true),
            CullFaceEnabledSetting(true),
            CullFaceSetting(CullFaceType::BACK),
            FrontFaceSetting(FrontFaceType::CCW),
            ClearColorSetting(0)
        };
        GraphicsSettingsApplier graphicsSettingsApplier = m_context->ApplySettings(graphicsSettings);

        ImGuiContext imguiContext(window);

        ResourceStorage resStorage;
        resStorage.InsertResFolder("engine", MORPH_ENGINE_RES);
        resStorage.InsertResFolder("shared", MORPH_SHARED_RES);
        GraphicsProgramCompiler progCompiler(*m_context, resStorage);
        CommonResources commonResources(*m_context, resStorage, progCompiler);
        NeuralNetResources neuralNetResources(progCompiler);

        GraphicsTimer<5> renderTimer(*m_context);

        ShaderTransform shaderTransform;
        ShaderTransformUniforms shaderTransformUniforms(shaderTransform);
        raw<Light> rawLights(1, &m_light);
        PBRForwardUniforms phongForwardUniforms(m_envGlobalAmbient, m_cameraData.transform.pos, m_material, m_lightCount, rawLights);
        PBRForwardTextureUnits phongForwardTextureUnits(m_textures, commonResources.WhiteTexture2D());
        Uniform<vec3> lightColorUniform("u_color", m_light.color);

        m_cameraData.transform.pos = vec3(-0.375, 0, 0.792);
        m_cameraData.transform.rotAxis = vec3(0, 1, 0);
        m_cameraData.transform.rotAngle = -0.425;
        m_objectData.transform.scale = vec3(0.5);

        m_light.lightType = LightType::POINT;
        m_light.position = vec4(0, 0, 1, 1);
        m_light.attenuationLin = 2;
        m_light.attenuationQuad = 3;

        TextureSettings sceneTextureSettings = {
            TextureMinFilterSetting(TextureMinFilter::NEAREST),
            TextureMagFilterSetting(TextureMagFilter::NEAREST),
            TextureWrap2DSetting(TextureWrapType::CLAMP_TO_EDGE)
        };
        m_sceneFramebuffer = m_context->CreateFramebuffer();
        Texture2D sceneTexture = m_context->CreateTexture2D(window.GetSize(), TextureSizedFormat::RGBA8, sceneTextureSettings);
        Renderbuffer sceneRenderbuffer = m_context->CreateRenderbuffer(window.GetSize(), TextureSizedFormat::DEPTH24_STENCIL8);
        m_sceneFramebuffer.Attach(CustomFramebufferAttachment::COLOR_0, sceneTexture);
        m_sceneFramebuffer.Attach(CustomFramebufferAttachment::DEPTH_STENCIL, sceneRenderbuffer);

        TextureUnit textureSamplerUnit = TextureUnit::_0;
        Uniform<TextureUnit> textureSamplerUniform("u_textureSampler", textureSamplerUnit);

        uvec3 shaderdataDim1 = uvec3(sceneTexture.dim().x, sceneTexture.dim().y, 3);
        uvec3 shaderdataDim2 = uvec3(sceneTexture.dim().x, sceneTexture.dim().y, 3);
        uvec3 shaderdataDim3 = uvec3(sceneTexture.dim().x, sceneTexture.dim().y, 32);
        m_shaderData1 = m_context->CreateTexture3D(shaderdataDim1, TextureSizedFormat::R32F, sceneTextureSettings);
        m_shaderData2 = m_context->CreateTexture3D(shaderdataDim2, TextureSizedFormat::R32F, sceneTextureSettings);
        m_shaderData3 = m_context->CreateTexture3D(shaderdataDim3, TextureSizedFormat::R32F, sceneTextureSettings);

        NeuralNetInputProgram inputProgram(neuralNetResources);
        NeuralNetOutputProgram outputProgram(neuralNetResources);
        ReLUProgram reluProgram(neuralNetResources);
        TanhProgram tahnProgram(neuralNetResources);

        style_transfer::Add add_layer;
        add_layer.set_id1(0);
        add_layer.set_id2(1);
        AddProgram addProgram(neuralNetResources, add_layer);
        style_transfer::Cat2 cat2_layer;
        cat2_layer.set_id1(0);
        cat2_layer.set_id2(1);
        Cat2Program cat2Program(neuralNetResources, cat2_layer);
        style_transfer::Upsampling upsampling_layer;
        upsampling_layer.set_scale_factor(2);
        UpsamplingProgram upsamplingProgram(neuralNetResources, upsampling_layer);

        style_transfer::Conv2d conv2d;
        conv2d.set_in_channels(3);
        conv2d.set_out_channels(8);
        conv2d.set_kernel_size_x(9);
        conv2d.set_kernel_size_y(9);
        conv2d.set_stride_x(1);
        conv2d.set_stride_y(1);
        conv2d.set_use_bias(true);
        google::protobuf::RepeatedField<float>& conv_weights = *conv2d.mutable_weights();
        conv_weights.Resize(9*9*3*8, 1.0f / 27.0f);
        google::protobuf::RepeatedField<float>& conv_biases = *conv2d.mutable_biases();
        conv_biases.Resize(8, 0.05f);
        Conv2dProgram conv2dProgram(*m_context, neuralNetResources, conv2d);
        int draw_num = 0;
        while(!window.ShouldClose() /*&& draw_num < 100*/)
        {
            float deltaTime = 1.0f / ImGui::GetIO().Framerate;
            float aspectRatio = (float) window.GetSize().x / (float) window.GetSize().y;
            mat4 V;
            mat4 P;

            // update
            {
                glm::quat cameraRot = glm::angleAxis(m_cameraData.transform.rotAngle, m_cameraData.transform.rotAxis);
                vec3 leftDir = glm::rotate(cameraRot, vec3(1,0,0));
                vec3 forwardDir = glm::rotate(cameraRot, vec3(0,0,-1));
                if(window.IsKeyPressed(Key::A)) {
                    m_cameraData.transform.pos -= leftDir * m_moveSpeed * deltaTime;
                }
                if(window.IsKeyPressed(Key::D)) {
                    m_cameraData.transform.pos += leftDir * m_moveSpeed * deltaTime;
                }
                if(window.IsKeyPressed(Key::S)) {
                    m_cameraData.transform.pos -= forwardDir * m_moveSpeed * deltaTime;
                }
                if(window.IsKeyPressed(Key::W)) {
                    m_cameraData.transform.pos += forwardDir * m_moveSpeed * deltaTime;
                }
                if(window.IsKeyPressed(Key::Q)) {
                    m_cameraData.transform.rotAngle += m_rotSpeed * deltaTime;
                }
                if(window.IsKeyPressed(Key::E)) {
                    m_cameraData.transform.rotAngle -= m_rotSpeed * deltaTime;
                }

                V = m_cameraData.transform.ToInv();
                P = m_cameraData.camera.ToMat(aspectRatio);
                shaderTransform.Set(m_objectData.transform.ToMat(), V, P);

                if(m_makeScreenshot) {
                    m_makeScreenshot = false;
                    spdlog::info("screenshot taken");
                    ResourceManager::SaveTexture2D_PNG(sceneTexture, std::to_string(m_screenshotNum) + ".png");
                    m_screenshotNum++;
                }
            }


            {
                FramebufferBinder framebufferBinder = m_context->BindFramebuffer(m_sceneFramebuffer);

                framebufferBinder.Clear();
                m_context->SetViewport(m_context->GetDefaultFramebuffer().dim());
                // draw iterations
                {
                    {
                        RenderProgramBinder programBinder = m_context->BindProgram(commonResources.PBRForwardProgram());
                        shaderTransformUniforms.SetUniforms(programBinder);
                        phongForwardUniforms.SetUniforms(programBinder);
                        TextureUnitsBinder textureUnitsBinder = phongForwardTextureUnits.Bind(*m_context, programBinder);
                        commonResources.SphereMesh().Draw(*m_context, framebufferBinder, programBinder);
                    }
                    if(m_useLayer)
                    {
                        //m_context->MemoryBarrier(bits(MemoryBarrierType::SHADER_IMAGE_ACCESS));
                        inputProgram.Evaluate(*m_context, sceneTexture, m_shaderData1);
                        //m_context->MemoryBarrier(bits(MemoryBarrierType::SHADER_IMAGE_ACCESS));
                        {
                            GraphicsQueryScope<GraphicsQueryType::TIME_ELAPSED> timeQueryScope = renderTimer.Begin();
                            conv2dProgram.Evaluate(*m_context, m_shaderData1, m_shaderData3);
                        }
                        //reluProgram.Evaluate(*m_context, m_shaderData1, m_shaderData2);
                        //m_context->MemoryBarrier(bits(MemoryBarrierType::SHADER_IMAGE_ACCESS));
                        outputProgram.Evaluate(*m_context, m_shaderData3, sceneTexture);
                        //m_context->MemoryBarrier(bits(MemoryBarrierType::SHADER_IMAGE_ACCESS));
                    }
                }
                // show light
                if(m_showLight) 
                {
                    Transform lightTrans;
                    lightTrans.pos = m_light.position;
                    lightTrans.scale = vec3(0.05);
                    shaderTransform.Set(lightTrans.ToMat(), V, P);
                    RenderProgramBinder programBinder = m_context->BindProgram(commonResources.MeshFillProgram());
                    shaderTransformUniforms.SetUniforms(programBinder);
                    lightColorUniform.Set(programBinder);
                    commonResources.CubeMesh().Draw(*m_context, framebufferBinder, programBinder);
                }
            }

            {
                FramebufferBinder framebufferBinder = m_context->BindFramebuffer(defaultFramebuffer);
                framebufferBinder.Clear();
                RenderProgramBinder programBinder = m_context->BindProgram(commonResources.ScreenFillProgram());
                TextureUnitBinder textureUnitBinder = m_context->BindTextureUnit(textureSamplerUnit, sceneTexture);
                textureSamplerUniform.Set(programBinder);
                commonResources.ScreenQuad2DMesh().Draw(*m_context, framebufferBinder, programBinder);
            }


            m_renderTimer = (float)renderTimer.GetAvgTime() / 1000000.0f;
            // draw ui
            {
                ImGuiDraw imguiDraw = imguiContext.Draw();

                ImGui::Begin("layers test settings");

                ImGui::Checkbox("use layer", &m_useLayer);

                if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen))
                {
                    ImGui::DragFloat3("position##1", (float*)&m_cameraData.transform.pos, 0.025);
                    ImGui::DragFloat("rotation", &m_cameraData.transform.rotAngle, 0.005);
                }
                if (ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen))
                {
                    ImGui::Checkbox("show", &m_showLight);
                    ImGui::DragFloat3("position##2", (float*)&m_light.position, 0.01);
                    ImGui::ColorEdit3("color", (float*)&m_light.color);
                    ImGui::DragFloat3("attenuation", (float*)&m_light.attenuationLin, 0.005, 0, 100);
                }
                if (ImGui::CollapsingHeader("Material", ImGuiTreeNodeFlags_DefaultOpen))
                {
                    ImGui::ColorEdit3("albedo", (float*)&m_material.albedo);
                    ImGui::DragFloat("metallic", &m_material.metallic, 0.01, 0, 1);
                    ImGui::DragFloat("roughness", &m_material.roughness, 0.01, 0, 1);
                    ImGui::DragFloat("ao", &m_material.ao, 0.01, 0, 1);
                }
                if (ImGui::CollapsingHeader("Statistics", ImGuiTreeNodeFlags_DefaultOpen))
                {
                    ImGui::Text("gpu avg. render time %.3f ms", m_renderTimer);
                    ImGui::Text("application avg. %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
                }

                ImGui::End();
            }
            defaultFramebuffer.SwapBuffers();
            m_windowManager->PollEvents();
            draw_num++;
        }

    };

private:
    void OnWindowManagerError(const WindowManagerError& error) {
        spdlog::error("{}: {}", error.type, error.message);
    }
    void OnWindowSizeEvent(const WindowSizeEvent& event) {
        spdlog::trace("window resize {}", event.size);
        m_sceneFramebuffer.Resize(event.size);
        uvec3 shaderStorageDim = uvec3(event.size.x, event.size.y, 3);
        m_shaderData1.ResizeUninit(shaderStorageDim);
        m_shaderData2.ResizeUninit(shaderStorageDim);
    }
    void OnKeyEvent(const KeyEvent& event) {
        spdlog::trace("key {} {}", event.key, event.action);
        Window& window = value_or_panic(m_windowManager->GetWindow(m_windowID), "window unexpectedly destroyed");
        if(event.action == KeyAction::PRESS) {
            if(event.key == Key::O) {
                spdlog::set_level(spdlog::level::trace);
            }
            if(event.key == Key::P) {
                spdlog::set_level(spdlog::level::info);
            }
            if(event.key == Key::L) {
                spdlog::info("screenshot should be taken");
                m_makeScreenshot = true;
            }
        }
    }

    void OnScrollEvent(const ScrollEvent& event) {
        spdlog::trace("scroll {}", event.offset);
    }
};

}
/*
#include <fstream>
#include <StyleTransferApp.hpp>
#include <Resource/ResourceManager.hpp>
#include <Profile/GraphicsTimer.hpp>

#include <NeuralNet/ModelPrinter.hpp>
#include <NeuralNet/Resources.hpp>
#include <NeuralNet/Layers.hpp>

using namespace style_transfer;

namespace Morph {

class NetTestApp : public StyleTransferApp
{
private:
    // neural net
    NeuralNetResources m_neuralNetResources;
    // net data
    Texture3D m_shaderData1;
    Texture3D m_shaderData2;
    Texture3D m_shaderData3;
    // timers
    GraphicsTimeElapsedQuery m_timer;
    float m_time = 0;
public:
    NetTestApp(const WindowAppConfig& winConfig, const StyleTransferAppConfig& appConfig)
        : StyleTransferApp(winConfig, appConfig),
        m_neuralNetResources(progCompiler()),
        m_timer(context().CreateQuery<GraphicsQueryType::TIME_ELAPSED>())
    {
        scene().modelType() = StyleTransferScene::ModelType::SPHERE;
    }

protected:
    void Transform() override
    {
        {
            auto timeQueryScope = context().BeginQuery(m_timer);
            //m_netPipeline.Evaluate(context(), scene().inTexture(), scene().outTexture());
        }
        m_time = (float)m_timer.GetValueBlocking() / 1000000.0f;
    }

    float TransformTime() override
    {
        return m_time;
    }
private:
};

void RunNetTest() {
    WindowAppConfig winConfig = {
        ivec2(1024,512), 
        "net test",
        ExecutionTypes::RunOnce(),
        {
            WindowBoolHint(WindowBoolSetting::RESIZABLE, false),
            WindowBoolHint(WindowBoolSetting::VISIBLE, false)
        }
    };
    StyleTransferAppConfig styleTransferConfig = {
        unord_map<string, string>({
            {"engine", MORPH_ENGINE_RES},
            {"view", MORPH_VIEW_RES},
            {"app", MORPH_APP_RES},
            {"style_transfer_lib", STYLE_TRANSFER_LIB_RES}
        })
    };

    StyleTransferScene::CameraData& cameraData = styleTransferConfig.sceneData.cameraData;
    StyleTransferScene::ObjectData& objectDat = styleTransferConfig.sceneData.objectData;
    Light& light = styleTransferConfig.sceneData.light;
    Material& material = styleTransferConfig.sceneData.material;

    cameraData.transform.pos = vec3(-0.375, 0, 0.792);
    cameraData.transform.rotAngle = -0.425f;
    cameraData.transform.rotAxis = vec3(0, 1, 0);

    objectDat.transform.scale = vec3(0.5);

    light.lightType = LightType::POINT;
    light.attenuationLin = 2;
    light.attenuationQuad = 3;

    material.metallic = 0.4;
    material.roughness = 0.6;

    styleTransferConfig.sceneData.sideBySide = true;

    NetTestApp app(winConfig, styleTransferConfig);
    app.Run();
}

}*/

int main(int, char**) {
    Morph::Application app;
    app.Run();
}
