
#ifndef NEURALNET_MODEL_PRINTER_HPP
#define NEURALNET_MODEL_PRINTER_HPP

#include <Core/Core.hpp>

namespace Morph
{

class ModelPrinter
{
public:
    static void Print(string path, uint level = 0);
};

}

#endif // NEURALNET_MODEL_PRINTER_HPP