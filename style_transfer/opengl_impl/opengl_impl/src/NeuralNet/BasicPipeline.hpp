#ifndef MORPH_NEURAL_NET_BASIC_PIPELINE_HPP
#define MORPH_NEURAL_NET_BASIC_PIPELINE_HPP

#include <Core/Core.hpp>

#include "Layers.hpp"
#include "nndata.pb.h"

namespace Morph
{

class BasicNeuralNetPipeline
{
private:
    NeuralNetInputProgram m_inputLayer;
    NeuralNetOutputProgram m_outputLayer;
    vector<NeuralNetLayerVar> m_layers;
    vector<Texture3D> m_data;
public:
    BasicNeuralNetPipeline(GraphicsContext& context, NeuralNetResources& resources, style_transfer::NeuralNet& neuralNet, uvec2 resolution);

    void Evaluate(GraphicsContext& context, Texture2D& inputTexture, Texture2D& outputTexture);

    u64 GetTotalMemoryUsage() const;
    u64 GetMaxBlockMemoryUsage() const;
    void PrintMemoryUsage() const;
};

}

#endif // MORPH_NEURAL_NET_BASIC_PIPELINE_HPP