#ifndef NEURAL_NET_SHADER_STORAGE_3D_HPP
#define NEURAL_NET_SHADER_STORAGE_3D_HPP

#include <Core/Core.hpp>
#include <Graphics/Context.hpp>

namespace Morph 
{

class ShaderStorage3D
{
private:
    uvec3 m_dim;
    ShaderStorageBuffer<0,0,0> m_buffer;
public:
    ShaderStorage3D() : m_dim(vec3(0)), m_buffer() {}
    ShaderStorage3D(GraphicsContext& context, uvec3 a_dim);

    void Resize(GraphicsContext& context, uvec3 a_dim);

    inline uvec3 dim() const { return m_dim; }
    inline ShaderStorageBuffer<0,0,0>& buffer() { return m_buffer; }

    operator bool() const { return m_buffer; }
};

}

#endif // NEURAL_NET_SHADER_STORAGE_3D_HPP