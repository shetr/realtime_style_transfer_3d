#include "Pipeline.hpp"

using namespace style_transfer;

namespace Morph
{

NeuralNetPipeline::NeuralNetPipeline(GraphicsContext& context, NeuralNetResources& resources, style_transfer::NeuralNet& neuralNet, uvec2 resolution)
    :
    m_inputLayer(resources, vec3(0.5f), vec3(0.5f)), m_outputLayer(resources, vec3(0.5f), vec3(0.5f)), m_storage(context)
{
    uvec3 startDim = uvec3(resolution.x, resolution.y, 3);
    vector<uvec3> dims;
    vector<vector<size_t>> dependencies;

    std::tie(dims, dependencies) = GetDimsAndDependencies(neuralNet, startDim);
    m_storage.Create(dims, dependencies);

    for (size_t i = 0; i < neuralNet.layers_size(); i++)
    {
        const Layer &layer = neuralNet.layers(i);
        Layer::LayerOneofCase layerType = layer.layer_oneof_case();
        if (layerType == Layer::LayerOneofCase::kConv2D)
        {
            std::in_place_type_t<Conv2dProgram> programType;
            const Conv2d &conv2D = layer.conv2d();
            m_layers.emplace_back(programType, context, resources, conv2D);
        }
        else if (layerType == Layer::LayerOneofCase::kUpsampling)
        {
            std::in_place_type_t<UpsamplingProgram> programType;
            const Upsampling &upsampling = layer.upsampling();
            m_layers.emplace_back(programType, resources, upsampling);
        }
        else if (layerType == Layer::LayerOneofCase::kTanh)
        {
            std::in_place_type_t<TanhProgram> programType;
            const Tanh &tanh = layer.tanh();
            m_layers.emplace_back(programType, resources);
        }
        else if (layerType == Layer::LayerOneofCase::kReLU)
        {
            std::in_place_type_t<ReLUProgram> programType;
            const ReLU &relu = layer.relu();
            m_layers.emplace_back(programType, resources);
        }
        else if (layerType == Layer::LayerOneofCase::kLeakyReLU)
        {
            std::in_place_type_t<LeakyReLUProgram> programType;
            const LeakyReLU &leakyrelu = layer.leakyrelu();
            m_layers.emplace_back(programType, resources, leakyrelu);
        }
        else if (layerType == Layer::LayerOneofCase::kBatchNorm2D)
        {
            std::in_place_type_t<BatchNorm2dProgram> programType;
            const BatchNorm2d &batchnorm2d = layer.batchnorm2d();
            m_layers.emplace_back(programType, context, resources, batchnorm2d);
        }
        else if (layerType == Layer::LayerOneofCase::kInstanceNorm2D)
        {
            //const Conv2d &conv2D = layer.conv2d();
        }
        else if (layerType == Layer::LayerOneofCase::kAdd)
        {
            std::in_place_type_t<AddProgram> programType;
            const Add &add = layer.add();
            m_layers.emplace_back(programType, resources, add);
        }
        else if (layerType == Layer::LayerOneofCase::kCat2)
        {
            std::in_place_type_t<Cat2Program> programType;
            const Cat2 &cat2 = layer.cat2();
            m_layers.emplace_back(programType, resources, cat2);
        }
        else if (layerType == Layer::LayerOneofCase::kCat3)
        {
            std::in_place_type_t<Cat3Program> programType;
            const Cat3 &cat3 = layer.cat3();
            m_layers.emplace_back(programType, resources, cat3);
        }
    }
}

void NeuralNetPipeline::Evaluate(GraphicsContext& context, Texture2D& inputTexture, Texture2D& outputTexture)
{
    m_inputLayer.Evaluate(context, inputTexture, m_storage[0]);
    context.MemoryBarrier(bits(MemoryBarrierType::SHADER_IMAGE_ACCESS));
    int i = 1;
    for(NeuralNetLayerVar& layer : m_layers) {
        match(layer,
            [&](Conv2dProgram& conv2d) {
                conv2d.Evaluate(context, m_storage[i-1], m_storage[i]);
            },
            [&](UpsamplingProgram& upsampling) {
                upsampling.Evaluate(context, m_storage[i-1], m_storage[i]);
            },
            [&](TanhProgram& tanh) {
                tanh.Evaluate(context, m_storage[i-1], m_storage[i]);
            },
            [&](ReLUProgram& relu) {
                relu.Evaluate(context, m_storage[i-1], m_storage[i]);
            },
            [&](LeakyReLUProgram& leakyRelu) {
                leakyRelu.Evaluate(context, m_storage[i-1], m_storage[i]);
            },
            [&](BatchNorm2dProgram& batchnorm2d) {
                batchnorm2d.Evaluate(context, m_storage[i-1], m_storage[i]);
            },
            [&](AddProgram& add) {
                add.Evaluate(context, m_storage[add.id1()], m_storage[add.id2()], m_storage[i]);
            },
            [&](Cat2Program& cat2) {
                cat2.Evaluate(context, m_storage[cat2.id1()], m_storage[cat2.id2()], m_storage[i]);
            },
            [&](Cat3Program& cat3) {
                cat3.Evaluate(context, m_storage[cat3.id1()], m_storage[cat3.id2()], m_storage[cat3.id3()], m_storage[i]);
            }
        );
        context.MemoryBarrier(bits(MemoryBarrierType::SHADER_IMAGE_ACCESS));
        i++;
    }
    m_outputLayer.Evaluate(context, m_storage[m_layers.size()], outputTexture);
}


u64 NeuralNetPipeline::GetTotalMemoryUsage() const
{
    u64 total = 0;
    for(const Texture3D& data : m_storage.GetTextures())
    {
        total += data.dim().x * data.dim().y * data.dim().z;
    }
    return total * sizeof(float);
}

u64 NeuralNetPipeline::GetMaxBlockMemoryUsage() const
{
    u64 max = 0;
    for(const Texture3D& data : m_storage.GetTextures())
    {
        u64 data_size = data.dim().x * data.dim().y * data.dim().z;
        if(data_size > max)
        {
            max = data_size;
        }
    }
    return max * sizeof(float);
}

void NeuralNetPipeline::PrintMemoryUsage() const
{
    int i = 0;
    for(const Texture3D& data : m_storage.GetTextures())
    {
        u64 data_size = data.dim().x * data.dim().y * data.dim().z * sizeof(float);
        std::cout << i << ": " << (float)data_size / (1024*1024) << " MB" << std::endl;
        i++;
    }
}

tuple<vector<uvec3>, vector<vector<size_t>>> NeuralNetPipeline::GetDimsAndDependencies(style_transfer::NeuralNet& neuralNet, uvec3 startDim)
{
    size_t storageSlotsCount = neuralNet.layers_size() + 1;
    tuple<vector<uvec3>, vector<vector<size_t>>> res = {vector<uvec3>(storageSlotsCount), vector<vector<size_t>>(storageSlotsCount)};
    vector<uvec3>& dims = std::get<0>(res);
    vector<vector<size_t>>& dependencies = std::get<1>(res);
    dims[0] = startDim;
    for (size_t prevLayerIndex = 0; prevLayerIndex < neuralNet.layers_size(); prevLayerIndex++)
    {
        size_t layerIndex = prevLayerIndex+1;
        const Layer &layer = neuralNet.layers(prevLayerIndex);
        uvec3 dim = uvec3(0);
        Layer::LayerOneofCase layerType = layer.layer_oneof_case();
        if (layerType == Layer::LayerOneofCase::kAdd)
        {
            const Add &add = layer.add();
            dim = dims[add.id1()];
            dependencies[layerIndex].push_back(add.id1());
            dependencies[layerIndex].push_back(add.id2());
            if(dims[add.id1()] != dims[add.id2()]) {
                spdlog::error("dims of {}, {} in add does not equal", add.id1(), add.id2());
            }
        }
        else if (layerType == Layer::LayerOneofCase::kCat2)
        {
            const Cat2 &cat2 = layer.cat2();
            dim = dims[cat2.id1()];
            dim.z += dims[cat2.id2()].z;
            dependencies[layerIndex].push_back(cat2.id1());
            dependencies[layerIndex].push_back(cat2.id2());
            if(dims[cat2.id1()].x != dims[cat2.id2()].x || dims[cat2.id1()].y != dims[cat2.id2()].y) {
                spdlog::error("dims of {}, {} in cat2 does not equal", cat2.id1(), cat2.id2());
            }
        }
        else if (layerType == Layer::LayerOneofCase::kCat3)
        {
            const Cat3 &cat3 = layer.cat3();
            dim = dims[cat3.id1()];
            dim.z += dims[cat3.id2()].z;
            dim.z += dims[cat3.id3()].z;
            dependencies[layerIndex].push_back(cat3.id1());
            dependencies[layerIndex].push_back(cat3.id2());
            dependencies[layerIndex].push_back(cat3.id3());
            if(dims[cat3.id1()].x != dims[cat3.id2()].x || dims[cat3.id1()].y != dims[cat3.id2()].y || 
            dims[cat3.id1()].x != dims[cat3.id3()].x || dims[cat3.id1()].y != dims[cat3.id3()].y) {
                spdlog::error("dims of {}, {}, {} in cat3 does not equal", cat3.id1(), cat3.id2(), cat3.id3());
            }
        }
        else
        {
            dependencies[layerIndex].push_back(prevLayerIndex);

            if (layerType == Layer::LayerOneofCase::kConv2D)
            {
                const Conv2d &conv2D = layer.conv2d();
                uvec3 prevDim = dims[prevLayerIndex];
                dim = uvec3(prevDim.x / conv2D.stride_x(), prevDim.y / conv2D.stride_y(), conv2D.out_channels());
            }
            else if (layerType == Layer::LayerOneofCase::kUpsampling)
            {
                const Upsampling &upsampling = layer.upsampling();
                uvec3 prevDim = dims[prevLayerIndex];
                dim = uvec3(prevDim.x * upsampling.scale_factor(), prevDim.y * upsampling.scale_factor(), prevDim.z);
            }
            else {
                dim = dims[prevLayerIndex];
            }
        }
        dims[layerIndex] = dim;
    }
    return res;
}

}