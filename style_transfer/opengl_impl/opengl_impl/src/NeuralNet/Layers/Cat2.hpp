#ifndef MORPH_NEURAL_NET_LAYERS_CAT2_HPP
#define MORPH_NEURAL_NET_LAYERS_CAT2_HPP

#include <Core/Core.hpp>
#include <Graphics/Context.hpp>
#include <Graphics/Uniforms.hpp>

#include <NeuralNet/Resources.hpp>
#include <NeuralNet/nndata.pb.h>

namespace Morph
{

class Cat2Program
{
    struct Cat2Uniforms
    {
        TextureUnit inputUnit1 = TextureUnit::_0;
        TextureUnit inputUnit2 = TextureUnit::_1;
        TextureUnit outputUnit = TextureUnit::_2;
    };
private:
    u32 m_id1;
    u32 m_id2;
    unique<Cat2Uniforms> m_uniforms;
    Uniform<TextureUnit> m_inputUnitUniform1;
    Uniform<TextureUnit> m_inputUnitUniform2;
    Uniform<TextureUnit> m_outputUnitUniform;
    ref<LayerProgram> m_program;
public:
    Cat2Program(NeuralNetResources& resources, const style_transfer::Cat2& cat2);

    inline u32 id1() const { return m_id1; }
    inline u32 id2() const { return m_id2; }

    void Evaluate(GraphicsContext& context, Texture3D& inputData1, Texture3D& inputData2, Texture3D& outputData);
};

}

#endif // MORPH_NEURAL_NET_LAYERS_CAT2_HPP