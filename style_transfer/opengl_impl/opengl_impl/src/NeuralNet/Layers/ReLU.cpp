#include "ReLU.hpp"

#include <NeuralNet/Utils.hpp>

namespace Morph
{

ReLUProgram::ReLUProgram(NeuralNetResources& resources)
    : 
    m_uniforms(unique<ReLUUniforms>(new ReLUUniforms())),
    m_inputUnitUniform("u_input_data", m_uniforms->inputUnit),
    m_outputUnitUniform("u_output_data", m_uniforms->outputUnit),
    m_program(resources.GetReluProgram())
{
}

void ReLUProgram::Evaluate(GraphicsContext& context, Texture3D& inputData, Texture3D& outputData)
{
    uvec3 dim = divide_upper(inputData.dim(), m_program.get().groupSize);
    ComputeProgramBinder programBinder = context.BindProgram(m_program.get().program);
    m_inputUnitUniform.Set(programBinder);
    m_outputUnitUniform.Set(programBinder);
    ImageBinder<true, false> inputBinder = context.BindImage<true, false>(m_uniforms->inputUnit, inputData, true);
    ImageBinder<false, true> ouputBinder = context.BindImage<false, true>(m_uniforms->outputUnit, outputData, true);
    context.DispatchCompute(programBinder, dim);
}

}