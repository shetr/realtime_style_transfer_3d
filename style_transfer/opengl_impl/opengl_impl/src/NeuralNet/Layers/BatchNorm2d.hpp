#ifndef MORPH_NEURAL_NET_LAYERS_BATCH_NORM_2D_HPP
#define MORPH_NEURAL_NET_LAYERS_BATCH_NORM_2D_HPP

#include <Core/Core.hpp>
#include <Graphics/Context.hpp>
#include <Graphics/Uniforms.hpp>

#include <NeuralNet/Resources.hpp>
#include <NeuralNet/nndata.pb.h>

namespace Morph
{

class BatchNorm2dProgram
{
    struct BatchNorm2dUniforms
    {
        float eps = 0;
        TextureUnit inputUnit = TextureUnit::_0;
        TextureUnit outputUnit = TextureUnit::_1;
    };
private:
    unique<BatchNorm2dUniforms> m_uniforms;
    Uniform<float> m_epsUniform;
    UniformBuffer<0,0,0> m_meansBuffer;
    UniformBuffer<0,0,0> m_varsBuffer;
    UniformBuffer<0,0,0> m_gammasBuffer;
    UniformBuffer<0,0,0> m_betasBuffer;
    Uniform<TextureUnit> m_inputUnitUniform;
    Uniform<TextureUnit> m_outputUnitUniform;
    ref<LayerProgram> m_program;
public:
    BatchNorm2dProgram(GraphicsContext& context, NeuralNetResources& resources, const style_transfer::BatchNorm2d& batchNorm2d);

    void Evaluate(GraphicsContext& context, Texture3D& inputData, Texture3D& outputData);
};

}

#endif // MORPH_NEURAL_NET_LAYERS_BATCH_NORM_2D_HPP