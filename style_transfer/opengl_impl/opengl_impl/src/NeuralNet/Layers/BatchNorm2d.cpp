#include "BatchNorm2d.hpp"

#include <NeuralNet/Utils.hpp>

namespace Morph
{

BatchNorm2dProgram::BatchNorm2dProgram(GraphicsContext& context, NeuralNetResources& resources, const style_transfer::BatchNorm2d& batchNorm2d)
    :
    m_uniforms(unique<BatchNorm2dUniforms>(new BatchNorm2dUniforms())),
    m_epsUniform("u_eps", m_uniforms->eps),
    m_inputUnitUniform("u_input_data", m_uniforms->inputUnit),
    m_outputUnitUniform("u_output_data", m_uniforms->outputUnit),
    m_program(resources.GetBatchNorm2dProgram(batchNorm2d.num_features()))
{
    m_uniforms->eps = batchNorm2d.eps();
    m_meansBuffer = context.CreateBufferRaw<GraphicsBufferTarget::UNIFORM,0,0,0>(
        sizeof(float)*batchNorm2d.means_size(), batchNorm2d.means().data()
    );
    m_varsBuffer = context.CreateBufferRaw<GraphicsBufferTarget::UNIFORM,0,0,0>(
        sizeof(float)*batchNorm2d.vars_size(), batchNorm2d.vars().data()
    );
    m_gammasBuffer = context.CreateBufferRaw<GraphicsBufferTarget::UNIFORM,0,0,0>(
        sizeof(float)*batchNorm2d.gammas_size(), batchNorm2d.gammas().data()
    );
    m_betasBuffer = context.CreateBufferRaw<GraphicsBufferTarget::UNIFORM,0,0,0>(
        sizeof(float)*batchNorm2d.betas_size(), batchNorm2d.betas().data()
    );
}

void BatchNorm2dProgram::Evaluate(GraphicsContext& context, Texture3D& inputData, Texture3D& outputData)
{
    uvec3 dim = divide_upper(inputData.dim(), m_program.get().groupSize);
    ComputeProgramBinder programBinder = context.BindProgram(m_program.get().program);
    m_epsUniform.Set(programBinder);
    m_inputUnitUniform.Set(programBinder);
    m_outputUnitUniform.Set(programBinder);
    m_meansBuffer.SetBindBase(0);
    m_varsBuffer.SetBindBase(1);
    m_gammasBuffer.SetBindBase(2);
    m_betasBuffer.SetBindBase(3);
    ImageBinder<true, false> inputBinder = context.BindImage<true, false>(m_uniforms->inputUnit, inputData, true);
    ImageBinder<false, true> ouputBinder = context.BindImage<false, true>(m_uniforms->outputUnit, outputData, true);
    context.DispatchCompute(programBinder, dim);
}

}