#ifndef MORPH_NEURAL_NET_LAYERS_CAT3_HPP
#define MORPH_NEURAL_NET_LAYERS_CAT3_HPP

#include <Core/Core.hpp>
#include <Graphics/Context.hpp>
#include <Graphics/Uniforms.hpp>

#include <NeuralNet/Resources.hpp>
#include <NeuralNet/nndata.pb.h>

namespace Morph
{

class Cat3Program
{
    struct Cat3Uniforms
    {
        TextureUnit inputUnit1 = TextureUnit::_0;
        TextureUnit inputUnit2 = TextureUnit::_1;
        TextureUnit inputUnit3 = TextureUnit::_2;
        TextureUnit outputUnit = TextureUnit::_3;
    };
private:
    u32 m_id1;
    u32 m_id2;
    u32 m_id3;
    unique<Cat3Uniforms> m_uniforms;
    Uniform<TextureUnit> m_inputUnitUniform1;
    Uniform<TextureUnit> m_inputUnitUniform2;
    Uniform<TextureUnit> m_inputUnitUniform3;
    Uniform<TextureUnit> m_outputUnitUniform;
    ref<LayerProgram> m_program;
public:
    Cat3Program(NeuralNetResources& resources, const style_transfer::Cat3 cat3);

    inline u32 id1() const { return m_id1; }
    inline u32 id2() const { return m_id2; }
    inline u32 id3() const { return m_id3; }

    void Evaluate(GraphicsContext& context, Texture3D& inputData1, Texture3D& inputData2, Texture3D& inputData3, Texture3D& outputData);
};

}

#endif // MORPH_NEURAL_NET_LAYERS_CAT3_HPP