#include "BasicPipeline.hpp"

using namespace style_transfer;

namespace Morph
{

BasicNeuralNetPipeline::BasicNeuralNetPipeline(GraphicsContext& context, NeuralNetResources& resources, style_transfer::NeuralNet& neuralNet, uvec2 resolution)
    :
    m_inputLayer(resources, vec3(0.5f), vec3(0.5f)), m_outputLayer(resources, vec3(0.5f), vec3(0.5f))
{
    TextureSettings textureSettings = {
        TextureMinFilterSetting(TextureMinFilter::NEAREST),
        TextureMagFilterSetting(TextureMagFilter::NEAREST),
        TextureWrap2DSetting(TextureWrapType::CLAMP_TO_BORDER)
    };

    uvec3 dim = uvec3(resolution.x, resolution.y, 3);
    m_data.push_back(context.CreateTexture3D(dim, TextureSizedFormat::R32F, textureSettings));

    for (size_t i = 0; i < neuralNet.layers_size(); i++)
    {
        const Layer &layer = neuralNet.layers(i);
        Layer::LayerOneofCase layerType = layer.layer_oneof_case();
        if (layerType == Layer::LayerOneofCase::kConv2D)
        {
            std::in_place_type_t<Conv2dProgram> programType;
            const Conv2d &conv2D = layer.conv2d();
            m_layers.emplace_back(programType, context, resources, conv2D);
            dim = uvec3(m_data.back().dim().x / conv2D.stride_x(), m_data.back().dim().y / conv2D.stride_y(), conv2D.out_channels());
            m_data.push_back(context.CreateTexture3D(dim, TextureSizedFormat::R32F, textureSettings));
        }
        else if (layerType == Layer::LayerOneofCase::kUpsampling)
        {
            std::in_place_type_t<UpsamplingProgram> programType;
            const Upsampling &upsampling = layer.upsampling();
            m_layers.emplace_back(programType, resources, upsampling);
            dim = uvec3(m_data.back().dim().x * upsampling.scale_factor(), m_data.back().dim().y * upsampling.scale_factor(), m_data.back().dim().z);
            m_data.push_back(context.CreateTexture3D(dim, TextureSizedFormat::R32F, textureSettings));
        }
        else if (layerType == Layer::LayerOneofCase::kTanh)
        {
            std::in_place_type_t<TanhProgram> programType;
            const Tanh &tanh = layer.tanh();
            m_layers.emplace_back(programType, resources);
            m_data.push_back(context.CreateTexture3D(m_data.back().dim(), TextureSizedFormat::R32F, textureSettings));
        }
        else if (layerType == Layer::LayerOneofCase::kReLU)
        {
            std::in_place_type_t<ReLUProgram> programType;
            const ReLU &relu = layer.relu();
            m_layers.emplace_back(programType, resources);
            m_data.push_back(context.CreateTexture3D(m_data.back().dim(), TextureSizedFormat::R32F, textureSettings));
        }
        else if (layerType == Layer::LayerOneofCase::kLeakyReLU)
        {
            std::in_place_type_t<LeakyReLUProgram> programType;
            const LeakyReLU &leakyrelu = layer.leakyrelu();
            m_layers.emplace_back(programType, resources, leakyrelu);
            m_data.push_back(context.CreateTexture3D(m_data.back().dim(), TextureSizedFormat::R32F, textureSettings));
        }
        else if (layerType == Layer::LayerOneofCase::kBatchNorm2D)
        {
            std::in_place_type_t<BatchNorm2dProgram> programType;
            const BatchNorm2d &batchnorm2d = layer.batchnorm2d();
            m_layers.emplace_back(programType, context, resources, batchnorm2d);
            m_data.push_back(context.CreateTexture3D(m_data.back().dim(), TextureSizedFormat::R32F, textureSettings));
        }
        else if (layerType == Layer::LayerOneofCase::kInstanceNorm2D)
        {
            //const Conv2d &conv2D = layer.conv2d();
        }
        else if (layerType == Layer::LayerOneofCase::kAdd)
        {
            std::in_place_type_t<AddProgram> programType;
            const Add &add = layer.add();
            m_layers.emplace_back(programType, resources, add);
            dim = m_data[add.id1()].dim();
            m_data.push_back(context.CreateTexture3D(dim, TextureSizedFormat::R32F, textureSettings));
        }
        else if (layerType == Layer::LayerOneofCase::kCat2)
        {
            std::in_place_type_t<Cat2Program> programType;
            const Cat2 &cat2 = layer.cat2();
            m_layers.emplace_back(programType, resources, cat2);
            dim = m_data[cat2.id1()].dim();
            dim.z += m_data[cat2.id2()].dim().z;
            m_data.push_back(context.CreateTexture3D(dim, TextureSizedFormat::R32F, textureSettings));
        }
        else if (layerType == Layer::LayerOneofCase::kCat3)
        {
            std::in_place_type_t<Cat3Program> programType;
            const Cat3 &cat3 = layer.cat3();
            m_layers.emplace_back(programType, resources, cat3);
            dim = m_data[cat3.id1()].dim();
            dim.z += m_data[cat3.id2()].dim().z;
            dim.z += m_data[cat3.id3()].dim().z;
            m_data.push_back(context.CreateTexture3D(dim, TextureSizedFormat::R32F, textureSettings));
        }
    }
}

void BasicNeuralNetPipeline::Evaluate(GraphicsContext& context, Texture2D& inputTexture, Texture2D& outputTexture)
{
    m_inputLayer.Evaluate(context, inputTexture, m_data.front());
    context.MemoryBarrier(bits(MemoryBarrierType::SHADER_IMAGE_ACCESS));
    int i = 1;
    for(NeuralNetLayerVar& layer : m_layers) {
        match(layer,
            [&](Conv2dProgram& conv2d) {
                conv2d.Evaluate(context, m_data[i-1], m_data[i]);
            },
            [&](UpsamplingProgram& upsampling) {
                upsampling.Evaluate(context, m_data[i-1], m_data[i]);
            },
            [&](TanhProgram& tanh) {
                tanh.Evaluate(context, m_data[i-1], m_data[i]);
            },
            [&](ReLUProgram& relu) {
                relu.Evaluate(context, m_data[i-1], m_data[i]);
            },
            [&](LeakyReLUProgram& leakyRelu) {
                leakyRelu.Evaluate(context, m_data[i-1], m_data[i]);
            },
            [&](BatchNorm2dProgram& batchnorm2d) {
                batchnorm2d.Evaluate(context, m_data[i-1], m_data[i]);
            },
            [&](AddProgram& add) {
                add.Evaluate(context, m_data[add.id1()], m_data[add.id2()], m_data[i]);
            },
            [&](Cat2Program& cat2) {
                cat2.Evaluate(context, m_data[cat2.id1()], m_data[cat2.id2()], m_data[i]);
            },
            [&](Cat3Program& cat3) {
                cat3.Evaluate(context, m_data[cat3.id1()], m_data[cat3.id2()], m_data[cat3.id3()], m_data[i]);
            }
        );
        context.MemoryBarrier(bits(MemoryBarrierType::SHADER_IMAGE_ACCESS));
        i++;
    }
    m_outputLayer.Evaluate(context, m_data.back(), outputTexture);
}


u64 BasicNeuralNetPipeline::GetTotalMemoryUsage() const
{
    u64 total = 0;
    for(const Texture3D& data : m_data)
    {
        total += data.dim().x * data.dim().y * data.dim().z;
    }
    return total * sizeof(float);
}

u64 BasicNeuralNetPipeline::GetMaxBlockMemoryUsage() const
{
    u64 max = 0;
    for(const Texture3D& data : m_data)
    {
        u64 data_size = data.dim().x * data.dim().y * data.dim().z;
        if(data_size > max)
        {
            max = data_size;
        }
    }
    return max * sizeof(float);
}

void BasicNeuralNetPipeline::PrintMemoryUsage() const
{
    int i = 0;
    for(const Texture3D& data : m_data)
    {
        u64 data_size = data.dim().x * data.dim().y * data.dim().z * sizeof(float);
        std::cout << i << ": " << (float)data_size / (1024*1024) << " MB" << std::endl;
        i++;
    }
}

}