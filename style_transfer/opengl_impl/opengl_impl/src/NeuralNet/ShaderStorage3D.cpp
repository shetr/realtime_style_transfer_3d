#include "ShaderStorage3D.hpp"

namespace Morph
{


ShaderStorage3D::ShaderStorage3D(GraphicsContext& context, uvec3 a_dim)
    : m_dim(a_dim), 
    m_buffer(context.CreateBufferRaw<GraphicsBufferTarget::SHADER_STORAGE,0,0,0>(a_dim.x*a_dim.y*a_dim.z*sizeof(float)))
{
}

void ShaderStorage3D::Resize(GraphicsContext& context, uvec3 a_dim)
{
    m_dim = a_dim;
    m_buffer = context.CreateBufferRaw<GraphicsBufferTarget::SHADER_STORAGE,0,0,0>(a_dim.x*a_dim.y*a_dim.z*sizeof(float));
}

}