#include "Render.hpp"

#include <StyleTransferApp.hpp>
#include <Resource/ResourceManager.hpp>
#include <Profile/GraphicsTimer.hpp>
#include <MorphImpl/Torch/TorchData.hpp>
#include <MorphImpl/Torch/CudaTimer.hpp>

using namespace Morph::StyleTransferAppImpl;

namespace Morph { namespace MorphImpl {

class TorchRenderApp : public StyleTransferApp
{
private:
    MorphImpl::TorchFuncs m_torchFuncs;
    // torch data operations
    vector<ImageRegistrator> m_imageRegistrators;
    vector<void*> m_imageRegistratorsCache;
    TorchData m_torchData;
    // timers
    CudaEventTimer m_cudaTimer;
    float m_cachedTime = 0;
public:
    TorchRenderApp(const StyleTransferAppArgs& appArgs, MorphImpl::TorchFuncs torchFuncs)
        : StyleTransferApp(appArgs),
        m_torchFuncs(torchFuncs),
        m_torchData(m_torchFuncs,
            uvec2(scene().inTexture().dim().x, scene().inTexture().dim().y),
            enum_count<OutputType>(),
            scene().stylesPaths()
        ),
        m_cudaTimer(m_torchFuncs)
    {
        for(int i = 0; i < enum_count<OutputType>(); ++i) {
            m_imageRegistratorsCache.push_back(nullptr);
            m_imageRegistrators.push_back(ImageRegistrator(m_torchFuncs, scene().OutputTypeToTexture(enum_value<OutputType>(i))));
        }
    }
protected:

    void ApplyStyle() override
    {
        CudaEventTimerScope cudaTimerScope(m_torchFuncs, m_cudaTimer);
        uint style = scene().style();

        const vector<OutputType>& addInputs = scene().stylesInputs()[style];

        for(int i = 0; i < addInputs.size(); i++) {
            m_imageRegistratorsCache[i] = m_imageRegistrators[enum_index(addInputs[i]).value()].cudaResource();
        }
        m_torchData.ApplyStyle(
            style,
            m_imageRegistrators[enum_index(OutputType::STYLE).value()],
            m_imageRegistratorsCache.data(),
            addInputs.size()
        );
    }

    float ApplyStyleTime() override
    {
        m_cachedTime = m_cudaTimer.ElapsedTime().value_or(m_cachedTime);
        return m_cachedTime;
    }
};

}}

void Morph::MorphImpl::TorchStyleTransfer(int argc, char *argv[], TorchFuncs torchFuncs) {
    StyleTransferAppArgs args(argc, argv, "shared:configs/torch_demo.json");

    TorchRenderApp app(args, torchFuncs);
    app.Run();
}