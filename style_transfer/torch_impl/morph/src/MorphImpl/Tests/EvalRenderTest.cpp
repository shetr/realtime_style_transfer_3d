#include "EvalRenderTest.hpp"

#include <StyleTransferApp.hpp>
#include <Resource/ResourceManager.hpp>
#include <Profile/GraphicsTimer.hpp>
#include <MorphImpl/Torch/TorchData.hpp>
#include <MorphImpl/Torch/CudaTimer.hpp>

using namespace Morph::StyleTransferAppImpl;

namespace Morph { namespace MorphImpl {

class EvalRenderTestApp : public StyleTransferApp
{
private:
    MorphImpl::TorchFuncs m_torchFuncs;
    // torch data operations
    ImageRegistrator m_fromImageRegistrator;
    ImageRegistrator m_toImageRegistrator;
    TorchData m_torchData;
    // timers
    CudaEventTimer m_cudaTimer;
    float m_time = 0;
public:
    EvalRenderTestApp(const StyleTransferAppArgs& appArgs, MorphImpl::TorchFuncs torchFuncs)
        : StyleTransferApp(appArgs),
        m_torchFuncs(torchFuncs),
        m_fromImageRegistrator(m_torchFuncs, scene().inTexture()),
        m_toImageRegistrator(m_torchFuncs, scene().outTexture()),
        m_torchData(m_torchFuncs,
            uvec2(scene().inTexture().dim().x, scene().inTexture().dim().y),
            1,
            scene().stylesPaths()
        ),
        m_cudaTimer(m_torchFuncs)
    {
        
    }

    virtual ~EvalRenderTestApp()
    {
        spdlog::info("eval time {}ms", m_time);
    }
protected:
    void ApplyStyle() override
    {
        {
            CudaEventTimerScope cudaTimerScope(m_torchFuncs, m_cudaTimer);
            m_torchData.EvalImage(m_fromImageRegistrator, m_toImageRegistrator);
        }
        m_time = m_cudaTimer.ElapsedTimeBlocking();
    }

    float ApplyStyleTime() override
    {
        return m_time;
    }
};

void EvalRenderTest(TorchFuncs torchFuncs) {
    TestConfig config;

    config.styles = vector<StyleData>({
        {"to_red", "shared:torch_impl/to_red.pt"}
    });

    SceneConfig sceneConfig;

    CameraData& cameraData = sceneConfig.cameraData;
    ObjectData& objectDat = sceneConfig.objectData;
    Light& light = sceneConfig.light;
    Material& material = sceneConfig.material;

    cameraData.transform.pos = vec3(-0.375, 0, 0.792);
    //cameraData.transform.rotAngle = -0.425f;
    //cameraData.transform.rotAxis = vec3(0, 1, 0);

    objectDat.transform.scale = vec3(0.5);

    light.lightType = LightType::POINT;
    light.attenuationLin = 2;
    light.attenuationQuad = 3;

    material.metallic = 0.4;
    material.roughness = 0.6;

    config.sceneConfigs.push_back(sceneConfig);

    config.outputs.push_back(OutputType::STYLE);

    StyleTransferAppArgs args(config);

    EvalRenderTestApp app(args, torchFuncs);
    app.Run();
}

}}