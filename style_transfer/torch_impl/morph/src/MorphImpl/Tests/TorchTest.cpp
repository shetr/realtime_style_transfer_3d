#include "TorchTest.hpp"

#include <StyleTransferApp.hpp>
#include <Profile/GraphicsTimer.hpp>
#include <MorphImpl/Torch/TorchData.hpp>
#include <MorphImpl/Torch/CudaTimer.hpp>

using namespace Morph::StyleTransferAppImpl;

namespace Morph { namespace MorphImpl {

class TorchTestApp : public StyleTransferApp
{
private:
    MorphImpl::TorchFuncs m_torchFuncs;
    // torch data operations
    ImageRegistrator m_fromImageRegistrator;
    ImageRegistrator m_toImageRegistrator;
    TorchData m_torchData;
    // timers
    CudaTimer<10> m_cudaTimer;
public:
    TorchTestApp(const StyleTransferAppArgs& appArgs, MorphImpl::TorchFuncs torchFuncs)
        : StyleTransferApp(appArgs),
        m_torchFuncs(torchFuncs),
        m_fromImageRegistrator(m_torchFuncs, scene().inTexture()),
        m_toImageRegistrator(m_torchFuncs, scene().outTexture()),
        m_torchData(m_torchFuncs,
            uvec2(scene().inTexture().dim().x, scene().inTexture().dim().y),
            1,
            scene().stylesPaths()
        ),
        m_cudaTimer(m_torchFuncs)
    {
        
    }
protected:
    void ApplyStyle() override
    {
        {
            auto cudaTimerScope = m_cudaTimer.Begin();
            m_torchData.InvertImage(m_fromImageRegistrator, m_toImageRegistrator);
        }
    }

    float ApplyStyleTime() override
    {
        return m_cudaTimer.GetAvgTime();
    }
};

void TorchTest(TorchFuncs torchFuncs) {
    InteractiveConfig config;

    config.title = "cuda draw test";

    config.styles = vector<StyleData>({
        {"to_red", "shared:torch_impl/to_red.pt"}
    });

    CameraData& cameraData = config.sceneConfig.cameraData;
    ObjectData& objectDat = config.sceneConfig.objectData;
    Light& light = config.sceneConfig.light;
    Material& material = config.sceneConfig.material;

    cameraData.transform.pos = vec3(-0.375, 0, 0.792);
    //cameraData.transform.rotAngle = -0.425f;
    //cameraData.transform.rotAxis = vec3(0, 1, 0);

    objectDat.transform.scale = vec3(0.5);

    light.lightType = LightType::POINT;
    light.attenuationLin = 2;
    light.attenuationQuad = 3;

    material.metallic = 0.4;
    material.roughness = 0.6;


    config.lazyUpdate = false;
    config.maskOutput = false;
    config.sideBySide = false;

    StyleTransferAppArgs args(config);

    TorchTestApp app(args, torchFuncs);
    app.Run();
}

}}

