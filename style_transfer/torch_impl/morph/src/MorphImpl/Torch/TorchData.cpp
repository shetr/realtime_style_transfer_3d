#include "TorchData.hpp"

namespace Morph { namespace MorphImpl {

TorchData::TorchData(TorchFuncs& torchFuncs, uvec2 dim, uint texturesCount, const std::vector<std::string>& stylesPaths)
{
    m_torchFuncs = &torchFuncs;
    std::vector<const char*> paths(stylesPaths.size());
    std::transform(stylesPaths.begin(), stylesPaths.end(), paths.begin(), 
        [](const string& path) { return path.c_str(); }
    );
    m_torchData = m_torchFuncs->CreateTorchData(dim.x, dim.y, texturesCount, paths.data(), stylesPaths.size());
}

TorchData::TorchData(TorchData&& old)
{
    std::swap(m_torchFuncs, old.m_torchFuncs);
    std::swap(m_torchData, old.m_torchData);
}
TorchData& TorchData::operator=(TorchData&& old)
{
    std::swap(m_torchFuncs, old.m_torchFuncs);
    std::swap(m_torchData, old.m_torchData);
    return *this;
}
TorchData::~TorchData()
{
    if(m_torchData != nullptr) {
        m_torchFuncs->DeleteTorchData(m_torchData);
    }
}

void TorchData::InvertImage(ImageRegistrator& fromImage, ImageRegistrator& toImage)
{
    m_torchFuncs->InvertImage(m_torchData, fromImage.cudaResource(), toImage.cudaResource());
}
void TorchData::ApplyStyle(uint style, ImageRegistrator& toImage, void** fromImagesResources, uint fromImagesCount)
{
    m_torchFuncs->ApplyStyle(m_torchData, style, toImage.cudaResource(), fromImagesResources, fromImagesCount);
}
void TorchData::EvalImage(ImageRegistrator& fromImage, ImageRegistrator& toImage)
{
    m_torchFuncs->EvalImage(m_torchData, fromImage.cudaResource(), toImage.cudaResource());
}

}}