#include "ImageRegistrator.hpp"

namespace Morph { namespace MorphImpl {

ImageRegistrator::ImageRegistrator(TorchFuncs& torchFuncs, Texture2D& texture)
{
    m_torchFuncs = &torchFuncs;
    m_torchFuncs->RegisterImage(
        m_cudaImageResource, 
        texture.id(), 
        GraphicsEnums::textureTypes.ToValue(TextureType::_2D)
    );
}

ImageRegistrator::ImageRegistrator(ImageRegistrator&& old)
{
    std::swap(m_torchFuncs, old.m_torchFuncs);
    std::swap(m_cudaImageResource, old.m_cudaImageResource);
}
ImageRegistrator& ImageRegistrator::operator=(ImageRegistrator&& old)
{
    std::swap(m_torchFuncs, old.m_torchFuncs);
    std::swap(m_cudaImageResource, old.m_cudaImageResource);
    return *this;
}

ImageRegistrator::~ImageRegistrator()
{
    if(m_cudaImageResource != nullptr) {
        m_torchFuncs->UnregisterImage(m_cudaImageResource);
    }
}

}}