#ifndef STYLE_TRANSFER_MORPH_IMPL_CUDA_TIMER_HPP
#define STYLE_TRANSFER_MORPH_IMPL_CUDA_TIMER_HPP

#include "TorchFuncs.hpp"
#include "CudaEventTimer.hpp"

namespace Morph { namespace MorphImpl {

template<uint EVENT_TIMERS_COUNT>
class CudaTimer
{
private:
    TorchFuncs* m_torchFuncs = nullptr;
    usize m_useIndex = 0;
    array<CudaEventTimer, EVENT_TIMERS_COUNT> m_cudaEventTimers;
    array<bool, EVENT_TIMERS_COUNT> m_cudaEventTimersUsed;
public:
    CudaTimer(TorchFuncs& torchFuncs) : m_torchFuncs(&torchFuncs) {
        for(CudaEventTimer& eventTimer : m_cudaEventTimers) {
            eventTimer = CudaEventTimer(torchFuncs);
        }
        m_cudaEventTimersUsed.fill(false);
    }

    CudaEventTimerScope Begin() {
        CudaEventTimerScope scope(*m_torchFuncs, m_cudaEventTimers[m_useIndex]);
        m_cudaEventTimersUsed[m_useIndex] = true;
        m_useIndex = (m_useIndex + 1) % EVENT_TIMERS_COUNT;
        return scope;
    }

    float GetAvgTime() const {
        float avg = 0;
        int count = 0;
        for(const CudaEventTimer& eventTimer : m_cudaEventTimers) {
            if(m_cudaEventTimersUsed[m_useIndex] && eventTimer.Query()) {
                avg += eventTimer.ElapsedTime().value();
                count++;
            }
        }
        if(count == 0) {
            return 0;
        }
        return avg / count;
    }
};

}}


#endif // STYLE_TRANSFER_MORPH_IMPL_CUDA_TIMER_HPP