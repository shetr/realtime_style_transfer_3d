#ifndef STYLE_TRANSFER_MORPH_IMPL_TORCH_DATA_HPP
#define STYLE_TRANSFER_MORPH_IMPL_TORCH_DATA_HPP

#include <Morph.hpp>

#include "TorchFuncs.hpp"
#include "ImageRegistrator.hpp"

namespace Morph { namespace MorphImpl {

class TorchData
{
private:
    TorchFuncs* m_torchFuncs = nullptr;
    void* m_torchData = nullptr;
public:
    TorchData() {}
    TorchData(TorchFuncs& torchFuncs, uvec2 dim, uint texturesCount, const std::vector<std::string>& stylesPaths);

    TorchData(const TorchData& other) = delete;
    TorchData(TorchData&& old);
    TorchData& operator=(TorchData&& old);
    virtual ~TorchData();

    inline operator bool() const { return m_torchData != nullptr; }

    void InvertImage(ImageRegistrator& fromImage, ImageRegistrator& toImage);
    void ApplyStyle(uint style, ImageRegistrator& toImage, void** fromImagesResources, uint fromImagesCount);
    void EvalImage(ImageRegistrator& fromImage, ImageRegistrator& toImage);
};

}}


#endif // STYLE_TRANSFER_MORPH_IMPL_TORCH_DATA_HPP