#include "TorchImpl.hpp"

#include <cuda_runtime.h>
#include <cuda_gl_interop.h>
#include <cstdlib>

#include "private/TorchData.hpp"
#include "private/Conversion.hpp"
#include "private/MapToSurface.cuh"
#include "private/CudaDebug.hpp"
#include "private/PNG.hpp"

namespace Morph { namespace TorchImpl {

void* CreateTorchData(unsigned int width, unsigned int height, unsigned int texturesCount, const char** stylesPaths, unsigned int stylesPathsCount)
{
    TorchData* torchData = new TorchData(width, height, texturesCount, stylesPaths, stylesPathsCount);
    return (void*)torchData;
}

void DeleteTorchData(void* torchData)
{
    delete ((TorchData*)torchData);
}

void RegisterImage(void*& out_resource, unsigned int texture_id, unsigned int texture_type)
{
    CUERR(cudaGraphicsGLRegisterImage(
        (cudaGraphicsResource**)&out_resource, texture_id, texture_type, cudaGraphicsRegisterFlagsWriteDiscard
    ));
}

void UnregisterImage(void* resource)
{
    CUERR(cudaGraphicsUnregisterResource((cudaGraphicsResource*)resource));
}

void InvertImage(void* _torchData, void*& _fromImageResource, void*& _toImageResource)
{
    TorchData* torchData = (TorchData*)_torchData;
    cudaGraphicsResource** fromImageResource = (cudaGraphicsResource **)&_fromImageResource;
    cudaGraphicsResource** toImageResource = (cudaGraphicsResource **)&_toImageResource;
    {
        // map the texture memmory
        MapToSurface mapToSurface(fromImageResource);
        // convert to tensor
        ConvertSurfaceToTensor(mapToSurface.surface(), torchData->cudaTextures[0], torchData->width, torchData->height);
    }
    torch::Tensor texture = torch::from_blob(
        torchData->cudaTextures[0], 
        {torchData->height, torchData->width, 3}, 
        torch::dtype(torch::kFloat32).device(torch::kCUDA));
    // evaluate
    texture = 1 - texture;
    // convert back
    torch::PackedTensorAccessor64<float, 3> cudaAccessor = texture.packed_accessor64<float,3>();
    {
        // map the texture memmory
        MapToSurface mapToSurface(toImageResource);
        ConvertTensorToSurface(mapToSurface.surface(), cudaAccessor);
    }
}

void ApplyStyle(void* _torchData, unsigned int style, void*& _toImageResource, void** _fromImagesResources, unsigned int fromImagesCount)
{
    using namespace torch::indexing;

    TorchData* torchData = (TorchData*)_torchData;
    cudaGraphicsResource** toImageResource = (cudaGraphicsResource **)&_toImageResource;

    // for internal profiling
    //printf("cuda time: %8.2f ms\n", torchData->avgTimer.GetAvgTime());

    torch::Tensor inData = torch::zeros({1, torchData->channels * fromImagesCount, torchData->height, torchData->width}, torch::dtype(torch::kFloat32).device(torch::kCUDA));
    for(size_t i = 0; i < fromImagesCount; ++i) {
        cudaGraphicsResource** fromImageResource = (cudaGraphicsResource **)&_fromImagesResources[i];
        {
            // map the texture memmory
            MapToSurface mapToSurface(fromImageResource);
            // convert to tensor
            ConvertSurfaceToTensor(mapToSurface.surface(), torchData->cudaTextures[i], torchData->width, torchData->height);
        }
        // put to inputs
        inData.index_put_({0, Slice(i*3, i*3+3)},
            // convert to tensor
            ((torch::from_blob(
            torchData->cudaTextures[i], 
            {torchData->height, torchData->width, torchData->channels}, 
            torch::dtype(torch::kFloat32).device(torch::kCUDA))
            // transform for nn model
            - 0.5f) / 0.5f).unsqueeze(0).permute({0, 3, 1, 2})
        );
    }
    // evaluate
    {
        std::vector<torch::jit::IValue> inputs;
        inputs.push_back(inData);
        torchData->outData = (
            torchData->styles[style].forward(inputs).toTensor()
            // transform back
            * 0.5f + 0.5f).permute({0, 2, 3, 1})[0];
    }
    {
        // for internal profiling, move timer where desired
        // EventTimerScope eventTimerScope = torchData->avgTimer.Begin();
        // convert back
        torch::PackedTensorAccessor64<float, 3> cudaAccessor = torchData->outData.packed_accessor64<float,3>();
        // map the texture memmory
        MapToSurface mapToSurface(toImageResource);
        ConvertTensorToSurface(mapToSurface.surface(), cudaAccessor);
    }
}

void ApplyStylePNG()
{
    
}

void EvalImage(void* _torchData, void*& _fromImageResource, void*& _toImageResource)
{
    TorchData* torchData = (TorchData*)_torchData;
    cudaGraphicsResource** fromImageResource = (cudaGraphicsResource **)&_fromImageResource;
    cudaGraphicsResource** toImageResource = (cudaGraphicsResource **)&_toImageResource;
    {
        // map the texture memmory
        MapToSurface mapToSurface(fromImageResource);
        // convert to tensor
        ConvertSurfaceToTensor(mapToSurface.surface(), torchData->cudaTextures[0], torchData->width, torchData->height);
    }
    torch::Tensor texture = torch::from_blob(
        torchData->cudaTextures[0], 
        {torchData->height, torchData->width, 3}, 
        torch::dtype(torch::kFloat32).device(torch::kCUDA));
    // transform for nn model
    texture = ((texture - 0.5f) / 0.5f).unsqueeze(0).permute({0, 3, 1, 2});
    // evaluate
    std::vector<torch::jit::IValue> inputs;
    inputs.push_back(texture);
    texture = torchData->styles[0].forward(inputs).toTensor();
    // transform back
    texture = (texture * 0.5f + 0.5f).permute({0, 2, 3, 1})[0];
    // convert back
    torch::PackedTensorAccessor64<float, 3> cudaAccessor = texture.packed_accessor64<float,3>();
    {
        // map the texture memmory
        MapToSurface mapToSurface(toImageResource);
        ConvertTensorToSurface(mapToSurface.surface(), cudaAccessor);
    }
}

void EvalPNGImage(std::string _model, std::string in_location, std::string out_location)
{
    torch::jit::script::Module model;
    try {
        model = torch::jit::load(_model);
        std::cout << "model loaded" << std::endl;
    }
    catch (const c10::Error& e) {
        std::cout << "error loading the model\n";
        std::cout << e.what() << std::endl;
        return;
    }
    model.to(torch::kCUDA);

    torch::Tensor texture = ReadTensorFrom_PNG(in_location);
    texture = texture.to(torch::kCUDA);
    texture = (texture - 0.5f) / 0.5f;
    texture = texture.unsqueeze(0).permute({0, 3, 1, 2});
    std::cout << "texture loaded" << std::endl;
    std::cout << texture.sizes() << std::endl;

    std::vector<torch::jit::IValue> inputs;
    inputs.push_back(texture);

    texture = model.forward(inputs).toTensor();
    std::cout << "evaluated" << std::endl;

    texture = texture * 0.5f + 0.5f;
    texture = texture.permute({0, 2, 3, 1})[0];
    texture = texture.to(torch::kCPU);
    WriteTensorTo_PNG(texture, out_location);
    std::cout << "texture saved" << std::endl;
}

void* CudaEventCreate()
{
    cudaEvent_t event;
    CUERR(cudaEventCreate(&event));
    return (void*)event;
}

void CudaEventDestroy(void* _event)
{
    cudaEvent_t event = (cudaEvent_t)_event;
    CUERR(cudaEventDestroy(event));
}

void CudaEventRecord(void* _event)
{
    cudaEvent_t event = (cudaEvent_t)_event;
    CUERR(cudaEventRecord(event));
}

void CudaEventSynchronize(void* _event)
{
    cudaEvent_t event = (cudaEvent_t)_event;
    CUERR(cudaEventSynchronize(event));
}

bool CudaEventElapsedTime(float* ms, void* _start_event, void* _end_event)
{
    cudaEvent_t start_event = (cudaEvent_t)_start_event;
    cudaEvent_t end_event = (cudaEvent_t)_end_event;
    cudaError err = cudaEventElapsedTime(ms, start_event, end_event);
    if(err == cudaError::cudaSuccess) {
        return true;
    } else if(err == cudaError::cudaErrorNotReady) {
        return false;
    } else {
        CUERR(err);
        return false;
    }
}

bool CudaEventQuery(void* _event)
{
    cudaEvent_t event = (cudaEvent_t)_event;
    cudaError err = cudaEventQuery(event);
    if(err == cudaError::cudaSuccess) {
        return true;
    } else if(err == cudaError::cudaErrorNotReady) {
        return false;
    } else {
        CUERR(err);
        return false;
    }
}

void CudaDeviceSynchronize()
{
    CUERR(cudaDeviceSynchronize());
}


}}