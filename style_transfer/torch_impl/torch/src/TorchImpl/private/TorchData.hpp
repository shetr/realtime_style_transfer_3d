#ifndef STYLE_TRANSFER_TORCH_IMPL_TORCH_DATA_HPP
#define STYLE_TRANSFER_TORCH_IMPL_TORCH_DATA_HPP

#include <string>
#include <vector>
#include <cuda_runtime.h>
#include <torch/torch.h>
#include <torch/script.h>

#include "CudaDebug.hpp"
#include "EventTimer.hpp"

namespace Morph { namespace TorchImpl {

struct TorchData
{
    unsigned int channels = 3;
    unsigned int height;
    unsigned int width;
    torch::Tensor outData;
    //std::vector<torch::Tensor> tensorTextures;
    std::vector<float*> cudaTextures;
    std::vector<torch::jit::script::Module> styles;
    AvgTimer<5> avgTimer;

    TorchData(unsigned int _width, unsigned int _height, unsigned int texturesCount, const char** stylesPaths, unsigned int stylesPathsCount) 
        : height(_height), width(_width), cudaTextures(texturesCount)
    {
        outData = torch::zeros({height, width, channels}, torch::dtype(torch::kFloat32).device(torch::kCUDA));
        //tensorTextures.reserve(texturesCount);
        for(size_t i = 0; i < texturesCount; ++i) {
            CUERR(cudaMallocManaged(&cudaTextures[i], width*height*channels*sizeof(float)));
            //tensorTextures.push_back(torch::zeros({height, width, channels}, torch::dtype(torch::kFloat32).device(torch::kCUDA)));
        }

        for(size_t i = 0; i < stylesPathsCount; ++i) {
            torch::jit::script::Module style;
            const char* stylePath = stylesPaths[i];
            try {
                style = torch::jit::load(stylePath);
                #ifdef MORPH_DEBUG
                    std::cout << "style " << i << " loaded" << std::endl;
                #endif
            }
            catch (const c10::Error& e) {
                std::cout << "error loading style at " << stylePath << " \n";
                std::cout << e.what() << std::endl;
                exit(1);
            }
            style.to(torch::kCUDA);
            styles.push_back(style);
        }
    }
    ~TorchData()
    {
        for(float* cudaTexture : cudaTextures) {
            CUERR(cudaFree(cudaTexture));
        }
    }
};

}}

#endif // STYLE_TRANSFER_TORCH_IMPL_TORCH_DATA_HPP