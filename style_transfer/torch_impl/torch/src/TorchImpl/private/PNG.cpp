#include "PNG.hpp"

#include <torch/script.h>

#include <iostream>
#include <memory>
#include <vector>
#include <string>

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb/stb_image_write.h>

namespace Morph { namespace TorchImpl {

torch::Tensor ReadTensorFrom_PNG(std::string filename)
{
    int width, height, channels;
    uint8_t *img = stbi_load(filename.c_str(), &width, &height, &channels, 0);
    std::vector<float> data(width * height * channels);
    for(int i = 0; i < data.size(); ++i) {
        data[i] = static_cast<float>(img[i]) / 255.0f;
    }
    torch::Tensor texture = torch::from_blob(data.data(), {height, width, channels});
    int dims_data[] = {0,1,2};
    torch::Tensor dims = torch::from_blob(dims_data, {3}, torch::kInt32);
    return channels > 3 ? texture.index_select(2, dims) : texture;
}

void WriteTensorTo_PNG(const torch::Tensor& texture, std::string filename)
{
    torch::Tensor texture_flat = texture.flatten();
    auto texture_accessor = texture_flat.accessor<float, 1>();
    int height = texture.size(0);
    int width = texture.size(1);
    int channels = texture.size(2);
    std::vector<uint8_t> data(width * height * channels);
    for(int i = 0; i < data.size(); ++i) {
        float v = texture_accessor[i] * 255.0f;
        data[i] = static_cast<uint8_t>(v);
    }
    stbi_flip_vertically_on_write(true);
    stbi_write_png(filename.c_str(), width, height, channels, data.data(), channels * width);
}

}}