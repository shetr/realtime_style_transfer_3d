#ifndef STYLE_TRANSFER_TORCH_IMPL_EVENT_TIMER_HPP
#define STYLE_TRANSFER_TORCH_IMPL_EVENT_TIMER_HPP

#include <cuda_runtime.h>
#include <array>

namespace Morph { namespace TorchImpl {

class EventTimer
{
private:
    cudaEvent_t m_startEvent = nullptr;
    cudaEvent_t m_endEvent = nullptr;
    mutable float m_timeCache = 0.0f;
public:
    EventTimer();

    EventTimer(const EventTimer& other) = delete;
    EventTimer(EventTimer&& old);
    EventTimer& operator=(EventTimer&& old);
    virtual ~EventTimer();
    inline operator bool() const { return m_startEvent != nullptr; }

    void RecordStart();
    void RecordEnd();

    // waits until time is measured
    void Synchronize() const;
    // checks if time was measured non-blocking
    bool Query() const;
    // elapsed time in miliseconds non-blocking, if value is not available returns negative float
    float ElapsedTime() const;
    // elapsed time in miliseconds blocking
    float ElapsedTimeBlocking() const;

private:
    inline cudaEvent_t start() { return m_startEvent; }
    inline cudaEvent_t end() { return m_endEvent; }
};

// Scope in which to measure the time
class EventTimerScope
{
private:
    EventTimer* m_timer = nullptr;
public:
    EventTimerScope(EventTimer& timer);
    EventTimerScope(const EventTimerScope& other) = delete;
    EventTimerScope(EventTimerScope&& old);
    EventTimerScope& operator=(EventTimerScope&& old);
    virtual ~EventTimerScope();
    inline operator bool() const { return m_timer != nullptr; }
};

template<size_t EVENT_TIMERS_COUNT>
class AvgTimer
{
private:
    size_t m_useIndex = 0;
    std::array<EventTimer, EVENT_TIMERS_COUNT> m_eventTimers;
    std::array<bool, EVENT_TIMERS_COUNT> m_eventTimersUsed;
public:
    AvgTimer() {
        for(EventTimer& eventTimer : m_eventTimers) {
            eventTimer = EventTimer();
        }
        m_eventTimersUsed.fill(false);
    }

    EventTimerScope Begin() {
        EventTimerScope scope(m_eventTimers[m_useIndex]);
        m_eventTimersUsed[m_useIndex] = true;
        m_useIndex = (m_useIndex + 1) % EVENT_TIMERS_COUNT;
        return scope;
    }

    float GetAvgTime() const {
        float avg = 0;
        int count = 0;
        for(const EventTimer& eventTimer : m_eventTimers) {
            if(m_eventTimersUsed[m_useIndex] && eventTimer.Query()) {
                float maybeTime = eventTimer.ElapsedTime();
                avg += maybeTime >= 0 ? maybeTime : 0;
                count++;
            }
        }
        if(count == 0) {
            return 0;
        }
        return avg / count;
    }
};

}}

#endif // STYLE_TRANSFER_TORCH_IMPL_EVENT_TIMER_HPP