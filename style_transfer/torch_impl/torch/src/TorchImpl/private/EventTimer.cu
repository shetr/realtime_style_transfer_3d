#include "EventTimer.hpp"

#include "CudaDebug.hpp"

namespace Morph { namespace TorchImpl {

    EventTimer::EventTimer()
{
    CUERR(cudaEventCreate(&m_startEvent));
    CUERR(cudaEventCreate(&m_endEvent));
}
EventTimer::EventTimer(EventTimer&& old)
{
    std::swap(m_startEvent, old.m_startEvent);
    std::swap(m_endEvent, old.m_endEvent);
}
EventTimer& EventTimer::operator=(EventTimer&& old)
{
    std::swap(m_startEvent, old.m_startEvent);
    std::swap(m_endEvent, old.m_endEvent);
    return *this;
}
EventTimer::~EventTimer()
{
    if(m_startEvent != nullptr) {
        CUERR(cudaEventDestroy(m_startEvent));
        CUERR(cudaEventDestroy(m_endEvent));
    }
}


void EventTimer::RecordStart()
{
    m_timeCache = -1;
    CUERR(cudaEventRecord(start()));
}
void EventTimer::RecordEnd()
{
    CUERR(cudaEventRecord(end()));
}

void EventTimer::Synchronize() const
{
    CUERR(cudaEventSynchronize(m_endEvent));
}
bool EventTimer::Query() const
{
    if(m_timeCache >= 0) {
        return true;
    }
    cudaError err = cudaEventQuery(m_endEvent);
    if(err == cudaError::cudaSuccess) {
        return true;
    } else if(err == cudaError::cudaErrorNotReady) {
        return false;
    } else {
        CUERR(err);
        return false;
    }
}
float EventTimer::ElapsedTime() const
{
    if(m_timeCache >= 0) {
        return m_timeCache;
    }
    float time = 0;

    cudaError err = cudaEventElapsedTime(&time, m_startEvent, m_endEvent);
    bool is_available;
    if(err == cudaError::cudaSuccess) {
        is_available = true;
    } else if(err == cudaError::cudaErrorNotReady) {
        is_available = false;
    } else {
        CUERR(err);
        is_available = false;
    }

    if(is_available) {
        m_timeCache = time;
    }
    return is_available ? m_timeCache : -1.0f;
}
float EventTimer::ElapsedTimeBlocking() const
{
    if(m_timeCache >= 0) {
        return m_timeCache;
    }
    Synchronize();
    float maybeTime = ElapsedTime();
    return maybeTime >= 0 ? maybeTime : 0;
}


EventTimerScope::EventTimerScope(EventTimer& timer)
    : m_timer(&timer)
{
    m_timer->RecordStart();
}
EventTimerScope::EventTimerScope(EventTimerScope&& old)
{
    std::swap(m_timer, old.m_timer);
}
EventTimerScope& EventTimerScope::operator=(EventTimerScope&& old)
{
    std::swap(m_timer, old.m_timer);
    return *this;
}
EventTimerScope::~EventTimerScope()
{
    if(m_timer != nullptr) {
        m_timer->RecordEnd();
    }
}

}}