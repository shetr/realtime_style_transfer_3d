#include "CudaDebug.hpp"

namespace Morph { namespace TorchImpl {

bool CudaError(cudaError err, const char* file, int line)
{
    bool res = false;
    if(err != cudaError::cudaSuccess) {
        res = true;
        std::cout << "Cuda error " << err << ", line " << line << ", file " << file << std::endl;
    }
    return res;
}

}}