#include <iostream>
#include <math.h>

#include <MorphImpl/Render.hpp>
#include <TorchAPI.hpp>

#include <string>

int main(int argc, char *argv[])
{

  Morph::MorphImpl::TorchStyleTransfer(argc, argv, Morph::GetTorchAPI());

  return 0;
} 
 
