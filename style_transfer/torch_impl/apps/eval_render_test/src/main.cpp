#include <iostream>
#include <math.h>

#include <MorphImpl/Tests/EvalRenderTest.hpp>
#include <TorchAPI.hpp>

int main(void)
{
  using namespace Morph::TorchImpl;
  
  Morph::MorphImpl::EvalRenderTest(Morph::GetTorchAPI());
  
  return 0;
} 
 
