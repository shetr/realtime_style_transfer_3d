#include <Core/Core.hpp>
#include <Resource/ResourceManager.hpp>
#include <Resource/Generated.hpp>

namespace Morph {

int RunGenSphere(int argc, char *argv[])
{
    IndexedVerticesMesh3D<u32> sphere = GeneratedResources::Sphere(70, 35, 1, true);
    ResourceManager::SaveMesh3D_OBJ(sphere, "sphere.obj");
    //IndexedVerticesMesh3D<u32> cylinder = GeneratedResources::Cylinder(3);
    //ResourceManager::SaveMesh3D_OBJ(cylinder, "cylinder.obj");
    return EXIT_SUCCESS;
}

}


int main(int argc, char *argv[]) {
    return Morph::RunGenSphere(argc, argv);
}