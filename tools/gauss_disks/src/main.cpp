#include <Core/Core.hpp>
#include <Resource/ResourceManager.hpp>
#include <Resource/Storage.hpp>

#include "poisson_disk_sampling.h"

namespace pds = thinks::poisson_disk_sampling;

namespace Morph {

const float eps = 0.00001;

struct GaussDisk
{
    vec3 pos;
    vec3 color;
};

bool in_range(ivec3 v, uvec3 dim) {
    return v.x >= 0 && v.y >= 0 && v.z >= 0 && v.x < dim.x && v.y < dim.y && v.z < dim.z;
}

vec3 lerp(const vec3& from, const vec3& to, float& t)
{
  return (1-t)*from + t*to;
}

float sqr(float x) { return x*x; }

float gauss(vec3 v, float sigma)
{
    return std::exp(-(sqr(v.x) + sqr(v.y) + sqr(v.z)) / (sqr(sigma)));
}

u64 index(uvec3 i, uvec3 dim)
{
    u64 x = i.x;
    u64 y = i.y;
    u64 z = i.z;
    u64 dx = dim.x;
    u64 dy = dim.y;
    return x + y * dim.x + z * dim.x * dim.y;
}

// x - phi, y - theta
vec2 dirToPolar(vec3 dir)
{
    vec3 dir_normed = glm::normalize(dir);
    vec2 xz = vec2(dir_normed.x, dir_normed.z);
    vec2 ty = vec2(glm::length(xz), dir_normed.y);
    vec2 xzDir = glm::normalize(xz);
    vec2 tyDir = glm::normalize(ty);

    float phi = xzDir.y >= 0 ? acos(xzDir.x) : glm::pi<float>() + acos(-xzDir.x);
    float theta = acos(-tyDir.y);
    return vec2(phi, theta);
}

vec2 toPolarRatio(vec2 polar)
{
    float phiRatio = polar.x / (2 * glm::pi<float>());
    float thetaRatio = polar.y / glm::pi<float>();
    return vec2(phiRatio, thetaRatio);
}

vec2 toPolarRatio(vec3 dir)
{
    return toPolarRatio(dirToPolar(dir));
}

ivec2 toPolarIndices(vec2 polarRatio, uint resolution)
{
    int w = polarRatio.x * (resolution - 1);
    int h = polarRatio.y * (resolution - 1);
    return ivec2(w, h);
}

int toPolarIndex(float polarRatio, uint resolution)
{
    return polarRatio * (resolution - 1);
}

bool isOnCyrcle(vec2 v, float r)
{
    return sqr(v.x) + sqr(v.y) - sqr(r) < eps;
}
bool isOnCyrcle(vec2 v, float r, vec2 p)
{
    return sqr(v.x - p.x) + sqr(v.y - p.y) - sqr(r) < eps;
}

bool isInSphere(vec3 v, float r, vec3 p)
{
    return sqr(v.x - p.x) + sqr(v.y - p.y) + sqr(v.z - p.z) <= sqr(r);
}

array<vec2, 2> cyrclesIntersections(float r1, float r2, float px)
{
    assert(std::abs(r1 - px) <= r2 && r1 > 0 && r2 >0 && r1 > r2);
    float k = sqr(px) + sqr(r1) - sqr(r2);
    float a = 4 * sqr(px);
    float b = - 4 * k * px;
    float x = -b / (2*a);
    float y = std::sqrt(sqr(r1) - sqr(x));
    return { vec2(x, y), vec2(x, -y) };
}

array<float, 2> orderExtendPolarRatios(array<float, 2> polarRatios)
{
    array<float, 2> ordered = polarRatios[0] < polarRatios[1] ? 
        array<float, 2>({ polarRatios[0], polarRatios[1] }):
        array<float, 2>({ polarRatios[1], polarRatios[0] });
    float dist1 = ordered[1] - ordered[0];
    float dist2 = 1 + ordered[0] - ordered[1];
    return dist1 <= dist2 ? ordered : array<float, 2>({ ordered[1], 1 + ordered[0] });
}

// first are horizontal, second vertical intersections, first min, then max
array<array<float, 2>, 2> spheresMinMaxPolarRatioIntersections(float r1, float r2, vec3 p2)
{
    bool isNorthPole = isInSphere(r1 * vec3(0,1,0), r2, p2);
    bool isSouthPole = isInSphere(r1 * vec3(0,-1,0), r2, p2);
    bool isPole = isNorthPole || isSouthPole;

    float p2_length = glm::length(p2);
    vec3 p2_normed = p2 / p2_length;

    vec3 r_normed = glm::normalize(glm::cross(p2_normed, vec3(0,1,0)));
    vec3 s_normed = glm::normalize(glm::cross(p2_normed, r_normed));

    array<vec2, 2> intersections = cyrclesIntersections(r1, r2, p2_length);

    array<float, 2> phiRes = isPole ?  array<float, 2>({ 0, 1 }) : orderExtendPolarRatios({
        toPolarRatio(intersections[0].x * p2_normed + intersections[0].y * r_normed).x,
        toPolarRatio(intersections[1].x * p2_normed + intersections[1].y * r_normed).x
    });
    array<float, 2> thetaRes = orderExtendPolarRatios({ 
        toPolarRatio(intersections[0].x * p2_normed + intersections[0].y * s_normed).y,
        toPolarRatio(intersections[1].x * p2_normed + intersections[1].y * s_normed).y
    });
    if(isNorthPole) {
        thetaRes = array<float, 2>({ std::min(thetaRes[0], thetaRes[1]), 1 });
    }
    if(isSouthPole) {
        thetaRes = array<float, 2>({ 0, std::max(thetaRes[0], thetaRes[1]) });
    }
    return { phiRes, thetaRes };
}

vector<u8> GenerateGaussDisksProjectedOnSphere(uint resolution, float radius, float sigma, bool cosUvs = false)
{
    const uint resMultiplier = 1;
    uint disks3dResolution = resolution * resMultiplier;
    uvec3 disksDim = uvec3(disks3dResolution);
    float disksRadius = radius * resolution;
    float disksSigma = sigma * resolution;
    
    const float tolerance = 0.001f;
    // exp(-r^2 / sigma^2) = tolerance
    //      -r^2 / sigma^2 = ln(tolerance)
    //      r = sqrt(-ln(tolerance) * sigma^2)
    float max_r = glm::sqrt(-glm::log(tolerance)) * disksSigma; // old just: 3*sigma
    int iMax_r = (int)max_r;
    spdlog::info("GenerateGaussDisksProjectedOnSphere started radius={}, sigma={}, max_r={}", disksRadius, disksSigma, max_r);
    
    array<float, 3> x_min = {0, 0, 0};
    array<float, 3> x_max = {(float)disksDim.x - 1.0f, (float)disksDim.y - 1.0f, (float)disksDim.z - 1.0f};
    spdlog::info("PoissonDiskSampling started");
    vector<array<float, 3>> samples = pds::PoissonDiskSampling((float)disksRadius, x_min, x_max);
    spdlog::info("PoissonDiskSampling finished");

    float sphereRadius = disks3dResolution / 2;
    vec3 sphereCenter = vec3(sphereRadius);
    uvec2 resDim = uvec2(resolution);
    vector<vec3> colors(resolution * resolution, vec3(0));
    vector<u8> result(3 * resolution * resolution, 0);

    fmt::print("drawing 3D gauss disks {:5.2f}%", 0.0);
    size_t sample_num = 1;
    for(const array<float, 3>& a_sample: samples) {
        vec3 sample = vec3(a_sample[0], a_sample[1], a_sample[2]);
        if(std::abs(glm::length(sample - sphereCenter) - sphereRadius) <= max_r) {
            vec3 sampleColor = vec3(rand()%255,rand()%255,rand()%255)/vec3(255,255,255);

            array<array<float, 2>, 2> minMaxPolarRatio = spheresMinMaxPolarRatioIntersections(sphereRadius, max_r, sample - sphereCenter);
            int minW = toPolarIndex(minMaxPolarRatio[0][0], resolution);
            int maxW = toPolarIndex(minMaxPolarRatio[0][1], resolution);
            int minH = toPolarIndex(minMaxPolarRatio[1][0], resolution);
            int maxH = toPolarIndex(minMaxPolarRatio[1][1], resolution);

            for(int h = std::max(minH, 0); h <= std::min(maxH, (int)resolution - 1); h++) {
                float tRatio = (h + 0.5f) / resolution;
                // TODO: maybe implement cosinus mapping, but needs proper mapping of closeTheta
                float theta = glm::pi<float>() * tRatio;
                float sinTheta = glm::sin(theta);
                float mCosTheta = -glm::cos(theta);
                for(int ww = std::max(minW, 0); ww <= std::min(maxW, (int)(2*resolution - 1)); ww++) {
                    int w = ww % resolution;
                    float pRatio = (w + 0.5f) / resolution;
                    float phi =  2 * glm::pi<float>() * pRatio;
                    float sinPhi = glm::sin(phi);
                    float cosPhi = glm::cos(phi);

                    vec3 spherePos = vec3(sinTheta * cosPhi, mCosTheta, sinTheta * sinPhi);
                    vec3 pos = (spherePos + vec3(1)) * vec3(0.5f) * vec3(disks3dResolution - 1);
                    size_t iCol = w + h * resolution;
                    colors[iCol] = lerp(colors[iCol], sampleColor, gauss(pos - sample, disksSigma));

                    size_t i = 3*(w + h * resolution);
                    result[i+0] = (u8)(255 * colors[iCol].r);
                    result[i+1] = (u8)(255 * colors[iCol].g);
                    result[i+2] = (u8)(255 * colors[iCol].b);
                }
            }
        }
        fmt::print("\rdrawing 3D gauss disks {:5.2f}%", 100.0 * sample_num / (double)samples.size());
        sample_num++;
    }
    fmt::print("\n");
    spdlog::info("GenerateGaussDisks3D finished");


    spdlog::info("GenerateGaussDisksProjectedOnSphere finished");

    return result;
}

array<array<float, 3>, 2> GetMeshMinMax(const vector<Mesh3DVertex>& vertices)
{   
    spdlog::info("GetMeshMinMax started");
    using num = std::numeric_limits<float>;
    array<float, 3> min = { num::infinity(), num::infinity(), num::infinity() };
    array<float, 3> max = { -num::infinity(), -num::infinity(), -num::infinity() };
    for(const Mesh3DVertex& vertex: vertices) {
        vec3 pos = vertex.position;
        for(int d = 0; d < 3; d++) {
            if(pos[d] < min[d]) { min[d] = pos[d]; }
            if(pos[d] > max[d]) { max[d] = pos[d]; }
        }
    }
    spdlog::info("GetMeshMinMax finished");
    return { min, max };
}

array<vec2, 2> GetTexCoordMinMax(const array<Mesh3DVertex, 3>& triangle)
{   
    using num = std::numeric_limits<float>;
    vec2 min = vec2(num::infinity(), num::infinity());
    vec2 max = vec2(-num::infinity(), -num::infinity());
    for(const Mesh3DVertex& vertex: triangle) {
        vec2 texCoord = vertex.texCoord;
        for(int d = 0; d < 2; d++) {
            if(texCoord[d] < min[d]) { min[d] = texCoord[d]; }
            if(texCoord[d] > max[d]) { max[d] = texCoord[d]; }
        }
    }
    return { min, max };
}

vector<GaussDisk> GenerateGaussDisks(array<array<float, 3>, 2> minMax, float radius)
{
    spdlog::info("GenerateGaussDisks started");
    spdlog::info("PoissonDiskSampling started");
    vector<array<float, 3>> samples = pds::PoissonDiskSampling(radius, minMax[0], minMax[1]);
    spdlog::info("PoissonDiskSampling finished");
    vector<GaussDisk> result;
    result.reserve(samples.size());
    for(const array<float, 3>& sample: samples) {
        GaussDisk disk = {
            vec3(sample[0], sample[1], sample[2]),
            vec3(rand()%255,rand()%255,rand()%255)/vec3(255,255,255)
        };
        result.push_back(disk);
    }
    spdlog::info("GenerateGaussDisks finished");
    return result;
}

ivec3 GetCellIndex(vec3 pos, vec3 min, float max_r)
{
    return (pos - min) / max_r;
}

vector3d<vector<cref<GaussDisk>>> CreateGaussDisksCells(const vector<GaussDisk>& gaussDisks, vec3 min, vec3 max, float max_r)
{
    spdlog::info("CreateGaussDisksCells started");
    vec3 size = max - min;
    uvec3 dim = glm::ceil(size / max_r);
    vector3d<vector<cref<GaussDisk>>> cells(dim);
    for(const GaussDisk& disk: gaussDisks) {
        ivec3 index = GetCellIndex(disk.pos, min, max_r);
        ivec3 i;
        for(i.z = index.z - 1; i.z <= index.z + 1; i.z++) {
            for(i.y = index.y - 1; i.y <= index.y + 1; i.y++) {
                for(i.x = index.x - 1; i.x <= index.x + 1; i.x++) {
                    if(in_range(i, dim)) {
                        cells[uvec3(i)].push_back(disk);
                    }
                }
            }
        }
    }
    spdlog::info("CreateGaussDisksCells finished");
    return cells;
}

ivec2 texCoordToInt(vec2 texCoord, uint resolution)
{
    ivec2 res = texCoord * (float)resolution;
    if(texCoord.x == 1) { res.x = resolution - 1; }
    if(texCoord.y == 1) { res.y = resolution - 1; }
    return res;
}

float lineSide(vec3 line, vec2 point)
{
    return glm::dot(vec2(line.x, line.y), point) + line.z;
}

vec3 lineFromPoints(vec2 p1, vec2 p2, vec2 dir)
{
    vec2 d = glm::normalize(p2 - p1);
    vec2 n = vec2(d.y, -d.x);
    vec3 params = vec3(n, -glm::dot(n, p1));
    return lineSide(params, dir) > 0 ? params : -params;
}

bool areTrianglesSeparateHalf(array<vec2, 3> tri1, array<vec2, 3> tri2)
{
    array<vec3, 3> lines = {
        lineFromPoints(tri1[0], tri1[1], tri1[2]),
        lineFromPoints(tri1[0], tri1[2], tri1[1]),
        lineFromPoints(tri1[1], tri1[2], tri1[0])
    };
    for(vec3 line: lines) {
        bool allSeparete = true;
        for(vec2 point: tri2) {
            if(lineSide(line, point) >= 0) {
                allSeparete = false;
            }
        }
        if(allSeparete) {
            return true;
        }
    }
    return false;
}

bool areTrianglesInCollision(array<vec2, 3> tri1, array<vec2, 3> tri2)
{
    return !(areTrianglesSeparateHalf(tri1, tri2) || areTrianglesSeparateHalf(tri2, tri1));
}

array<vec2, 4> getPixelUV(int w, int h, uint resolution)
{
    return {
        vec2(w + 0, h + 0) / vec2(resolution),
        vec2(w + 1, h + 0) / vec2(resolution),
        vec2(w + 1, h + 1) / vec2(resolution),
        vec2(w + 0, h + 1) / vec2(resolution)
    };
}

void forEachPixelInTriangle(vector<u8>& image, uint resolution, const array<Mesh3DVertex, 3>& triangle, std::function<array<u8,3>(vec3)> getColor)
{
    array<vec2, 3> triUV = {
        triangle[0].texCoord,
        triangle[1].texCoord,
        triangle[2].texCoord
    };
    mat3 toBaricentric = glm::inverse(mat3(
        vec3(1.0f, triangle[0].texCoord),
        vec3(1.0f, triangle[1].texCoord),
        vec3(1.0f, triangle[2].texCoord)
    ));
    array<vec2, 2> minMax = GetTexCoordMinMax(triangle);
    uvec2 min = glm::max(ivec2(0), texCoordToInt(minMax[0], resolution) - ivec2(1));
    uvec2 max = glm::min(ivec2(resolution - 1), texCoordToInt(minMax[1], resolution) + ivec2(1));
    for(int h = min.y; h <= max.y; h++) {
        for(int w = min.x; w <= max.x; w++) {
            array<vec2, 4> pixelUV = getPixelUV(w, h, resolution);
            bool inCollision = 
                areTrianglesInCollision(triUV, {pixelUV[0], pixelUV[1], pixelUV[2]}) ||
                areTrianglesInCollision(triUV, {pixelUV[0], pixelUV[2], pixelUV[3]});
            bool nextToCollision = false;
            if(!inCollision) {
                for(int dh = std::max(h - 1, 0); dh <= std::min(h + 1, (int)resolution - 1); dh++) {
                    for(int dw = std::max(w - 1, 0); dw <= std::min(w + 1, (int)resolution - 1); dw++) {
                        if(dh != h && dw != w) {
                            pixelUV = getPixelUV(dw, dh, resolution);
                            nextToCollision = 
                                areTrianglesInCollision(triUV, {pixelUV[0], pixelUV[1], pixelUV[2]}) ||
                                areTrianglesInCollision(triUV, {pixelUV[0], pixelUV[2], pixelUV[3]});
                            if(nextToCollision) {
                                goto endNextToCollisionCheck;
                            }
                        }
                    }
                }
            }
            endNextToCollisionCheck:
            if(inCollision || nextToCollision) {
                vec2 texCoord = (vec2(w, h) + vec2(0.5f)) / vec2(resolution);
                vec3 bar = toBaricentric * vec3(1.0f, texCoord);
                vec3 pos = mat3(triangle[0].position, triangle[1].position, triangle[2].position) * bar;
                int i = 3*(w + h * resolution);
                bool pixelNotSet = image[i+0] == 127 && image[i+1] == 127 && image[i+2] == 127;
                if(!(nextToCollision && !pixelNotSet)) {
                    array<u8, 3> color = getColor(pos);
                    for(int di = 0; di < 3; di++) {
                        image[i+di] = pixelNotSet ? color[di] : ((int)image[i+di] + (int)color[di]) / 2;
                    }
                }
            }
        }
    }
}

vector<u8> GenerateGaussDisksProjectedOnMesh(const IndexedVerticesMesh3D<u32>& mesh, uint resolution, float radius, float sigma)
{
    const float tolerance = 0.001f;
    // exp(-r^2 / sigma^2) = tolerance
    //      -r^2 / sigma^2 = ln(tolerance)
    //      r = sqrt(-ln(tolerance) * sigma^2)
    float max_r = glm::sqrt(-glm::log(tolerance)) * sigma; // old just: 3*sigma
    spdlog::info("GenerateGaussDisksProjectedOnMesh started radius={}, sigma={}, max_r={}", radius, sigma, max_r);
    array<array<float, 3>, 2> minMax = GetMeshMinMax(mesh.vertices);
    vector<GaussDisk> gaussDisks = GenerateGaussDisks(minMax, radius);
    vec3 min = vec3(minMax[0][0], minMax[0][1], minMax[0][2]);
    vec3 max = vec3(minMax[1][0], minMax[1][1], minMax[1][2]);
    vec3 size = max - min;
    vector3d<vector<cref<GaussDisk>>> cells = CreateGaussDisksCells(gaussDisks, min, max, max_r);

    vector<u8> image(3 * resolution * resolution, 127);

    fmt::print("drawing gauss disks on mesh {:5.2f}%", 0.0);
    for(int t = 0; t < mesh.indices.size(); t += 3) {
        array<Mesh3DVertex, 3> triangle = {
            mesh.vertices[mesh.indices[t + 0]],
            mesh.vertices[mesh.indices[t + 1]],
            mesh.vertices[mesh.indices[t + 2]],
        };
        forEachPixelInTriangle(image, resolution, triangle, [&](vec3 pos) -> array<u8, 3> {
            ivec3 cellIndex = GetCellIndex(pos, min, max_r);
            vec3 color = vec3(1);
            if(in_range(cellIndex, cells.dim())) {
                vector<cref<GaussDisk>>& disks = cells[uvec3(cellIndex)];
                for(const GaussDisk& disk: disks) {
                    if(glm::length(disk.pos - pos) <= max_r) {
                        color = lerp(color, disk.color, gauss(pos - disk.pos, sigma));
                    }
                }
            }
            return {(u8)(255 * color.r), (u8)(255 * color.g), (u8)(255 * color.b)}; 
        });
        fmt::print("\rdrawing gauss disks on mesh {:5.2f}%", 100.0 * t / (double)(mesh.indices.size() - 3));
    }
    fmt::print("\n");
    spdlog::info("GenerateGaussDisksProjectedOnMesh finished");
    return image;
}

int RunGaussDisks(int argc, char *argv[])
{
    spdlog::set_pattern("[%H:%M:%S.%e][%^%L%$] %v");

    ResourceStorage resStorage(unord_map<string, string>({
        {"engine", MORPH_ENGINE_RES},
        {"shared", MORPH_SHARED_RES}
    }));

    uint resolution = 1024;
    float radius = 0.01f;
    float sigma = 0.015f;

    string meshPath = "shared:meshes/golem.obj";
    IndexedVerticesMesh3D<u32> mesh = value_or_panic(
        ResourceManager::LoadMesh3D_OBJ(
            value_or_panic(
                resStorage.GetPath(meshPath), 
                "resource path {} does not exist", meshPath
            )
        ), 
        "failed to open file {}", meshPath
    );

    //vector<u8> result = GenerateGaussDisksProjectedOnSphere(resolution, radius, sigma, false);
    vector<u8> result = GenerateGaussDisksProjectedOnMesh(mesh, resolution, radius, sigma);
    
    Image2D disksImage = { 
        uvec2(resolution),
        result,
        TextureSizedFormat::RGB8
    };
    ResourceManager::SaveImage2D_PNG(disksImage, "gauss_disks.png");

    return EXIT_SUCCESS;
}

}


int main(int argc, char *argv[]) {
    return Morph::RunGaussDisks(argc, argv);
}