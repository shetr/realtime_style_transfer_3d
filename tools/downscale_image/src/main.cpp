#include <Core/Core.hpp>
#include <Resource/ResourceManager.hpp>

namespace Morph {

Image2D DownscaleImage(const Image2D& image)
{
    TextureDataFormat dataFormat = GraphicsEnums::GetTextureDataFormat(image.format);
    TextureFormat format = dataFormat.format;
    uint channels = static_cast<uint>(format)+1;

    Image2D resImage = {
        image.dim,
        vector<u8>(image.data.size()),
        image.format
    };

    uvec2 quarter = image.dim / uvec2(4);
    uvec2 threeQuarters = quarter * uvec2(3);

    for(uint h = 0; h < image.dim.y; ++h) {
        for(uint w = 0; w < image.dim.x; ++w) {
            for(uint ch = 0; ch < channels; ++ch) {
                uvec2 pos = uvec2(w, h);
                uint i = ch + w * channels + h * channels * image.dim.x;
                if(w < quarter.x || h < quarter.y || w >= threeQuarters.x || h >= threeQuarters.y) {
                    if(ch != 3) {
                        resImage.data[i] = 0;
                    } else {
                        resImage.data[i] = 255;
                    }
                } else {
                    uint sum = 0;
                    uvec2 inPos = (pos - quarter) * uvec2(2);
                    for(uint inH = inPos.y; inH <= inPos.y + 1; ++inH) {
                        for(uint inW = inPos.x; inW <= inPos.x + 1; ++inW) {
                            uint inI = ch + inW * channels + inH * channels * image.dim.x;
                            sum += image.data[inI];
                        }
                    }
                    resImage.data[i] = (u8)std::round(sum / 4.0);
                }
            }
        }
    }

    return resImage;
}

int RunDownscaleImage(int argc, char *argv[])
{
    spdlog::set_pattern("[%H:%M:%S.%e][%^%L%$] %v");
    if(argc < 3) {
        fmt::print("usage: downscale_image <levels> <images>\n");
        return EXIT_FAILURE;
    }
    string levelsArg = argv[1];
    
    int levels = std::atoi(levelsArg.c_str());
    if(levels == 0) {
        panic("invalid levels argument");
    }
    for(int i = 2; i < argc; i++) {
        string imagePath = argv[i];
        size_t dotPos = imagePath.find(".png");
        if (dotPos == std::string::npos) {
            panic("invalid file format, .png needed at: {}", imagePath);
        }
        string imagePathNoExt = imagePath.substr(0, dotPos);
        
        Image2D image = value_or_panic(ResourceManager::LoadImage2D_PNG(imagePath), "could not read image at {}", imagePath);
        for(int l = 1, s = 2; l <= levels; l++, s*=2) {
            image = DownscaleImage(image);
            ResourceManager::SaveImage2D_PNG(image, fmt::format("{}_{}.png", imagePathNoExt, s));
        }
    }
    return EXIT_SUCCESS;
}

}


int main(int argc, char *argv[]) {
    return Morph::RunDownscaleImage(argc, argv);
}