#include <StyleTransferApp.hpp>


int main(int argc, char *argv[]) {
    using namespace Morph::StyleTransferAppImpl;
    using namespace Morph;
    
    StyleTransferAppArgs args(argc, argv, "shared:configs/demo.json");
    StyleTransferApp app(args);
    app.Run();
}