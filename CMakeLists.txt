cmake_minimum_required(VERSION 3.11...3.16)

project(RealtimeStyleTransfer3D
    VERSION 0.1
    DESCRIPTION "Realtime style transfer on 3D scene with convolutional neural networks"
    LANGUAGES CXX C)

set(CMAKE_CXX_STANDARD 17)

if(CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME)

    set(CMAKE_CXX_EXTENSIONS OFF)

    set(CMAKE_POSITION_INDEPENDENT_CODE ON)

    set_property(GLOBAL PROPERTY USE_FOLDERS ON)

    add_compile_definitions(STYLE_TRANSFER_CMAKE_ROOT="${CMAKE_CURRENT_SOURCE_DIR}")

    add_compile_definitions(
        $<$<CONFIG:Debug>:MORPH_DEBUG>
        $<$<CONFIG:Release>:MORPH_RELEASE>
    )

    if(ENABLE_DEPLOY)
        add_compile_definitions(MORPH_DEPLOY)
        add_compile_definitions(MORPH_ENGINE_RES="res/engine/")
        add_compile_definitions(MORPH_SHARED_RES="res/shared/")
    else()
        add_compile_definitions(MORPH_ENGINE_RES="${CMAKE_CURRENT_SOURCE_DIR}/extern/morphengine/engine/res/")
        add_compile_definitions(MORPH_SHARED_RES="${CMAKE_CURRENT_SOURCE_DIR}/res/")
    endif()

    if(ENABLE_PROFILING)
        add_compile_definitions(MORPH_PROFILE)
    endif()

    if(CMAKE_SYSTEM_NAME MATCHES "Windows")
        add_compile_definitions(MORPH_WINDOWS)
    endif()
    if(CMAKE_SYSTEM_NAME MATCHES "Linux")
        add_compile_definitions(MORPH_LINUX)
    endif()
    if(CMAKE_SYSTEM_NAME MATCHES "Darwin")
        add_compile_definitions(MORPH_APPLE)
    endif()

endif()

add_subdirectory(app)
add_subdirectory(style_transfer)
add_subdirectory(tools)
add_subdirectory(tests/unit_tests)


add_subdirectory(extern/morphengine/engine)
add_subdirectory(extern/morphengine/extern/glad)
add_subdirectory(extern/morphengine/extern/glfw)
add_subdirectory(extern/morphengine/extern/glm)
add_subdirectory(extern/morphengine/extern/fmt)
add_subdirectory(extern/morphengine/extern/spdlog)
set(GOOGLETEST_VERSION 1.10.0)
add_subdirectory(extern/morphengine/extern/googletest/googletest)
add_subdirectory(extern/protobuf/cmake)

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)
