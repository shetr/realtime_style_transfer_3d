#version 450 core

layout(local_size_x = GROUP_SIZE_X, local_size_y = GROUP_SIZE_Y, local_size_z = GROUP_SIZE_Z) in;

uniform float u_eps;

layout (packed, binding = 0) uniform l_meansLayout
{
    float u_means[NUM_FEATURES];
};
layout (packed, binding = 1) uniform l_varsLayout
{
    float u_vars[NUM_FEATURES];
};
layout (packed, binding = 2) uniform l_gammasLayout
{
    float u_gammas[NUM_FEATURES];
};
layout (packed, binding = 3) uniform l_betasLayout
{
    float u_betas[NUM_FEATURES];
};

readonly layout(r32f) uniform image3D u_input_data;
writeonly layout(r32f) uniform image3D u_output_data;

void main() {
    ivec3 coord = ivec3(gl_GlobalInvocationID);
    float value = imageLoad(u_input_data, coord).x;
    value = u_gammas[coord.z] * (value - u_means[coord.z]) / sqrt(u_vars[coord.z] + u_eps) + u_betas[coord.z];
    imageStore(u_output_data, coord, vec4(value));
}
