#version 450 core

layout(local_size_x = GROUP_SIZE_X, local_size_y = GROUP_SIZE_Y, local_size_z = GROUP_SIZE_Z) in;

uniform vec3 u_mean;
uniform vec3 u_std;

readonly layout(r32f) uniform image3D u_input_data;
writeonly layout(rgba8) uniform image2D u_output_data;

void main() {
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);
    vec4 pixel = vec4(1);
    pixel.x = (u_std.x * imageLoad(u_input_data, ivec3(pixel_coords, 0)).x) + u_mean.x;
    pixel.y = (u_std.y * imageLoad(u_input_data, ivec3(pixel_coords, 1)).x) + u_mean.y;
    pixel.z = (u_std.z * imageLoad(u_input_data, ivec3(pixel_coords, 2)).x) + u_mean.z;
    imageStore(u_output_data, pixel_coords, pixel);
}