#ifndef STYLE_TRANSFER_APP_ARGS_HPP
#define STYLE_TRANSFER_APP_ARGS_HPP

#include "StyleTransferAppImpl/Config.hpp"
#include "StyleTransferAppImpl/ConfigJson.hpp"

#include <iostream>

namespace Morph {

struct StyleTransferAppArgs
{
private:
    Void d_init;
public:
    StyleTransferAppImpl::ConfigVar config;

    StyleTransferAppArgs(const StyleTransferAppImpl::ConfigVar& _config, bool configSaveIgnoreDefaults = true)
        :
        d_init([&]() -> Void {
            InitStart(configSaveIgnoreDefaults);
            return Void();
        }()),
        config(_config)
    {}

    StyleTransferAppArgs(string configPath, bool configSaveIgnoreDefaults = true)
        :
        d_init([&]() -> Void {
            InitStart(configSaveIgnoreDefaults);
            return Void();
        }()),
        config(value_or_panic(StyleTransferAppImpl::ConfigJson::LoadConfig(configPath)))
    {}

    StyleTransferAppArgs(int argc, char *argv[], 
        StyleTransferAppImpl::ConfigVar defConfig = StyleTransferAppImpl::InteractiveConfig(),
        bool configSaveIgnoreDefaults = true)
        :
        d_init([&]() -> Void {
            InitStart(configSaveIgnoreDefaults);
            CheckArgs(argc, argv);
            return Void();
        }()),
        config( argc == 2 ?
            value_or_panic(StyleTransferAppImpl::ConfigJson::LoadConfig(argv[1])) :
            defConfig
        )
    {}

    StyleTransferAppArgs(int argc, char *argv[], 
        string defConfig, bool configSaveIgnoreDefaults = true)
        :
        d_init([&]() -> Void {
            InitStart(configSaveIgnoreDefaults);
            CheckArgs(argc, argv);
            return Void();
        }()),
        config( argc == 2 ?
            value_or_panic(StyleTransferAppImpl::ConfigJson::LoadConfig(argv[1])) :
            value_or_panic(StyleTransferAppImpl::ConfigJson::LoadConfig(defConfig))
        )
    {}
private:
    static void InitStart(bool configSaveIgnoreDefaults) {
        spdlog::set_pattern("[%H:%M:%S.%e][%^%L%$] %v");
        spdlog::info("initialization started");
        StyleTransferAppImpl::Globals::Init(unord_map<string, string>({
            {"engine", MORPH_ENGINE_RES},
            {"shared", MORPH_SHARED_RES}
        }));
        StyleTransferAppImpl::Globals::Get()->configSaveIgnoreDefaults() = configSaveIgnoreDefaults;
    }
    static void CheckArgs(int argc, char *argv[]) {
        if(argc > 2) {
            std::cout << "Usage:" << std::endl;
            std::cout << argv[0] << " " << std::endl;
            std::cout << argv[0] << " config_file" << std::endl;
            panic("Wrong number of arguments: {}", argc - 1);
        }
    }
};

}


#endif // STYLE_TRANSFER_APP_ARGS_HPP