#ifndef STYLE_TRANSFER_APP_HPP
#define STYLE_TRANSFER_APP_HPP

#include <App/WindowApp.hpp>
#include <Resource/ResourceManager.hpp>
#include <Resource/GraphicsProgramCompiler.hpp>
#include <Resource/Common.hpp>

#include "StyleTransferAppImpl/Config.hpp"
#include "StyleTransferAppImpl/Scene.hpp"
#include "StyleTransferAppImpl/Control.hpp"
#include "StyleTransferAppImpl/GUI.hpp"

#include "StyleTransferAppArgs.hpp"

namespace Morph {

class StyleTransferApp : public WindowApp
{
private:
    StyleTransferAppImpl::ConfigVar m_configVar;
    cref<StyleTransferAppImpl::Config> m_config;
    Void d_setLogLevel;
    ImGuiContext m_imguiContext;
    GraphicsSettingsApplier m_graphicsSettingsApplier;
    Void d_initRender;
protected:
    GraphicsProgramCompiler m_progCompiler;
    CommonResources m_commonResources;
    bool m_updateStyle;
    bool m_forceUpdateStyle;
    StyleTransferAppImpl::Scene m_scene;
    StyleTransferAppImpl::GUI m_gui;
    StyleTransferAppImpl::Control m_control;
public:
    StyleTransferApp(const StyleTransferAppArgs& appArgs);
    virtual ~StyleTransferApp();
protected:
    virtual void ApplyStyle();
    // time in ms
    virtual float ApplyStyleTime();

    inline ImGuiContext& imguiContext() { return m_imguiContext; }
    inline ResourceStorage& resStorage() { return StyleTransferAppImpl::Globals::Get()->resStorage(); }
    inline GraphicsProgramCompiler& progCompiler() { return m_progCompiler; }
    inline CommonResources& commonResources() { return m_commonResources; }
    inline StyleTransferAppImpl::Scene& scene() { return m_scene; }
private:
    void RunIter(f64 lastIterTime, f64 lastFrameTime) override;

    GraphicsSettingsApplier ApplyGraphicsSettings();
    void InitRender();

    WindowAppConfig GetWindowConfig(const StyleTransferAppImpl::ConfigVar& appConfig);

    const StyleTransferAppImpl::Config& UnVar(const StyleTransferAppImpl::ConfigVar& appConfig);
};

}


#endif // STYLE_TRANSFER_APP_HPP