#include "ConfigJson.hpp"

#include "ConfigJsonImpl.hpp"

namespace Morph { namespace StyleTransferAppImpl {

Result<Void, SaveConfigError> ConfigJson::SaveConfig(const ConfigVar& config, const string& path)
{
    bool saved = resStorage().WriteToFileResOrNormal(path, ConfigToJSON(config));
    if(saved) {
        return Void();
    }
    return SaveConfigError(path);
}
Result<Void, SaveConfigError> ConfigJson::SaveSceneConfig(const SceneConfig& sceneConfig, const string& path)
{
    bool saved = resStorage().WriteToFileResOrNormal(path, SceneConfigToJSON(sceneConfig));
    if(saved) {
        return Void();
    }
    return SaveConfigError(path);
}

Result<ConfigVar, LoadConfigError> ConfigJson::LoadConfig(const string& path)
{
    using ResType = Result<ConfigVar, LoadConfigError>;
    opt<string> maybeConfigJson = resStorage().ReadFileResOrNormal(path);
    if(maybeConfigJson.has_value()) {
        string& configJson = maybeConfigJson.value();
        return match(ConfigFromJSON(configJson),
            [](ConfigVar config) -> ResType {
                return config;
            },
            [&](ParseConfigError error) -> ResType {
                return LoadConfigError(path, error.message());
            }
        );
    }
    return LoadConfigError(path, "failed to read from the file");
}
Result<SceneConfig, LoadConfigError> ConfigJson::LoadSceneConfig(const string& path)
{
    using ResType = Result<SceneConfig, LoadConfigError>;
    opt<string> maybeConfigJson = resStorage().ReadFileResOrNormal(path);
    if(maybeConfigJson.has_value()) {
        string& configJson = maybeConfigJson.value();
        return match(SceneConfigFromJSON(configJson),
            [](SceneConfig config) -> ResType {
                return config;
            },
            [&](ParseConfigError error) -> ResType {
                return LoadConfigError(path, error.message());
            }
        );
    }
    return LoadConfigError(path, "failed to read from the file");
}

string ConfigJson::ConfigToJSON(const ConfigVar& configVar)
{
    return match(configVar,
        [&](const InteractiveConfig& config) -> string {
            return InteractiveConfigToJSON(config);
        },
        [&](const TestConfig& config) -> string {
            return TestConfigToJSON(config);
        }
    );
}
string ConfigJson::InteractiveConfigToJSON(const InteractiveConfig& config)
{
    json configJson = config;
    configJson["type"] = "INTERACTIVE";
    return configJson.dump(4);
}
string ConfigJson::TestConfigToJSON(const TestConfig& config)
{
    json configJson = config;
    configJson["type"] = "TEST";
    return configJson.dump(4);
}
string ConfigJson::SceneConfigToJSON(const SceneConfig& sceneConfig)
{
    json sceneConfigJson = sceneConfig;
    return sceneConfigJson.dump(4);
}

Result<InteractiveConfig, ParseConfigError> InteractiveConfigFromJSON(const json& configJson);
Result<TestConfig, ParseConfigError> TestConfigFromJSON(const json& configJson);

Result<ConfigVar, ParseConfigError> ConfigJson::ConfigFromJSON(const string& configJsonStr)
{
    using ResType = Result<ConfigVar, ParseConfigError>;
    try {
        json configJson = json::parse(configJsonStr);
        string type = configJson.at("type").get<string>();
        if(type == "INTERACTIVE") {
            return match(InteractiveConfigFromJSON(configJson),
                [&](const InteractiveConfig& config) -> ResType {
                    return config;
                },
                [&](const ParseConfigError& error) -> ResType {
                    return error;
                }
            );
        } else if (type == "TEST") {
            return match(TestConfigFromJSON(configJson),
                [&](const TestConfig& config) -> ResType {
                    return config;
                },
                [&](const ParseConfigError& error) -> ResType {
                    return error;
                }
            );
        }
        return ParseConfigError("invalid config type");
    } catch (std::exception& e) {
        return ParseConfigError(e.what());
    }
}
Result<SceneConfig, ParseConfigError> ConfigJson::SceneConfigFromJSON(const string& sceneConfigJsonStr)
{
    try {
        json sceneConfigJson = json::parse(sceneConfigJsonStr);
        return sceneConfigJson.get<SceneConfig>();
    } catch (std::exception& e) {
        return ParseConfigError(e.what());
    }
}

Result<InteractiveConfig, ParseConfigError> InteractiveConfigFromJSON(const json& configJson)
{
    try {
        return configJson.get<InteractiveConfig>();
    } catch (std::exception& e) {
        return ParseConfigError(e.what());
    }
}
Result<TestConfig, ParseConfigError> TestConfigFromJSON(const json& configJson)
{
    try {
        return configJson.get<TestConfig>();
    } catch (std::exception& e) {
        return ParseConfigError(e.what());
    }
}



}}