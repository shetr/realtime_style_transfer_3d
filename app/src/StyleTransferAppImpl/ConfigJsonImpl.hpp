#ifndef STYLE_TRANSFER_APP_IMPL_CONFIG_JSON_IMPL_HPP
#define STYLE_TRANSFER_APP_IMPL_CONFIG_JSON_IMPL_HPP

#include "Config.hpp"

#include <nlohmann/json.hpp>

using json = nlohmann::json;

namespace Morph { namespace StyleTransferAppImpl {


class ConfigParseException : public std::exception
{
    std::string m_message;
  public:
    ConfigParseException(std::string message) : m_message(message) {}

    inline const char* what() const noexcept override{
        return m_message.c_str();
    }

};

NLOHMANN_JSON_SERIALIZE_ENUM( OutputType, {
    {OutputType::STYLE, "STYLE"},
    {OutputType::NORMALS, "NORMALS"},
    {OutputType::LIGHT_DIR, "LIGHT_DIR"},
    {OutputType::MASK, "MASK"},
    {OutputType::INPUT, "INPUT"},
    {OutputType::INPUT_DISPLAY, "INPUT_DISPLAY"},
    {OutputType::FLAT_TEXTURE, "FLAT_TEXTURE"},
    {OutputType::SPHERE_TEXTURE, "SPHERE_TEXTURE"}
})

NLOHMANN_JSON_SERIALIZE_ENUM( StyleAdditionalInput, {
    {StyleAdditionalInput::NONE, "NONE"},
    {StyleAdditionalInput::NORMALS, "NORMALS"},
    {StyleAdditionalInput::LIGTH_DIR, "LIGTH_DIR"},
    {StyleAdditionalInput::NORMALS_AND_LIGHT_DIR, "NORMALS_AND_LIGHT_DIR"}
})

NLOHMANN_JSON_SERIALIZE_ENUM( LightType, {
    {LightType::NONE, "NONE"},
    {LightType::DIR, "DIR"},
    {LightType::POINT, "POINT"},
    {LightType::SPOT, "SPOT"}
})

void to_json(json& configJson, const InteractiveConfig& config);
void from_json(const json& configJson, InteractiveConfig& config);

void to_json(json& configJson, const TestConfig& config);
void from_json(const json& configJson, TestConfig& config);

void to_json(json& sceneConfigJson, const SceneConfig& sceneConfig);
void from_json(const json& sceneConfigJson, SceneConfig& sceneConfig);

bool is_equal(SceneConfig o1, SceneConfig o2);

void to_json(json& j, const StyleData& v);
void from_json(const json& j, StyleData& v);
void to_json(json& j, const MeshData& v);
void from_json(const json& j, MeshData& v);
void to_json(json& j, const ObjectData& v);
void from_json(const json& j, ObjectData& v);
void to_json(json& j, const CameraData& v);
void from_json(const json& j, CameraData& v);

bool is_equal(ObjectData o1, ObjectData o2);
bool is_equal(CameraData o1, CameraData o2);

}}

namespace Morph {

void to_json(json& j, const PerspectiveCamera3D& v);
void from_json(const json& j, PerspectiveCamera3D& v);
void to_json(json& j, const TransformRotAngleAxis& v);
void from_json(const json& j, TransformRotAngleAxis& v);
void to_json(json& j, const TransformRotEuler& v);
void from_json(const json& j, TransformRotEuler& v);
void to_json(json& j, const Light& v);
void from_json(const json& j, Light& v);
void to_json(json& j, const Material& v);
void from_json(const json& j, Material& v);

bool is_equal(PerspectiveCamera3D o1, PerspectiveCamera3D o2);
bool is_equal(TransformRotAngleAxis o1, TransformRotAngleAxis o2);
bool is_equal(TransformRotEuler o1, TransformRotEuler o2);
bool is_equal(Light o1, Light o2);
bool is_equal(Material o1, Material o2);

}

namespace spdlog { namespace level {

NLOHMANN_JSON_SERIALIZE_ENUM( level_enum, {
    {trace, "trace"},
    {debug, "debug"},
    {info, "info"},
    {warn, "warn"},
    {err, "err"},
    {critical, "critical"},
    {off, "off"},
    {n_levels, "n_levels"}
})

}}

namespace glm {

    void to_json(json& j, const uvec2& v);
    void from_json(const json& j, uvec2& v);
    void to_json(json& j, const vec3& v);
    void from_json(const json& j, vec3& v);
    void to_json(json& j, const vec4& v);
    void from_json(const json& j, vec4& v);
    void to_json(json& j, const quat& q);
    void from_json(const json& j, quat& q);

}

#endif // STYLE_TRANSFER_APP_IMPL_CONFIG_JSON_IMPL_HPP