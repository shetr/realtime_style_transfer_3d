#include "Utils.hpp"

namespace Morph { namespace StyleTransferAppImpl {


const uint imguiPadding = 8;
const uint imguiWindowSize = 350; // TODO: move to config

uint GetImguiWindowOffset()
{
    return imguiPadding + imguiWindowSize;
}

uvec2 GetLeftFramebufferOffset(bool showGUI)
{
    uvec2 res = uvec2(imguiPadding);
    if(showGUI) {
        res.x += imguiWindowSize;
        res.x += imguiPadding;
    }
    return res;
}

uvec2 GetShowGUIButtonSize()
{
    uvec2 buttonSize = uvec2(64, 22);
    return buttonSize + uvec2(imguiPadding * 2);
}

uvec2 ComputeWindowSize(uvec2 renderSize, bool sideBySize, bool showGUI)
{
    uvec2 res = renderSize;
    if(sideBySize) {
        res.x *= 2; 
        res.x += imguiPadding;
    }
    if(showGUI) {
        res.x += imguiWindowSize;
        res.x += imguiPadding;
    }
    res.x += 2*imguiPadding;
    res.y += 2*imguiPadding;
    return res;
}

}}