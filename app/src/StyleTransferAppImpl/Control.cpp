#include "Control.hpp"

#include <Window/Manager.hpp>

#include "Utils.hpp"

namespace Morph { namespace StyleTransferAppImpl {

Control::Control(
    WindowManager& windowManager,
    Window& window,
    Scene& scene,
    GUI& gui,
    bool& updateStyle,
    bool& forceUpdateStyle,
    bool ignoreKeyboardInput)
    :
    m_windowManager(windowManager),
    m_window(window),
    m_scene(scene),
    m_gui(gui),
    m_updateStyle(updateStyle),
    m_forceUpdateStyle(forceUpdateStyle),
    m_ignoreKeyboardInput(ignoreKeyboardInput),
    m_lastCursorPos(GetCursorPosEye()),
    m_keyEventAttacher(&windowManager, this, &Control::OnKeyEvent),
    m_scrollEventAttacher(&windowManager, this, &Control::OnScrollEvent),
    m_mouseButtonEventAttacher(&windowManager, this, &Control::OnMouseButtonEvent)
{
}

void Control::Update(f64 lastIterTime, f64 lastFrameTime)
{
    float deltaTime = (float)lastIterTime;
    float aspectRatio = m_scene->aspectRatio();

    quat cameraRot(m_scene->cameraData().transform.rot);
    vec3 upDir = vec3(0,1,0);
    vec3 leftDirTilted = glm::rotate(cameraRot, vec3(1,0,0));
    vec3 forwardDirTilted = glm::rotate(cameraRot, vec3(0,0,-1));
    vec3 upDirTilted = glm::rotate(cameraRot, vec3(0,1,0));
    vec3 forwardDirProjected = glm::normalize(glm::cross(upDir, leftDirTilted));

    float scrollMoveSpeed = m_scene->config().cameraData.scrollMoveSpeed;
    float mouseMoveSpeed = m_scene->config().cameraData.mouseMoveSpeed;
    float mouseRotSpeed = m_scene->config().cameraData.mouseRotSpeed;
    dvec2 cursorPos = GetCursorPosEye();
    vec2 cursorMoveDir = vec2(cursorPos - m_lastCursorPos);

    bool moved = false;

    if(!m_ignoreKeyboardInput) {
        // Keyboard movement
        if(m_window->IsKeyPressed(Key::A)) {
            m_scene->cameraData().transform.pos -= leftDirTilted * m_scene->cameraData().moveSpeed * deltaTime;
            moved = true;
        }
        if(m_window->IsKeyPressed(Key::D)) {
            m_scene->cameraData().transform.pos += leftDirTilted * m_scene->cameraData().moveSpeed * deltaTime;
            moved = true;
        }
        if(m_window->IsKeyPressed(Key::S)) {
            m_scene->cameraData().transform.pos -= forwardDirProjected * m_scene->cameraData().moveSpeed * deltaTime;
            moved = true;
        }
        if(m_window->IsKeyPressed(Key::W)) {
            m_scene->cameraData().transform.pos += forwardDirProjected * m_scene->cameraData().moveSpeed * deltaTime;
            moved = true;
        }
        if(m_window->IsKeyPressed(Key::LEFT_SHIFT)) {
            m_scene->cameraData().transform.pos -= upDir * m_scene->cameraData().moveSpeed * deltaTime;
            moved = true;
        }
        if(m_window->IsKeyPressed(Key::SPACE)) {
            m_scene->cameraData().transform.pos += upDir * m_scene->cameraData().moveSpeed * deltaTime;
            moved = true;
        }
        if(m_window->IsKeyPressed(Key::Q) || m_window->IsKeyPressed(Key::LEFT)) {
            m_scene->cameraData().transform.rot.y += m_scene->cameraData().rotSpeed * deltaTime;
            moved = true;
        }
        if(m_window->IsKeyPressed(Key::E) || m_window->IsKeyPressed(Key::RIGHT)) {
            m_scene->cameraData().transform.rot.y -= m_scene->cameraData().rotSpeed * deltaTime;
            moved = true;
        }
        if(m_window->IsKeyPressed(Key::DOWN) && 
            m_scene->cameraData().transform.rot.x > m_maxDownRot) {
            m_scene->cameraData().transform.rot.x -= m_scene->cameraData().rotSpeed * deltaTime;
            moved = true;
        }
        if(m_window->IsKeyPressed(Key::UP) && 
            m_scene->cameraData().transform.rot.x < m_maxUpRot) {
            m_scene->cameraData().transform.rot.x += m_scene->cameraData().rotSpeed * deltaTime;
            moved = true;
        }
        // mouse movement
        if(m_scrollOffsetAccumulator != 0) {
            m_scene->cameraData().transform.pos += forwardDirTilted * scrollMoveSpeed * m_scrollOffsetAccumulator * deltaTime;
            m_scrollOffsetAccumulator = 0;
            moved = true;
        }
        if(m_mouseMoving) {
            m_scene->cameraData().transform.pos += mat2x3(leftDirTilted, upDirTilted) * (-cursorMoveDir) * mouseMoveSpeed * deltaTime;
        }
        if(m_mouseRotating) {
            m_scene->cameraData().transform.rot.y += -cursorMoveDir.x * mouseRotSpeed * deltaTime;
            if(!((cursorMoveDir.y < 0 && m_scene->cameraData().transform.rot.x <= m_maxDownRot) ||
                (cursorMoveDir.y > 0 && m_scene->cameraData().transform.rot.x >= m_maxUpRot))) {
                m_scene->cameraData().transform.rot.x += (cursorMoveDir.y) * mouseRotSpeed * deltaTime;
            }
        }
        if(m_mouseMoving || m_mouseRotating) {
            moved = true;
        }

        // correct rotations
        if(m_scene->cameraData().transform.rot.y > glm::pi<float>()) {
            m_scene->cameraData().transform.rot.y -= 2*glm::pi<float>();
        }
        if(m_scene->cameraData().transform.rot.y <= -glm::pi<float>()) {
            m_scene->cameraData().transform.rot.y += 2*glm::pi<float>();
        }
        if(m_scene->cameraData().transform.rot.x < m_maxDownRot) {
            m_scene->cameraData().transform.rot.x = m_maxDownRot;
        }
        if(m_scene->cameraData().transform.rot.x > m_maxUpRot) {
            m_scene->cameraData().transform.rot.x = m_maxUpRot;
        }
        // mouse correction
        if((m_mouseMoving || m_mouseRotating) && !IsCusorInsideLeftFramebuffer()) {
            dvec2 cursorPosScreen = KeepCursorInLeftFramebuffer();
            m_window->SetCursorPosScreen(cursorPosScreen);
            cursorPos = PosToEyeCoords(cursorPosScreen);
        }
        m_lastCursorPos = cursorPos;
    }
    if(m_gui->valuesChanged()) {
        moved = true;
    } 

    if(moved) {
        m_isMoving = true;
        m_moveTimeTolerance = 0.25f;
    }
    else if(m_isMoving && !moved) {
        if(m_moveTimeTolerance > 0) {
            m_moveTimeTolerance -= deltaTime;
        } else {
            m_isMoving = false;
            m_updateStyle.set(true);
        }
    }
}

dvec2 Control::GetCursorPosEye()
{
    return PosToEyeCoords(m_window->GetCursorPosScreen());
}

dvec2 Control::PosToEyeCoords(dvec2 pos)
{
    dvec2 min = GetLeftFramebufferOffset(m_gui->show());
    dvec2 size = m_scene->textureDim();
    return dvec2(2, -2) * (pos - min) / size + dvec2(-1, 1);
}

dvec2 Control::KeepCursorInLeftFramebuffer()
{
    dvec2 cursorPos = m_window->GetCursorPosScreen();
    ivec2 min = GetLeftFramebufferOffset(m_gui->show());
    ivec2 size = m_scene->textureDim();
    ivec2 max = min + size;
    dvec2 relPos = cursorPos - dvec2(min);
    ivec2 iRelPos = relPos;
    dvec2 dRelPos = relPos - dvec2(iRelPos);

    for(int d = 0; d < 2; d++) {
        if(relPos[d] >= size[d]) {
            cursorPos[d] = min[d] + (iRelPos[d] % size[d]) + dRelPos[d];
        } else if(relPos[d] < 0) {
            int negModulo = std::abs(iRelPos[d]) % size[d];
            cursorPos[d] = min[d] + (negModulo == 0 ? 0 : (size[d] - negModulo)) + dRelPos[d];
        }
    }
    return cursorPos;
}

bool Control::IsCusorInsideLeftFramebuffer()
{
    dvec2 cursorPos = m_window->GetCursorPosScreen();
    uvec2 min = GetLeftFramebufferOffset(m_gui->show());
    uvec2 max = min + m_scene->textureDim();
    return cursorPos.x >= min.x && cursorPos.x < max.x && cursorPos.y >= min.y && cursorPos.y < max.y;
}

bool Control::IsCusorInsideShowGUIButton()
{
    if(m_gui->show()) {
        return false;
    }
    dvec2 cursorPos = m_window->GetCursorPosScreen();
    uvec2 min = GetLeftFramebufferOffset(m_gui->show());
    uvec2 max = min + GetShowGUIButtonSize();
    return cursorPos.x >= min.x && cursorPos.x < max.x && cursorPos.y >= min.y && cursorPos.y < max.y;
}

void Control::OnKeyEvent(const KeyEvent& event)
{
    if(!m_ignoreKeyboardInput && event.action == KeyAction::PRESS) {

        if(event.key == Key::G) {
            m_gui->show() = !m_gui->show();
            m_gui->ResizeWindow();
        }

        if(event.key == Key::L) {
            m_gui->lazyUpdate() = true;
        }
        if(event.key == Key::U) {
            m_forceUpdateStyle.set(true);
        }
    }
}

void Control::OnScrollEvent(const ScrollEvent& event)
{
    if(!m_ignoreKeyboardInput && IsCusorInsideLeftFramebuffer()) {
        float offset = event.offset.y;
        m_scrollOffsetAccumulator += offset;
    }
}

void Control::OnMouseButtonEvent(const MouseButtonEvent& event)
{
    if(!m_ignoreKeyboardInput) {
        if(event.pressed) {
            if(IsCusorInsideLeftFramebuffer() && !IsCusorInsideShowGUIButton()) {
                if(event.button == MouseButton::LEFT_1) {
                    m_mouseMoving = true;
                }
                if(event.button == MouseButton::MIDDLE_3) {
                    m_mouseRotating = true;
                }
            }
        } else {
            if(event.button == MouseButton::LEFT_1) {
                m_mouseMoving = false;
            }
            if(event.button == MouseButton::MIDDLE_3) {
                m_mouseRotating = false;
            }
        }
    }
}

}}