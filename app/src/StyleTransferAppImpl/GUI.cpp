#include "GUI.hpp"

#include <Resource/ResourceManager.hpp>

#include "Utils.hpp"
#include "ConfigJson.hpp"

namespace Morph { namespace StyleTransferAppImpl {

GUI::GUI(
        const ConfigVar& configVar,
        GraphicsContext& context,
        string title,
        bool show,
        Window& window,
        ImGuiContext& imguiContext,
        Scene& scene,
        bool lazyUpdate,
        bool& forceUpdateStyle,
        bool showReference
    )
    : m_configVar(configVar),
    m_context(context),
    m_title(title + " settings"),
    m_show(show),
    m_window(window),
    m_imguiContext(imguiContext),
    m_scene(scene),
    m_lazyUpdate(lazyUpdate),
    m_forceUpdateStyle(forceUpdateStyle),
    m_showReference(showReference),
    m_inScreenshot(scene.inRawTexture(), "in"),
    m_outScreenshot(scene.outTexture(), "out"),
    m_normalsScreenshot(scene.normalsTexture(), "normals"),
    m_lightDirScreenshot(scene.lightDirTexture(), "lightDir"),
    m_maskScreenshot(scene.maskTexture(), "mask"),
    m_inDisplayScreenshot(scene.inDisplayTexture(), "inDisplay"),
    m_flatTextureScreenshot(scene.flatTexture(), "flatTexture"),
    m_screenshots({
        m_inScreenshot,
        m_outScreenshot,
        m_normalsScreenshot,
        m_lightDirScreenshot,
        m_maskScreenshot,
        m_inDisplayScreenshot,
        m_flatTextureScreenshot
    })
{

    ImGuiStyle& style = ImGui::GetStyle();
    style.WindowRounding = 0;
}

void GUI::Display(float styleTime, float lastIterTime)
{
    m_valuesChanged = false;

    FramebufferBinder framebufferBinder = m_context->BindFramebuffer(m_context->GetDefaultFramebuffer());
    framebufferBinder.Clear();

    ImGuiDraw imguiDraw = m_imguiContext->Draw();

    ImGuiWindowFlags flags = 
        ImGuiWindowFlags_NoDecoration |
        ImGuiWindowFlags_NoMove |
        ImGuiWindowFlags_NoResize |
        ImGuiWindowFlags_NoSavedSettings |
        ImGuiWindowFlags_NoBringToFrontOnFocus;

    const ImGuiViewport* viewport = ImGui::GetMainViewport();
    ImGui::SetNextWindowPos(viewport->Pos);
    ImGui::SetNextWindowSize(viewport->Size);

    float renderTime = (float)m_scene->renderTimer().GetAvgTime() / 1000000.0f;
    float displayTime = (float)m_scene->displayTimer().GetAvgTime() / 1000000.0f;
    
    ImGui::Begin(m_title.c_str(), (bool*)0, flags);

    if(m_show)
    {
        ImGui::BeginChild("SettingsUI", ImVec2(350, 0), true);
        if(ImGui::Button("Hide GUI")) {
            m_show = false;
            //ExpandCollapseGUI();
            ResizeWindow();
        }

        if (ImGui::BeginTabBar("##TabBar"))
        {
            if (ImGui::BeginTabItem("Scene")) {
                if (ImGui::CollapsingHeader("Rendering", ImGuiTreeNodeFlags_DefaultOpen)) {
                    RenderingHeader();
                }
                if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
                    CameraHeader();
                }
                if (ImGui::CollapsingHeader("Object", ImGuiTreeNodeFlags_DefaultOpen)) {
                    ObjectHeader();
                }
                if (ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen)) {
                    LightHeader();
                }
                ImGui::EndTabItem();
            }
            if (ImGui::BeginTabItem("Settings")) {
                if(ImGui::Checkbox("side by side", &m_scene->sideBySide())) {
                    ResizeWindow();
                }
                Changed(ImGui::Checkbox("lazy update", &m_lazyUpdate) && !m_lazyUpdate);
                Changed(ImGui::Checkbox("no updates", &m_noUpdates));
                if(ImGui::Button("force update")) {
                    m_forceUpdateStyle.set(true);
                }
                ImGui::Checkbox("show reference", &m_showReference);
                ImGui::DragFloat("blend reference", &m_scene->blendReference(), 0.01, 0, 1);
                ImGui::Checkbox("ignore defaults when saving", &Globals::Get()->configSaveIgnoreDefaults());
                if (ImGui::CollapsingHeader("Saves", ImGuiTreeNodeFlags_DefaultOpen)) {
                    if(ImGui::Button("save app config")) {
                        SaveConfig();
                    }
                    if(ImGui::Button("save scene config")) {
                        SaveSceneConfig();
                    }
                    if(ImGui::Button("save all")) {
                        for(Screenshot& screenshot: m_screenshots) {
                            screenshot.ShouldMakeOne();
                        }
                        SaveSceneConfig();
                    }
                    if(ImGui::Button("screenshot input")) {
                        m_inScreenshot.ShouldMakeOne();
                    }
                    if(ImGui::Button("screenshot output")) {
                        m_outScreenshot.ShouldMakeOne();
                    }
                    if(ImGui::Button("screenshot mask")) {
                        m_maskScreenshot.ShouldMakeOne();
                    }
                    if(ImGui::Button("screenshot normals")) {
                        m_normalsScreenshot.ShouldMakeOne();
                    }
                    if(ImGui::Button("screenshot light dir")) {
                        m_lightDirScreenshot.ShouldMakeOne();
                    }
                    if(ImGui::Button("screenshot flat texture")) {
                        m_flatTextureScreenshot.ShouldMakeOne();
                    }
                }
                ImGui::EndTabItem();
            }
            if (ImGui::BeginTabItem("Statiscitcs")) {
                ImGui::Text("style %.3f ms", styleTime);
                ImGui::Text("render scene %.3f ms", renderTime);
                ImGui::Text("display %.3f ms", displayTime);
                ImGui::Text("application frame %.3f ms (%.1f FPS)", 1000.0f * lastIterTime, 1.0f / lastIterTime);

                ImGui::EndTabItem();
            }
            if (ImGui::BeginTabItem("Controls")) {
                ImGui::Text("MOUSE SCROLL - zoom camera in/out");
                ImGui::Text("MOUSE LEFT PRESS - move camera");
                ImGui::Text("MOUSE MIDDLE PRESS - rotate camera");
                ImGui::Text("W - move forwards");
                ImGui::Text("S - move backwards");
                ImGui::Text("A - move left");
                ImGui::Text("D - move right");
                ImGui::Text("SPACE - move up");
                ImGui::Text("LEFT_SHIFT - move down");
                ImGui::Text("Q or ARROW LEFT - rotate left");
                ImGui::Text("E or ARROW RIGHT - rotate right");
                ImGui::Text("ARROW UP - rotate up");
                ImGui::Text("ARROW DOWN - rotate down");
                ImGui::Text("G - show/hide GUI");
                ImGui::Text("L - set lazy update to true");
                ImGui::Text("U - force update");
                ImGui::EndTabItem();
            }
            ImGui::EndTabBar();
        }
        ImGui::EndChild();

        ImGui::SameLine();
    }

    DrawTextures();

    ImGui::End();

    // draw "Show GUI" button
    if(!m_show) {
        const float DISTANCE = 8.0f;
        ImVec2 window_pos = ImVec2(DISTANCE, DISTANCE);
        ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always);
        ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowViewport(viewport->ID);
        ImGuiWindowFlags window_flags = 
            ImGuiWindowFlags_NoDecoration |
            ImGuiWindowFlags_NoDocking |
            ImGuiWindowFlags_AlwaysAutoResize |
            ImGuiWindowFlags_NoSavedSettings |
            ImGuiWindowFlags_NoNav |
            ImGuiWindowFlags_NoMove;
        ImGui::Begin("Overlay show button", nullptr, window_flags);
        if(ImGui::Button("Show GUI")) {
            m_show = true;
            ResizeWindow();
            //ExpandCollapseGUI();
        }
        ImGui::End();
    }

    // draw reference image to right corner
    if(m_showReference && m_scene->stylesReferences().size() > 0) {
        const opt<Texture2D>& maybeStyleReference = m_scene->stylesReferences()[m_scene->style()];
        if(maybeStyleReference.has_value())
        {
            const Texture2D& styleReference = maybeStyleReference.value();

            const float DISTANCE = 8.0f;
            uvec2 computedWindowSize = ComputeWindowSize(m_scene->textureDim(), m_scene->sideBySide(), m_show);
            float divideSize = 5;
            float maxSize = m_scene->textureDim().y / divideSize;
            float yRatio = (float)styleReference.dim().y / (float)styleReference.dim().x;
            ImVec2 textureSize = ImVec2(maxSize, maxSize * yRatio);
            ImVec2 window_pos = ImVec2(computedWindowSize.x - textureSize.x - 3 * DISTANCE + 1, DISTANCE);
            ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always);
            ImGuiViewport* viewport = ImGui::GetMainViewport();
            ImGui::SetNextWindowViewport(viewport->ID);
            ImGuiWindowFlags window_flags = 
                ImGuiWindowFlags_NoDecoration |
                ImGuiWindowFlags_NoDocking |
                ImGuiWindowFlags_AlwaysAutoResize |
                ImGuiWindowFlags_NoSavedSettings |
                ImGuiWindowFlags_NoNav |
                ImGuiWindowFlags_NoMove;
            ImGui::Begin("Overlay show reference image", nullptr, window_flags);
            ImTextureID textureId = reinterpret_cast<ImTextureID>((usize)styleReference.id());
            ImGui::Image(textureId, textureSize, ImVec2(0, 1), ImVec2(1, 0));
            ImGui::End();
        }
    }

    for(Screenshot& screenshot: m_screenshots) {
        screenshot.MaybeMakeOne();
    }
}

void GUI::DrawTextures()
{
    ImGui::BeginChild("Textures", ImVec2(0, 0), false);
    {
        if(m_scene->sideBySide()) {
            Texture2D& selected = m_scene->OutputTypeToTexture(m_scene->inputType());
            ImTextureID inTextureId = reinterpret_cast<ImTextureID>((usize)selected.id());
            ImVec2 inTextureSize = ImVec2(m_scene->textureDim().x, m_scene->textureDim().y);
            ImGui::Image(inTextureId, inTextureSize, ImVec2(0, 1), ImVec2(1, 0));
            ImGui::SameLine();
        }
        Texture2D& selected = m_scene->outputType() == OutputType::STYLE ? m_scene->outDisplayTexture()
            : m_scene->OutputTypeToTexture(m_scene->outputType());
        ImTextureID outTextureId = reinterpret_cast<ImTextureID>((usize)selected.id());
        ImVec2 outTextureSize = ImVec2(m_scene->textureDim().x, m_scene->textureDim().y);
        ImGui::Image(outTextureId, outTextureSize, ImVec2(0, 1), ImVec2(1, 0));
        
    }
    ImGui::EndChild();
}

bool GUI::Changed(bool changed)
{
    if(changed) {
        m_valuesChanged = true;
    }
    return changed;
}

void GUI::ResizeWindow()
{
    uvec2 computedWindowSize = ComputeWindowSize(m_scene->textureDim(), m_scene->sideBySide(), m_show);
    m_window->SetSize(computedWindowSize);
}

void GUI::MakeScreenshot(OutputType outputType)
{
    switch (outputType)
    {
    case OutputType::STYLE:
        m_outScreenshot.JustMakeOne();
        break;
    case OutputType::NORMALS:
        m_normalsScreenshot.JustMakeOne();
        break;
    case OutputType::LIGHT_DIR:
        m_lightDirScreenshot.JustMakeOne();
        break;
    case OutputType::MASK:
        m_maskScreenshot.JustMakeOne();
        break;
    case OutputType::INPUT:
        m_inScreenshot.JustMakeOne();
        break;
    case OutputType::INPUT_DISPLAY:
        m_inDisplayScreenshot.JustMakeOne();
        break;
    
    default:
        break;
    }
}

void GUI::ExpandCollapseGUI()
{
    ivec2 winPos = m_window->GetPos();
    ivec2 offset = ivec2(GetImguiWindowOffset(), 0);
    if(m_show) {
        m_window->SetPos(winPos - offset);
    } else {
        m_window->SetPos(winPos + offset);
    }
}

void GUI::SaveConfig()
{
    string saveName = "config.json";
    spdlog::info("config saved as {}", saveName);
    ConfigVar configVar = m_configVar;
    match(configVar,
        [&](StyleTransferAppImpl::InteractiveConfig& config) {
            config.sceneConfig = m_scene->config();
        },
        [&](const StyleTransferAppImpl::TestConfig& config) {
        }
    );
    ConfigJson::SaveConfig(configVar, saveName);
}
void GUI::SaveSceneConfig()
{
    string saveName = "scene" + std::to_string(m_sceneConfigNum) + ".json";
    spdlog::info("scene config saved as {}", saveName);
    ConfigJson::SaveSceneConfig(m_scene->config(), saveName);
    m_sceneConfigNum++;
}

GUI::Screenshot::Screenshot(const Texture2D& texture, string name)
    : m_shouldMakeOne(false), m_texture(texture), m_name(name), m_screenshotNum(0)
{
}
void GUI::Screenshot::ShouldMakeOne()
{
    m_shouldMakeOne = true;
}
void GUI::Screenshot::MaybeMakeOne()
{
    if(m_shouldMakeOne) {
        m_shouldMakeOne = false;
        JustMakeOne();
    }
}
void GUI::Screenshot::JustMakeOne()
{
    string shotName = m_name + std::to_string(m_screenshotNum) + ".png";
    spdlog::info("screenshot saved as {}", shotName);
    ResourceManager::SaveTexture2D_PNG(m_texture, shotName);
    m_screenshotNum++;
}


void GUI::RenderingHeader()
{
    Changed(ImGui::Checkbox("mask input", &m_scene->maskInput()));
    Changed(ImGui::Checkbox("mask output", &m_scene->maskOutput()));
    Changed(ImGui::ColorEdit4("clear color", (float*)&m_scene->clearColor()));
    
    if (m_scene->stylesNames().size() > 0 &&
        ImGui::BeginCombo("style type", m_scene->stylesNames()[m_scene->style()].c_str()))
    {
        for (uint i = 0; i < m_scene->stylesNames().size(); ++i)
        {
            string styleName = m_scene->stylesNames()[i];
            bool is_selected = i == m_scene->style();
            if (ImGui::Selectable(styleName.c_str(), is_selected)) {
                m_scene->style() = i;
                m_valuesChanged = true;
            }
            if (is_selected) {
                ImGui::SetItemDefaultFocus();
            }
        }
        ImGui::EndCombo();
    }
    if(m_scene->sideBySide()) {
        static const char* items[] = {
            "style", "normals", "ligth dir", "mask", "input", "input displayed", "flat texture", "sphere texture"
        };
        ImGui::Combo("left output", &m_scene->inputTypeUnsafe(), items, IM_ARRAYSIZE(items));
    }
    {
        static const char* items[] = {
            "style", "normals", "ligth dir", "mask", "input", "input displayed", "flat texture", "sphere texture"
        };
        ImGui::Combo(m_scene->sideBySide() ? "right output" : "output type", &m_scene->outputTypeUnsafe(), items, IM_ARRAYSIZE(items));
    }
}

void GUI::CameraHeader()
{
    Changed(ImGui::DragFloat3("position##1", (float*)&m_scene->cameraData().transform.pos, 0.025));
    DragRotationEuler("rotation##1", m_scene->cameraData().transform.rot, 0.025, -360, 360);
    Changed(ImGui::DragFloat("move speed", &m_scene->cameraData().moveSpeed, 0.05, 0, 10000));
    Changed(ImGui::DragFloat("rot speed", &m_scene->cameraData().rotSpeed, 0.05, 0, 10000));
    Changed(ImGui::DragFloat("scroll speed", &m_scene->cameraData().scrollMoveSpeed, 0.05, 0, 10000));
    Changed(ImGui::DragFloat("mouse move", &m_scene->cameraData().mouseMoveSpeed, 0.05, 0, 10000));
    Changed(ImGui::DragFloat("mouse rot", &m_scene->cameraData().mouseRotSpeed, 0.05, 0, 10000));
    DragAngle("fov", m_scene->cameraData().camera.fov, 0.05, 0.01, 180);
    Changed(ImGui::DragFloat("near", &m_scene->cameraData().camera.near, 0.001, 0.00001, 1, "%.5f"));
    Changed(ImGui::DragFloat("far", &m_scene->cameraData().camera.far, 0.05, 0.01, 10000));
}

void GUI::LightHeader()
{
    Changed(ImGui::ColorEdit3("ambient", (float*)&m_scene->envGlobalAmbient()));
    static const char* ligth_types[] = {
        "none", "dir", "point", "spot"
    };
    Changed(ImGui::Combo("light type", &(int&)m_scene->light().lightType, ligth_types, IM_ARRAYSIZE(ligth_types)));
    if(m_scene->light().lightType != LightType::NONE) {
        Changed(ImGui::Checkbox("show", &m_scene->showLight()));
        Changed(ImGui::ColorEdit3("color", (float*)&m_scene->light().color));
        Changed(ImGui::DragFloat3("position##3", (float*)&m_scene->light().position, 0.01));
        Changed(ImGui::DragFloat3("attenuation", (float*)&m_scene->light().attenuationConst, 0.005, 0, 100));
        if(m_scene->light().lightType != LightType::POINT) {
            if(DragRotationEuler("direction", m_scene->lightTransform().rot, 0.25, -180, 180)) {
                m_scene->light().spotDir = mat3(m_scene->lightTransform().GetRotation()) * vec3(0,0,-1);
            }
        }
        if(m_scene->light().lightType == LightType::SPOT) {
            Changed(ImGui::DragFloat("exponent", &m_scene->light().spotExponent, 0.01));
            DragAngle("cutoff", m_scene->light().spotCutoff, 0.25, 0, 90);
        }
    }
}

void GUI::ObjectHeader()
{
    if (ImGui::BeginCombo("mesh type", m_scene->meshesNames()[m_scene->mesh()].c_str()))
    {
        for (uint i = 0; i < m_scene->meshesNames().size(); ++i)
        {
            string meshName = m_scene->meshesNames()[i];
            bool is_selected = i == m_scene->mesh();
            if (ImGui::Selectable(meshName.c_str(), is_selected)) {
                m_scene->mesh() = i;
                m_valuesChanged = true;
            }
            if (is_selected) {
                ImGui::SetItemDefaultFocus();
            }
        }
        ImGui::EndCombo();
    }
    ImGui::Text("Transform");
    Changed(ImGui::DragFloat3("position##2", (float*)&m_scene->objectData().transform.pos, 0.025));
    DragRotationEuler("rotation##2", m_scene->objectData().transform.rot, 0.25, -180, 180);
    Changed(ImGui::DragFloat3("scale", (float*)&m_scene->objectData().transform.scale, 0.025, 0, 100000));
    DragScaleAll("scale all", m_scene->objectData().transform.scale, 0.025, 0, 100000);
    ImGui::Text("Material");
    Changed(ImGui::ColorEdit3("albedo", (float*)&m_scene->material().albedo));
    Changed(ImGui::DragFloat("metallic", &m_scene->material().metallic, 0.01, 0, 1));
    Changed(ImGui::DragFloat("roughness", &m_scene->material().roughness, 0.01, 0, 1));
    Changed(ImGui::DragFloat("ao", &m_scene->material().ao, 0.01, 0, 1));
}

bool GUI::DragRotationEuler(string name, vec3& rot, float speed, float min, float max)
{
    vec3 rotDeg = glm::degrees(rot);
    if(Changed(ImGui::DragFloat3(name.c_str(), (float*)&rotDeg, speed, min, max))) {
        rot = glm::radians(rotDeg);
        return true;
    }
    return false;
}

bool GUI::DragRotationQuat(string name, quat& rot, float speed, float min, float max)
{
    vec3 rotDeg = glm::degrees(glm::eulerAngles(rot));
    if(Changed(ImGui::DragFloat3(name.c_str(), (float*)&rotDeg, speed, min, max))) {
        rot = quat(glm::radians(rotDeg));
        return true;
    }
    return false;
}

bool GUI::DragAngle(string name, float& angle, float speed, float min, float max)
{
    float angeleDeg = glm::degrees(angle);
    if(Changed(ImGui::DragFloat(name.c_str(), &angeleDeg, speed, min, max))) {
        angle = glm::radians(angeleDeg);
        return true;
    }
    return false;
}

bool GUI::DragScaleAll(string name, vec3& scale, float speed, float min, float max)
{
    float oneScale = glm::length(scale) / glm::length(vec3(1));
    float scaleAll = oneScale;
    if(scale != vec3(0)) {
        if(Changed(ImGui::DragFloat(name.c_str(), &scaleAll, speed, min == 0 ? speed : min, max)) && scaleAll != 0) {
            scale *= scaleAll / oneScale;
            return true;
        }
    }
    return false;
}

}}