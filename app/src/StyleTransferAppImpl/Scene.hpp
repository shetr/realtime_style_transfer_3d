#ifndef STYLE_TRANSFER_APP_IMPL_SCENE_HPP
#define STYLE_TRANSFER_APP_IMPL_SCENE_HPP

#include <Graphics/Context.hpp>
#include <Resource/Common.hpp>
#include <Data/ShaderTransform.hpp>
#include <Profile/GraphicsTimer.hpp>

#include "SceneConfig.hpp"

namespace Morph { namespace StyleTransferAppImpl {

enum class AxisType
{
    X, Y, Z
};

struct AxisPart
{
    bool isTop;
    AxisType axisType;
    ref<TransformNode> transNode;
};

struct AxisArrowsTransforms
{

    TransformPosScaleNoRot arrowTopTrans;
    TransformPosScaleNoRot arrowBodyTrans;
    TransformRotEuler xArrowTrans;
    TransformRotEuler yArrowTrans;
    TransformRotEuler zArrowTrans;
    TransformRotEuler axisArrowsTrans;
    // transform nodes
    TransformNode xArrowTopTransNode;
    TransformNode xArrowBodyTransNode;
    TransformNode yArrowTopTransNode;
    TransformNode yArrowBodyTransNode;
    TransformNode zArrowTopTransNode;
    TransformNode zArrowBodyTransNode;
    TransformNode xArrowTransNode;
    TransformNode yArrowTransNode;
    TransformNode zArrowTransNode;
    TransformNode axisArrowsTransNode;
    array<AxisPart, 6> axisParts;

    AxisArrowsTransforms();
};

enum class DirLightPartType
{
    TOP, BODY, SPHERE
};

struct DirLightPart
{
    DirLightPartType partType;
    ref<TransformNode> transNode;
};

struct DirLightTransforms
{
    TransformPosScaleNoRot topTrans;
    TransformPosScaleNoRot bodyTrans;
    TransformPosScaleNoRot sphereTrans;
    TransformRotEuler groupTrans;
    TransformRotEuler dirTrans;
    // transform nodes
    TransformNode topTransNode;
    TransformNode bodyTransNode;
    TransformNode sphereTransNode;
    TransformNode groupTransNode;
    TransformNode dirTransNode;
    array<DirLightPart, 3> parts;

    DirLightTransforms();
};

// At first it was meant to be just scene related data,
// but it also contains framebuffers, code for rendering to them and some settings of it's behaviour.
class Scene
{
private:
    struct TextureFramebuffer {
        CustomFramebuffer framebuffer;
        Texture2D texture;
        Renderbuffer renderbuffer;

        TextureFramebuffer(GraphicsContext& context, uvec2 dim);
    };
private:
    ref<GraphicsContext> m_context;
    ref<ResourceStorage> m_resStorage;
    ref<CommonResources> m_commonResources;
    // scene data
    SceneConfig m_config;
    bool m_showLight;
    uint m_lightCount = 1;
    MaterialTex m_textures;
    ShaderTransform m_shaderTransform;
    raw<Light> m_rawLights;
    // reference texture
    float m_blendReference = 0.0f;
    // uniforms
    ShaderTransformUniforms m_shaderTransformUniforms;
    PBRForwardUniforms m_pbrForwardUniforms;
    PBRForwardTextureUnits m_pbrForwardTextureUnits;
    Uniform<vec3> m_lightColorUniform;
    TextureUnit m_textureSamplerUnit;
    Uniform<TextureUnit> m_textureSamplerUniform;
    TextureUnit m_maskSamplerUnit;
    Uniform<TextureUnit> m_maskSamplerUniform;
    Uniform<TextureUnit> m_texture1Uniform;
    Uniform<TextureUnit> m_texture2Uniform;
    Uniform<float> m_blendFactorUniform;
    // framebuffers
    ref<DefaultFramebuffer> m_defaultFramebuffer;
    // texture dim
    bool m_sideBySide;
    uvec2 m_textureDim;
    // texture framebuffers
    TextureFramebuffer m_inRaw;
    TextureFramebuffer m_in;
    TextureFramebuffer m_out;
    TextureFramebuffer m_normals;
    TextureFramebuffer m_lightDir;
    TextureFramebuffer m_mask;
    TextureFramebuffer m_inDisplay;
    TextureFramebuffer m_outDisplay;
    TextureFramebuffer m_flatTexture;
    TextureFramebuffer m_sphereTexture;
    // meshes
    vector<string> m_meshesNames;
    vector<IndexedStaticMesh3D<u32>> m_meshesStorage;
    vector<opt<Texture2D>> m_meshesFlatTextures;
    vector<ref<IndexedStaticMesh3D<u32>>> m_meshes;
    // styles
    vector<string> m_stylesNames;
    vector<string> m_stylesPaths;
    vector<vector<OutputType>> m_stylesInputs;
    vector<opt<Texture2D>> m_stylesRefrences;
    // timers
    GraphicsTimer<5> m_renderTimer;
    GraphicsTimer<5> m_displayTimer;
    // what to show on input
    OutputType m_inputType;
    // what to show on output
    OutputType m_outputType;
    // mask 
    bool m_maskInput;
    bool m_maskOutput;
    // arrow models
    IndexedStaticMesh3D<u32> m_arrowTop;
    IndexedStaticMesh3D<u32> m_arrowBody;
    IndexedStaticMesh3D<u32> m_arrowSphere;
    // axis rotation visualization
    AxisArrowsTransforms m_axisArrows;
    vec3 m_axisColor = vec3(0);
    Uniform<vec3> m_axisColorUniform;
    float m_axisArrowsScreenSpace;
    uint m_axisArrowsMaxScreenSpace;
    // dir light visualization
    DirLightTransforms m_lightTrans;
public:
    Scene(
        GraphicsContext& context,
        ResourceStorage& resStorage,
        CommonResources& commonResources,
        const SceneConfig& config,
        const vector<MeshData>& meshes,
        const vector<StyleData>& styles,
        uvec2 renderSize,
        bool showLight,
        bool sideBySide,
        bool maskInput,
        bool maskOutput,
        OutputType outputType
    );
    void RenderInputs();
    void RenderOutput();

    inline bool& showLight() { return m_showLight; }

    inline CameraData& cameraData() { return m_config.cameraData; }
    inline ObjectData& objectData() { return m_config.objectData; }
    inline Light& light() { return m_config.light; }
    inline Material& material() { return m_config.material; }
    inline vec3& envGlobalAmbient() { return m_config.envGlobalAmbient; }
    inline vec4& clearColor() { return m_config.clearColor; }
    inline uint& mesh() { return m_config.mesh; }
    inline uint& style() { return m_config.style; }
    inline string styleName() { return m_stylesNames[m_config.style]; }
    inline ShaderTransform& shaderTransform() { return m_shaderTransform; }

    inline const vector<string>& meshesNames() const { return m_meshesNames; }
    inline const vector<string>& stylesNames() const { return m_stylesNames; }
    inline const vector<string>& stylesPaths() const { return m_stylesPaths; }
    inline const vector<vector<OutputType>>& stylesInputs() const { return m_stylesInputs; }
    inline const vector<opt<Texture2D>>& stylesReferences() const { return m_stylesRefrences; }

    inline void SetConfig(const SceneConfig& config) { m_config = config; }
    inline const SceneConfig& config() const { return m_config; }

    inline TextureUnit& textureSamplerUnit() { return m_textureSamplerUnit; }
    inline Uniform<TextureUnit>& textureSamplerUniform() { return m_textureSamplerUniform; }

    inline bool& sideBySide() { return m_sideBySide; }
    inline uvec2 textureDim() { return m_textureDim;}

    inline Texture2D& inRawTexture() { return m_inRaw.texture; }
    inline Texture2D& inTexture() { return m_in.texture; }
    inline Texture2D& outTexture() { return m_out.texture; }
    inline Texture2D& normalsTexture() { return m_normals.texture; }
    inline Texture2D& lightDirTexture() { return m_lightDir.texture; }
    inline Texture2D& maskTexture() { return m_mask.texture; }
    inline Texture2D& inDisplayTexture() { return m_inDisplay.texture; }
    inline Texture2D& outDisplayTexture() { return m_outDisplay.texture; }
    inline Texture2D& flatTexture() { return m_flatTexture.texture; }

    inline DefaultFramebuffer& defaultFramebuffer() { return m_defaultFramebuffer; }
    inline CustomFramebuffer& outFramebuffer() { return m_out.framebuffer; }

    inline GraphicsTimer<5>& renderTimer() { return m_renderTimer; }
    inline GraphicsTimer<5>& displayTimer() { return m_displayTimer; }

    inline OutputType& inputType() { return m_inputType; }
    inline OutputType& outputType() { return m_outputType; }
    inline int& inputTypeUnsafe() { return reinterpret_cast<int&>(m_inputType); }
    inline int& outputTypeUnsafe() { return reinterpret_cast<int&>(m_outputType); }
    inline bool& maskInput() { return m_maskInput; }
    inline bool& maskOutput() { return m_maskOutput; }

    inline float& blendReference() { return m_blendReference; }

    inline TransformRotEuler& lightTransform() { return m_lightTrans.dirTrans; }

    float aspectRatio() const;
    
    Texture2D& OutputTypeToTexture(OutputType outputType);
private:
    void DisplayTexture(Framebuffer& outFramebuffer, Texture& inTexture, bool mask);
    
    void Render(TextureFramebuffer& textureFramebuffer, RenderProgram& program);
    void RenderFlatTexture();
    void RenderSphereTexture();

    Texture2D& OutputTypeToTexture();

    IndexedStaticMesh3D<u32>& GetActiveMesh();

    vector<string> CreateMeshesNames(const vector<MeshData>& meshes);
    vector<IndexedStaticMesh3D<u32>> CreateMeshesStorage(const vector<MeshData>& meshes);
    vector<opt<Texture2D>> CreateMeshesFlatTextures(const vector<MeshData>& meshes);
    vector<string> CreateStylesNames(const vector<StyleData>& styles);
    vector<string> CreateStylesPaths(const vector<StyleData>& styles);
    vector<vector<OutputType>> CreateStylesInputs(const vector<StyleData>& styles);
    vector<opt<Texture2D>> CreateStylesReferences(const vector<StyleData>& styles);

    vector<ref<IndexedStaticMesh3D<u32>>> CreateMeshes();

    vec3 LightDirToEulerRot(vec3 lightDir);
};  

}}


#endif // STYLE_TRANSFER_APP_IMPL_SCENE_HPP