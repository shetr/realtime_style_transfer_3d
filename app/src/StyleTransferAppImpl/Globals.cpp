#include "Globals.hpp"

namespace Morph { namespace StyleTransferAppImpl {

Globals* Globals::s_instance = nullptr;

void Globals::Init(const ResourceStorage& resStorage)
{
    if(s_instance != nullptr) {
        Drop();
    }
    s_instance = new Globals(resStorage);
}
void Globals::Drop()
{
    delete s_instance;
    s_instance = nullptr;
}

Globals* Globals::Get()
{
    return s_instance;
}

Globals::Globals(const ResourceStorage& resStorage)
    : m_resStorage(resStorage)
{
}

}}