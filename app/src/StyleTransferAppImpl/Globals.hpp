#ifndef STYLE_TRANSFER_APP_IMPL_GLOBALS_HPP
#define STYLE_TRANSFER_APP_IMPL_GLOBALS_HPP

#include <Resource/Storage.hpp>

namespace Morph { namespace StyleTransferAppImpl {

// Making some things global for necessary evil.
class Globals
{
    static Globals* s_instance;
    ResourceStorage m_resStorage;
    bool m_configSaveIgnoreDefaults = true;
public:
    static void Init(const ResourceStorage& resStorage);
    static void Drop();
    static Globals* Get();
    inline ResourceStorage& resStorage() { return m_resStorage; }
    inline const ResourceStorage& resStorage() const { return m_resStorage; }
    inline bool& configSaveIgnoreDefaults() { return m_configSaveIgnoreDefaults; }
private:
    Globals(const ResourceStorage& resStorage);
};

}}

#endif // STYLE_TRANSFER_APP_IMPL_GLOBALS_HPP