#include "ConfigJsonImpl.hpp"

#include "Globals.hpp"

#include <exception>


namespace Morph { namespace StyleTransferAppImpl {

#define FROM_JSON_ATTRIBUTE(jsonObj, obj, attr) \
    if(jsonObj.contains(#attr)) \
        jsonObj[#attr].get_to(obj.attr)

#define TO_JSON_ATTRIBUTE(jsonObj, obj, defObj, attr) \
    if(!(StyleTransferAppImpl::Globals::Get()->configSaveIgnoreDefaults() && obj.attr == defObj.attr)) \
        jsonObj[#attr] = obj.attr

template<typename T>
void set_opt(const json& jsonObj, opt<T>& toSet, string at)
{
    T outValue;
    jsonObj[at].get_to(outValue);
    toSet = outValue;
}

#define FROM_JSON_ATTRIBUTE_OPT(jsonObj, obj, attr) \
    if(jsonObj.contains(#attr)) \
        set_opt(jsonObj, obj.attr, #attr)

#define TO_JSON_ATTRIBUTE_OPT(jsonObj, obj, attr) \
    if(obj.attr.has_value()) \
        jsonObj[#attr] = obj.attr.value()


#define TO_JSON_ATTRIBUTE_CMP(jsonObj, obj, defObj, attr) \
    if(!(StyleTransferAppImpl::Globals::Get()->configSaveIgnoreDefaults() && is_equal(obj.attr, defObj.attr))) \
        jsonObj[#attr] = obj.attr

void to_json(json& configJson, const InteractiveConfig& config)
{
    InteractiveConfig defConfig;
    TO_JSON_ATTRIBUTE(configJson, config, defConfig, renderSize);
    TO_JSON_ATTRIBUTE(configJson, config, defConfig, title);
    configJson["meshes"] = config.meshes;
    configJson["styles"] = config.styles;
    TO_JSON_ATTRIBUTE(configJson, config, defConfig, maskInput);
    TO_JSON_ATTRIBUTE(configJson, config, defConfig, maskOutput);
    TO_JSON_ATTRIBUTE(configJson, config, defConfig, logLevel);
    // interactive specific
    TO_JSON_ATTRIBUTE(configJson, config, defConfig, sideBySide);
    TO_JSON_ATTRIBUTE(configJson, config, defConfig, showGUI);
    TO_JSON_ATTRIBUTE(configJson, config, defConfig, showLight);
    TO_JSON_ATTRIBUTE(configJson, config, defConfig, lazyUpdate);
    TO_JSON_ATTRIBUTE(configJson, config, defConfig, showReference);
    TO_JSON_ATTRIBUTE(configJson, config, defConfig, outputType);
    TO_JSON_ATTRIBUTE_CMP(configJson, config, defConfig, sceneConfig);
}
void from_json(const json& configJson, InteractiveConfig& config)
{
    FROM_JSON_ATTRIBUTE(configJson, config, renderSize);
    FROM_JSON_ATTRIBUTE(configJson, config, title);
    FROM_JSON_ATTRIBUTE(configJson, config, meshes);
    FROM_JSON_ATTRIBUTE(configJson, config, styles);
    FROM_JSON_ATTRIBUTE(configJson, config, maskInput);
    FROM_JSON_ATTRIBUTE(configJson, config, maskOutput);
    FROM_JSON_ATTRIBUTE(configJson, config, logLevel);
    // interactive specific
    FROM_JSON_ATTRIBUTE(configJson, config, sideBySide);
    FROM_JSON_ATTRIBUTE(configJson, config, showGUI);
    FROM_JSON_ATTRIBUTE(configJson, config, showLight);
    FROM_JSON_ATTRIBUTE(configJson, config, lazyUpdate);
    FROM_JSON_ATTRIBUTE(configJson, config, showReference);
    FROM_JSON_ATTRIBUTE(configJson, config, outputType);
    FROM_JSON_ATTRIBUTE(configJson, config, sceneConfig);
}

void to_json(json& configJson, const TestConfig& config)
{
    TestConfig defConfig;
    TO_JSON_ATTRIBUTE(configJson, config, defConfig, renderSize);
    TO_JSON_ATTRIBUTE(configJson, config, defConfig, title);
    configJson["meshes"] = config.meshes;
    configJson["styles"] = config.styles;
    TO_JSON_ATTRIBUTE(configJson, config, defConfig, maskInput);
    TO_JSON_ATTRIBUTE(configJson, config, defConfig, maskOutput);
    TO_JSON_ATTRIBUTE(configJson, config, defConfig, logLevel);
    // test specific
    configJson["sceneConfigs"] = config.sceneConfigs;
    configJson["outputs"] = config.outputs;
}
void from_json(const json& configJson, TestConfig& config)
{
    FROM_JSON_ATTRIBUTE(configJson, config, renderSize);
    FROM_JSON_ATTRIBUTE(configJson, config, title);
    FROM_JSON_ATTRIBUTE(configJson, config, meshes);
    FROM_JSON_ATTRIBUTE(configJson, config, styles);
    FROM_JSON_ATTRIBUTE(configJson, config, maskInput);
    FROM_JSON_ATTRIBUTE(configJson, config, maskOutput);
    FROM_JSON_ATTRIBUTE(configJson, config, logLevel);
    // test specific
    FROM_JSON_ATTRIBUTE(configJson, config, sceneConfigs);
    FROM_JSON_ATTRIBUTE(configJson, config, outputs);
}

void to_json(json& sceneConfigJson, const SceneConfig& sceneConfig)
{
    SceneConfig defSceneConfig;
    sceneConfigJson["storageType"] = "HERE";
    TO_JSON_ATTRIBUTE(sceneConfigJson, sceneConfig, defSceneConfig, mesh);
    TO_JSON_ATTRIBUTE(sceneConfigJson, sceneConfig, defSceneConfig, style);
    TO_JSON_ATTRIBUTE_CMP(sceneConfigJson, sceneConfig, defSceneConfig, cameraData);
    TO_JSON_ATTRIBUTE_CMP(sceneConfigJson, sceneConfig, defSceneConfig, objectData);
    TO_JSON_ATTRIBUTE_CMP(sceneConfigJson, sceneConfig, defSceneConfig, light);
    TO_JSON_ATTRIBUTE_CMP(sceneConfigJson, sceneConfig, defSceneConfig, material);
    TO_JSON_ATTRIBUTE(sceneConfigJson, sceneConfig, defSceneConfig, envGlobalAmbient);
    TO_JSON_ATTRIBUTE(sceneConfigJson, sceneConfig, defSceneConfig, clearColor);
}
void from_json(const json& sceneConfigJson, SceneConfig& sceneConfig)
{
    string storageType = sceneConfigJson.at("storageType").get<string>();
    if (storageType == "FILE") {
        string path = sceneConfigJson.at("path").get<string>();
        opt<string> maybeContents = Globals::Get()->resStorage().ReadFileResOrNormal(path);
        if(maybeContents.has_value()) {
            string contents = maybeContents.value();
            json contentsJson = json::parse(contents);
            contentsJson.get_to(sceneConfig);
        }
        else {
            throw ConfigParseException("could not read from path " + path);
        }
    } else if (storageType == "HERE") {
        FROM_JSON_ATTRIBUTE(sceneConfigJson, sceneConfig, mesh);
        FROM_JSON_ATTRIBUTE(sceneConfigJson, sceneConfig, style);
        FROM_JSON_ATTRIBUTE(sceneConfigJson, sceneConfig, cameraData);
        FROM_JSON_ATTRIBUTE(sceneConfigJson, sceneConfig, objectData);
        FROM_JSON_ATTRIBUTE(sceneConfigJson, sceneConfig, light);
        FROM_JSON_ATTRIBUTE(sceneConfigJson, sceneConfig, material);
        FROM_JSON_ATTRIBUTE(sceneConfigJson, sceneConfig, envGlobalAmbient);
        FROM_JSON_ATTRIBUTE(sceneConfigJson, sceneConfig, clearColor);
    } else {
        throw ConfigParseException("incorrect scene config storage type");
    }
}

bool is_equal(SceneConfig o1, SceneConfig o2)
{
    return
        o1.mesh == o2.mesh &&
        o1.style == o2.style &&
        is_equal(o1.cameraData, o2.cameraData)&&
        is_equal(o1.objectData, o2.objectData)&&
        is_equal(o1.light, o2.light)&&
        is_equal(o1.material, o2.material)&&
        o1.envGlobalAmbient == o2.envGlobalAmbient;
}

void to_json(json& j, const StyleData& v)
{
    StyleData d;
    j = json{ {"name", v.name}, {"path", v.path} };
    TO_JSON_ATTRIBUTE(j, v, d, inputs);
    TO_JSON_ATTRIBUTE_OPT(j, v, refrencePath);
}
void from_json(const json& j, StyleData& v)
{
    j["name"].get_to(v.name);
    j["path"].get_to(v.path);
    FROM_JSON_ATTRIBUTE(j, v, inputs);
    if(v.inputs.size() < 1) {
        throw ConfigParseException("Style " + v.name + " needs to have at least one input.");
    }
    FROM_JSON_ATTRIBUTE_OPT(j, v, refrencePath);
}
void to_json(json& j, const MeshData& v)
{
    j = json{ {"name", v.name}, {"path", v.path} };
    TO_JSON_ATTRIBUTE_OPT(j, v, flatTexturePath);
}
void from_json(const json& j, MeshData& v)
{
    j["name"].get_to(v.name);
    j["path"].get_to(v.path);
    FROM_JSON_ATTRIBUTE_OPT(j, v, flatTexturePath);
}
void to_json(json& j, const ObjectData& v)
{
    ObjectData d;
    TO_JSON_ATTRIBUTE_CMP(j, v, d, transform);
}
void from_json(const json& j, ObjectData& v)
{
    FROM_JSON_ATTRIBUTE(j, v, transform);
}
void to_json(json& j, const CameraData& v)
{
    CameraData d;
    TO_JSON_ATTRIBUTE(j, v, d, moveSpeed);
    TO_JSON_ATTRIBUTE(j, v, d, rotSpeed);
    TO_JSON_ATTRIBUTE(j, v, d, scrollMoveSpeed);
    TO_JSON_ATTRIBUTE(j, v, d, mouseMoveSpeed);
    TO_JSON_ATTRIBUTE(j, v, d, mouseRotSpeed);
    TO_JSON_ATTRIBUTE_CMP(j, v, d, camera);
    TO_JSON_ATTRIBUTE_CMP(j, v, d, transform);
}
void from_json(const json& j, CameraData& v)
{
    FROM_JSON_ATTRIBUTE(j, v, moveSpeed);
    FROM_JSON_ATTRIBUTE(j, v, rotSpeed);
    FROM_JSON_ATTRIBUTE(j, v, scrollMoveSpeed);
    FROM_JSON_ATTRIBUTE(j, v, mouseMoveSpeed);
    FROM_JSON_ATTRIBUTE(j, v, mouseRotSpeed);
    FROM_JSON_ATTRIBUTE(j, v, camera);
    FROM_JSON_ATTRIBUTE(j, v, transform);
}

bool is_equal(ObjectData o1, ObjectData o2)
{
    return
    is_equal(o1.transform, o2.transform)
    ;
}
bool is_equal(CameraData o1, CameraData o2)
{
    return
    o1.moveSpeed == o2.moveSpeed &&
    o1.rotSpeed == o2.rotSpeed &&
    o1.scrollMoveSpeed == o2.scrollMoveSpeed &&
    o1.mouseMoveSpeed == o2.mouseMoveSpeed &&
    o1.mouseRotSpeed == o2.mouseRotSpeed &&
    is_equal(o1.camera, o2.camera) &&
    is_equal(o1.transform, o2.transform)
    ;
}

}}

namespace Morph {

void to_json(json& j, const PerspectiveCamera3D& v)
{
    PerspectiveCamera3D d;
    TO_JSON_ATTRIBUTE(j, v, d, fov);
    TO_JSON_ATTRIBUTE(j, v, d, near);
    TO_JSON_ATTRIBUTE(j, v, d, far);
}
void from_json(const json& j, PerspectiveCamera3D& v)
{
    FROM_JSON_ATTRIBUTE(j, v, fov);
    FROM_JSON_ATTRIBUTE(j, v, near);
    FROM_JSON_ATTRIBUTE(j, v, far);
}
void to_json(json& j, const TransformRotAngleAxis& v)
{
    TransformRotAngleAxis d;
    TO_JSON_ATTRIBUTE(j, v, d, pos);
    TO_JSON_ATTRIBUTE(j, v, d, scale);
    TO_JSON_ATTRIBUTE(j, v, d, rotAngle);
    TO_JSON_ATTRIBUTE(j, v, d, rotAxis);
}
void to_json(json& j, const TransformRotEuler& v)
{
    TransformRotEuler d;
    TO_JSON_ATTRIBUTE(j, v, d, pos);
    TO_JSON_ATTRIBUTE(j, v, d, scale);
    TO_JSON_ATTRIBUTE(j, v, d, rot);
}
void from_json(const json& j, TransformRotAngleAxis& v)
{
    FROM_JSON_ATTRIBUTE(j, v, pos);
    FROM_JSON_ATTRIBUTE(j, v, scale);
    FROM_JSON_ATTRIBUTE(j, v, rotAngle);
    FROM_JSON_ATTRIBUTE(j, v, rotAxis);
}
void from_json(const json& j, TransformRotEuler& v)
{
    FROM_JSON_ATTRIBUTE(j, v, pos);
    FROM_JSON_ATTRIBUTE(j, v, scale);
    FROM_JSON_ATTRIBUTE(j, v, rot);
}
void to_json(json& j, const Light& v)
{
    Light d;
    TO_JSON_ATTRIBUTE(j, v, d, lightType);
    TO_JSON_ATTRIBUTE(j, v, d, color);
    TO_JSON_ATTRIBUTE(j, v, d, position);
    TO_JSON_ATTRIBUTE(j, v, d, spotDir);
    TO_JSON_ATTRIBUTE(j, v, d, spotExponent);
    TO_JSON_ATTRIBUTE(j, v, d, spotCutoff);
    TO_JSON_ATTRIBUTE(j, v, d, attenuationConst);
    TO_JSON_ATTRIBUTE(j, v, d, attenuationLin);
    TO_JSON_ATTRIBUTE(j, v, d, attenuationQuad);
}
void from_json(const json& j, Light& v)
{
    FROM_JSON_ATTRIBUTE(j, v, lightType);
    FROM_JSON_ATTRIBUTE(j, v, color);
    FROM_JSON_ATTRIBUTE(j, v, position);
    FROM_JSON_ATTRIBUTE(j, v, spotDir);
    FROM_JSON_ATTRIBUTE(j, v, spotExponent);
    FROM_JSON_ATTRIBUTE(j, v, spotCutoff);
    FROM_JSON_ATTRIBUTE(j, v, attenuationConst);
    FROM_JSON_ATTRIBUTE(j, v, attenuationLin);
    FROM_JSON_ATTRIBUTE(j, v, attenuationQuad);
}
void to_json(json& j, const Material& v)
{
    Material d;
    TO_JSON_ATTRIBUTE(j, v, d, albedo);
    TO_JSON_ATTRIBUTE(j, v, d, metallic);
    TO_JSON_ATTRIBUTE(j, v, d, roughness);
    TO_JSON_ATTRIBUTE(j, v, d, ao);
}
void from_json(const json& j, Material& v)
{
    FROM_JSON_ATTRIBUTE(j, v, albedo);
    FROM_JSON_ATTRIBUTE(j, v, metallic);
    FROM_JSON_ATTRIBUTE(j, v, roughness);
    FROM_JSON_ATTRIBUTE(j, v, ao);
}



bool is_equal(PerspectiveCamera3D o1, PerspectiveCamera3D o2)
{
    return
    o1.fov == o2.fov &&
    o1.near == o2.near &&
    o1.far == o2.far
    ;
}
bool is_equal(TransformRotAngleAxis o1, TransformRotAngleAxis o2)
{
    return
    o1.pos == o2.pos &&
    o1.scale == o2.scale &&
    o1.rotAngle == o2.rotAngle &&
    o1.rotAxis == o2.rotAxis
    ;
}
bool is_equal(TransformRotEuler o1, TransformRotEuler o2)
{
    return
    o1.pos == o2.pos &&
    o1.scale == o2.scale &&
    o1.rot == o2.rot
    ;
}
bool is_equal(Light o1, Light o2)
{
    return
    o1.lightType == o2.lightType &&
    o1.color == o2.color &&
    o1.position == o2.position &&
    o1.spotDir == o2.spotDir &&
    o1.spotExponent == o2.spotExponent &&
    o1.spotCutoff == o2.spotCutoff &&
    o1.attenuationConst == o2.attenuationConst &&
    o1.attenuationLin == o2.attenuationLin &&
    o1.attenuationQuad == o2.attenuationQuad
    ;
}
bool is_equal(Material o1, Material o2)
{
    return
    o1.albedo == o2.albedo &&
    o1.metallic == o2.metallic &&
    o1.roughness == o2.roughness &&
    o1.ao == o2.ao
    ;
}

}

namespace glm {

void to_json(json& j, const uvec2& v)
{
    j = json{ {"x", v.x}, {"y", v.y} };
}
void from_json(const json& j, uvec2& v)
{
    j["x"].get_to(v.x);
    j["y"].get_to(v.y);
}
void to_json(json& j, const vec3& v)
{
    j = json{ {"x", v.x}, {"y", v.y}, {"z", v.z} };
}
void from_json(const json& j, vec3& v)
{
    j["x"].get_to(v.x);
    j["y"].get_to(v.y);
    j["z"].get_to(v.z);
}
void to_json(json& j, const vec4& v)
{
    j = json{ {"x", v.x}, {"y", v.y}, {"z", v.z}, {"w", v.w} };
}
void from_json(const json& j, vec4& v)
{
    j["x"].get_to(v.x);
    j["y"].get_to(v.y);
    j["z"].get_to(v.z);
    j["w"].get_to(v.w);
}
void to_json(json& j, const quat& q)
{
    j = json{ {"x", q.x}, {"y", q.y}, {"z", q.z}, {"w", q.w} };
}
void from_json(const json& j, quat& q)
{
    j["x"].get_to(q.x);
    j["y"].get_to(q.y);
    j["z"].get_to(q.z);
    j["w"].get_to(q.w);
}

}