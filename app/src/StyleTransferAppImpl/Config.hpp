#ifndef STYLE_TRANSFER_APP_IMPL_CONFIG_HPP
#define STYLE_TRANSFER_APP_IMPL_CONFIG_HPP

#include "SceneConfig.hpp"

namespace Morph { namespace StyleTransferAppImpl {

struct Config
{
    uvec2 renderSize = uvec2(512,512);
    string title = "style transfer";
    vector<MeshData> meshes;
    vector<StyleData> styles;
    bool maskInput = true;
    bool maskOutput = true;
    spdlog::level::level_enum logLevel = spdlog::level::info;

    virtual bool SideBySide() const = 0;
    virtual bool ShowGUI() const = 0;
    virtual bool ShowLight() const = 0;
    virtual const SceneConfig& FirstScene() const = 0;
    virtual bool IsInteractive() const = 0;
    virtual bool LazyUpdate() const = 0;
    virtual bool ShowReference() const = 0;
    virtual OutputType GetOutputType() const = 0;
};

struct InteractiveConfig : public Config
{
    bool sideBySide = true;
    bool showGUI = false;
    bool showLight = false;
    bool lazyUpdate = true;
    bool showReference = true;
    OutputType outputType = OutputType::STYLE;
    SceneConfig sceneConfig;

    bool SideBySide() const override { return sideBySide; }
    bool ShowGUI() const override { return showGUI; }
    bool ShowLight() const override { return showLight; }
    const SceneConfig& FirstScene() const override { return sceneConfig; }
    bool IsInteractive() const override { return true; }
    bool LazyUpdate() const override { return lazyUpdate; }
    bool ShowReference() const override { return showReference; }
    OutputType GetOutputType() const override { return outputType; }
};

struct TestConfig : public Config
{
    vector<SceneConfig> sceneConfigs; // should have at least one element
    vector<OutputType> outputs;

    bool SideBySide() const override { return false; }
    bool ShowGUI() const override { return false; }
    bool ShowLight() const override { return false; }
    const SceneConfig& FirstScene() const override { return sceneConfigs.front(); }
    bool IsInteractive() const override { return false; }
    bool LazyUpdate() const override { return false; }
    bool ShowReference() const override { return false; }
    OutputType GetOutputType() const override { return OutputType::STYLE; }
};

using ConfigVar = variant<
    InteractiveConfig,
    TestConfig
>;

}}

#endif // STYLE_TRANSFER_APP_IMPL_CONFIG_HPP