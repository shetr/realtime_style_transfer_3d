#ifndef STYLE_TRANSFER_APP_IMPL_GUI_HPP
#define STYLE_TRANSFER_APP_IMPL_GUI_HPP

#include <ImGui/ImGui.hpp>

#include "Scene.hpp"
#include "Config.hpp"

namespace Morph { namespace StyleTransferAppImpl {

class GUI
{
    class Screenshot {
        bool m_shouldMakeOne;
        cref<Texture2D> m_texture;
        string m_name;
        int m_screenshotNum;
    public:
        Screenshot(const Texture2D& texture, string name);
        void ShouldMakeOne();
        void MaybeMakeOne();
        void JustMakeOne();
    };
private:
    cref<ConfigVar> m_configVar;
    ref<GraphicsContext> m_context;
    string m_title;
    bool m_show;
    ref<Window> m_window;
    ref<ImGuiContext> m_imguiContext;
    ref<Scene> m_scene;
    bool m_valuesChanged = false;
    bool m_noUpdates = false;
    bool m_lazyUpdate;
    ref<bool> m_forceUpdateStyle;
    bool m_showReference;
    Screenshot m_inScreenshot;
    Screenshot m_outScreenshot;
    Screenshot m_normalsScreenshot;
    Screenshot m_lightDirScreenshot;
    Screenshot m_maskScreenshot;
    Screenshot m_inDisplayScreenshot;
    Screenshot m_flatTextureScreenshot;
    array<ref<Screenshot>, 7> m_screenshots;
    int m_sceneConfigNum = 0;
public:
    GUI(
        const ConfigVar& configVar,
        GraphicsContext& context,
        string title,
        bool show,
        Window& window,
        ImGuiContext& imguiContext,
        Scene& scene,
        bool lazyUpdate,
        bool& forceUpdateStyle,
        bool showReference
    );

    void Display(float styleTime, float lastIterTime);

    inline bool& show() { return m_show; }
    inline bool& valuesChanged() { return m_valuesChanged; }
    inline bool& noUpdates() { return m_noUpdates; }
    inline bool& lazyUpdate() { return m_lazyUpdate; }

    void ResizeWindow();
    void MakeScreenshot(OutputType outputType);
private:
    void ExpandCollapseGUI();
    void DrawTextures();
    bool Changed(bool changed);
    void SaveConfig();
    void SaveSceneConfig();

    void RenderingHeader();
    void CameraHeader();
    void LightHeader();
    void ObjectHeader();

    bool DragRotationEuler(string name, vec3& rot, float speed = 1, float min = 0, float max = 0);
    bool DragRotationQuat(string name, quat& rot, float speed = 1, float min = 0, float max = 0);
    bool DragAngle(string name, float& angle, float speed = 1, float min = 0, float max = 0);
    bool DragScaleAll(string name, vec3& scale, float speed = 1, float min = 0, float max = 0);
};

}}


#endif // STYLE_TRANSFER_APP_IMPL_GUI_HPP