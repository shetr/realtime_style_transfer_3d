#ifndef STYLE_TRANSFER_APP_IMPL_CONFIG_JSON_HPP
#define STYLE_TRANSFER_APP_IMPL_CONFIG_JSON_HPP

#include "Config.hpp"

#include "Globals.hpp"

namespace Morph { namespace StyleTransferAppImpl {

class SaveConfigError
{
private:
    string m_path;
public:
    SaveConfigError(string path) : m_path(path) {}
    inline friend std::ostream& operator<<(std::ostream& os, const SaveConfigError& error) {
        os << "config error saving file " << error.m_path;
        return os;
    }
};

class LoadConfigError
{
private:
    string m_path;
    string m_message;
public:
    LoadConfigError(string path, string message) : m_path(path), m_message(message) {}
    const string& message() { return m_message; }
    inline friend std::ostream& operator<<(std::ostream& os, const LoadConfigError& error) {
        os << "config error loading file " << error.m_path << ": " << error.m_message;
        return os;
    }
};

class ParseConfigError
{
private:
    string m_message;
public:
    ParseConfigError(string message) : m_message(message) {}
    const string& message() { return m_message; }
    inline friend std::ostream& operator<<(std::ostream& os, const ParseConfigError& error) {
        os << "config parsing error: " << error.m_message;
        return os;
    }
};

class ConfigJson
{
public:

    static inline const ResourceStorage& resStorage() { return Globals::Get()->resStorage(); }

    static Result<Void, SaveConfigError> SaveConfig(const ConfigVar& config, const string& path);
    static Result<Void, SaveConfigError> SaveSceneConfig(const SceneConfig& sceneConfig, const string& path);

    static Result<ConfigVar, LoadConfigError> LoadConfig(const string& path);
    static Result<SceneConfig, LoadConfigError> LoadSceneConfig(const string& path);

    static string ConfigToJSON(const ConfigVar& configVar);
    static string SceneConfigToJSON(const SceneConfig& sceneConfig);

    static Result<ConfigVar, ParseConfigError> ConfigFromJSON(const string& configJsonStr);
    static Result<SceneConfig, ParseConfigError> SceneConfigFromJSON(const string& sceneConfigJsonStr);

private:
    static string InteractiveConfigToJSON(const InteractiveConfig& config);
    static string TestConfigToJSON(const TestConfig& config);

};


}}

#endif // STYLE_TRANSFER_APP_IMPL_CONFIG_JSON_HPP