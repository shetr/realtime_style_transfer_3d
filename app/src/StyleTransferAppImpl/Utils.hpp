#ifndef STYLE_TRANSFER_APP_IMPL_UTILS_HPP
#define STYLE_TRANSFER_APP_IMPL_UTILS_HPP

#include <Core/Core.hpp>

namespace Morph { namespace StyleTransferAppImpl {

uint GetImguiWindowOffset();

uvec2 GetLeftFramebufferOffset(bool showGUI);

uvec2 GetShowGUIButtonSize();

uvec2 ComputeWindowSize(uvec2 renderSize, bool sideBySize, bool showGUI);

}}

#endif // STYLE_TRANSFER_APP_IMPL_UTILS_HPP