#ifndef STYLE_TRANSFER_APP_IMPL_SCENE_CONFIG_HPP
#define STYLE_TRANSFER_APP_IMPL_SCENE_CONFIG_HPP

#include <Data/Transform.hpp>
#include <Data/Camera.hpp>
#include <Data/PBR.hpp>

namespace Morph { namespace StyleTransferAppImpl {


struct CameraData
{
    float moveSpeed = 1;
    float rotSpeed = 1;
    float scrollMoveSpeed = 1.5f;
    float mouseMoveSpeed = 15;
    float mouseRotSpeed = 40;
    PerspectiveCamera3D camera;
    TransformRotEuler transform;
};

struct ObjectData
{
    TransformRotEuler transform;
};

enum class OutputType : int {
    STYLE = 0,
    NORMALS,
    LIGHT_DIR,
    MASK,
    INPUT,
    INPUT_DISPLAY,
    FLAT_TEXTURE,
    SPHERE_TEXTURE
};

struct SceneConfig
{
    uint mesh = 0;
    uint style = 0;
    CameraData cameraData;
    ObjectData objectData;
    Light light;
    Material material;
    vec3 envGlobalAmbient = vec3(0.001);
    vec4 clearColor = vec4(0,0,0,1);
};

enum class StyleAdditionalInput
{
    NONE,
    NORMALS,
    LIGTH_DIR,
    NORMALS_AND_LIGHT_DIR
};

struct StyleData
{
    string name;
    string path;
    vector<OutputType> inputs = { OutputType::INPUT };
    opt<string> refrencePath;
};

struct MeshData
{
    string name;
    string path;
    opt<string> flatTexturePath;
};

}}

#endif // STYLE_TRANSFER_APP_IMPL_SCENE_CONFIG_HPP