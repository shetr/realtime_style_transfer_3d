#include "Scene.hpp"

#include <Resource/ResourceManager.hpp>
#include <Resource/Generated.hpp>
#include <Data/Image.hpp>

namespace Morph { namespace StyleTransferAppImpl {

AxisArrowsTransforms::AxisArrowsTransforms()
    :
    xArrowTopTransNode(arrowTopTrans, xArrowTransNode),
    xArrowBodyTransNode(arrowBodyTrans, xArrowTransNode),
    yArrowTopTransNode(arrowTopTrans, yArrowTransNode),
    yArrowBodyTransNode(arrowBodyTrans, yArrowTransNode),
    zArrowTopTransNode(arrowTopTrans, zArrowTransNode),
    zArrowBodyTransNode(arrowBodyTrans, zArrowTransNode),
    xArrowTransNode(xArrowTrans, axisArrowsTransNode),
    yArrowTransNode(yArrowTrans, axisArrowsTransNode),
    zArrowTransNode(zArrowTrans, axisArrowsTransNode),
    axisArrowsTransNode(axisArrowsTrans),
    axisParts({
        AxisPart({true, AxisType::X, xArrowTopTransNode}),
        AxisPart({false, AxisType::X, xArrowBodyTransNode}),
        AxisPart({true, AxisType::Y, yArrowTopTransNode}),
        AxisPart({false, AxisType::Y, yArrowBodyTransNode}),
        AxisPart({true, AxisType::Z, zArrowTopTransNode}),
        AxisPart({false, AxisType::Z, zArrowBodyTransNode})
    })
{
    arrowTopTrans.pos = vec3(0, 0.7, 0);
    arrowTopTrans.scale = vec3(0.3, 0.2, 0.3);
    arrowBodyTrans.pos = vec3(0, 0.3, 0);
    arrowBodyTrans.scale = vec3(0.1, 0.6, 0.1);
    xArrowTrans.rot = vec3(0, 0, -glm::pi<float>() / 2);
    zArrowTrans.rot = vec3(glm::pi<float>() / 2, 0, 0);
}

DirLightTransforms::DirLightTransforms()
    :
    topTransNode(topTrans, groupTransNode),
    bodyTransNode(bodyTrans, groupTransNode),
    sphereTransNode(sphereTrans, groupTransNode),
    groupTransNode(groupTrans, dirTransNode),
    dirTransNode(dirTrans),
    parts({
        DirLightPart({DirLightPartType::TOP, topTransNode}),
        DirLightPart({DirLightPartType::BODY, bodyTransNode}),
        DirLightPart({DirLightPartType::SPHERE, sphereTransNode})
    })
{
    topTrans.pos = vec3(0, 3.5, 0);
    topTrans.scale = vec3(1, 1, 1);
    bodyTrans.pos = vec3(0, 1.5, 0);
    bodyTrans.scale = vec3(0.4, 3, 0.4);
    sphereTrans.scale = vec3(2);
    groupTrans.scale = vec3(0.03);
    groupTrans.rot = vec3(-glm::pi<float>() / 2, 0, 0);
}


Scene::Scene(
        GraphicsContext& context,
        ResourceStorage& resStorage,
        CommonResources& commonResources,
        const SceneConfig& config,
        const vector<MeshData>& meshes,
        const vector<StyleData>& styles,
        uvec2 renderSize,
        bool showLight,
        bool sideBySide,
        bool maskInput,
        bool maskOutput,
        OutputType outputType)
    : m_context(context),
    m_resStorage(resStorage),
    m_commonResources(commonResources),
    // scene data
    m_config(config),
    m_showLight(showLight),
    m_rawLights(1, &light()),
    // uniforms
    m_shaderTransformUniforms(m_shaderTransform),
    m_pbrForwardUniforms(envGlobalAmbient(), cameraData().transform.pos, material(), m_lightCount, m_rawLights),
    m_pbrForwardTextureUnits(m_textures, m_commonResources->WhiteTexture2D()),
    m_lightColorUniform("u_color", light().color),
    m_textureSamplerUnit(TextureUnit::_0),
    m_textureSamplerUniform("u_textureSampler", m_textureSamplerUnit),
    m_maskSamplerUnit(TextureUnit::_1),
    m_maskSamplerUniform("u_maskSampler", m_maskSamplerUnit),
    m_texture1Uniform("u_texture1Sampler", m_textureSamplerUnit),
    m_texture2Uniform("u_texture2Sampler", m_maskSamplerUnit),
    m_blendFactorUniform("u_blendFactor", m_blendReference),
    // framebuffers
    m_defaultFramebuffer(context.GetDefaultFramebuffer()),
    // texture dim
    m_sideBySide(sideBySide),
    m_textureDim(renderSize),
    // texture framebuffers
    m_inRaw(context, m_textureDim),
    m_in(context, m_textureDim),
    m_out(context, m_textureDim),
    m_normals(context, m_textureDim),
    m_lightDir(context, m_textureDim),
    m_mask(context, m_textureDim),
    m_inDisplay(context, m_textureDim),
    m_outDisplay(context, m_textureDim),
    m_flatTexture(context, m_textureDim),
    m_sphereTexture(context, m_textureDim),
    // meshes
    m_meshesNames(CreateMeshesNames(meshes)),
    m_meshesStorage(CreateMeshesStorage(meshes)),
    m_meshesFlatTextures(CreateMeshesFlatTextures(meshes)),
    m_meshes(CreateMeshes()),
    // styles
    m_stylesNames(CreateStylesNames(styles)),
    m_stylesPaths(CreateStylesPaths(styles)),
    m_stylesInputs(CreateStylesInputs(styles)),
    m_stylesRefrences(CreateStylesReferences(styles)),
    // timers
    m_renderTimer(m_context),
    m_displayTimer(m_context),
    // what to show on input
    m_inputType(OutputType::INPUT_DISPLAY),
    // what to show on output
    m_outputType(outputType),
    // mask
    m_maskInput(maskInput),
    m_maskOutput(maskOutput),
    // arrow models
    m_arrowTop(m_context, GeneratedResources::Cone(10)),
    m_arrowBody(m_context, GeneratedResources::Cylinder(10)),
    m_arrowSphere(m_context, GeneratedResources::Sphere(10)),
    // axis rotation visualization
    m_axisColorUniform("u_color", m_axisColor),
    m_axisArrowsScreenSpace(0.15f),
    m_axisArrowsMaxScreenSpace(76)
{
    m_lightTrans.dirTrans.rot = LightDirToEulerRot(m_config.light.spotDir);
}

float Scene::aspectRatio() const
{
    return (float) m_textureDim.x / (float) m_textureDim.y;
}

void Scene::RenderInputs()
{
    GraphicsQueryScope<GraphicsQueryType::TIME_ELAPSED> timeQueryScope = m_renderTimer.Begin();

    GraphicsSettingsApplier viewportApplier = m_context->ApplySetting(
        ViewportSetting(m_textureDim)
    );
    {
        // udate transform
        mat4 V = cameraData().transform.ToInv();
        mat4 P = cameraData().camera.ToMat(aspectRatio());
        m_shaderTransform.Set(objectData().transform.ToMat(), V, P);
        // render model
        {
            GraphicsSettingsApplier clearColorApplier = m_context->ApplySettings({ClearColorSetting(m_config.clearColor)});
            Render(m_inDisplay, m_commonResources->PBRForwardProgram());
        }
        // render helper textures
        Render(m_normals, m_commonResources->MeshNormalsProgram());
        Render(m_lightDir, m_commonResources->MeshLightDirProgram());
        Render(m_mask, m_commonResources->MeshMaskProgram());
        RenderFlatTexture();
        RenderSphereTexture();
        // prepare as style input, maybe mask
        DisplayTexture(m_in.framebuffer, m_inDisplay.texture, maskInput());
        // copy for save input option
        m_context->CopyFramebuffer(m_inDisplay.framebuffer, m_inRaw.framebuffer, EnumBits<FramebufferBufferType>(FramebufferBufferType::COLOR));
        
        { // draw axis arrows
            GraphicsSettingsApplier viewportApplier = m_context->ApplySetting(
                ViewportSetting(uvec2(std::min((uint)(m_textureDim.y * m_axisArrowsScreenSpace), m_axisArrowsMaxScreenSpace)))
            );
            FramebufferBinder framebufferBinder = m_context->BindFramebuffer(m_inDisplay.framebuffer);
            for(AxisPart& axisPart: m_axisArrows.axisParts) {
                IndexedStaticMesh3D<u32>& mesh = axisPart.isTop ? m_arrowTop : m_arrowBody;
                switch (axisPart.axisType) {
                    case AxisType::X: m_axisColor = vec3(1,0,0); break;
                    case AxisType::Y: m_axisColor = vec3(0,1,0); break;
                    case AxisType::Z: m_axisColor = vec3(0,0,1); break;
                }
                mat4 ortho = glm::ortho(-1, 1, -1, 1, -1, 1);
                m_shaderTransform.Set(axisPart.transNode->worldTransform(), cameraData().transform.GetRotationInv(), ortho);
                RenderProgramBinder programBinder = m_context->BindProgram(m_commonResources->MeshFillProgram());
                m_shaderTransformUniforms.SetUniforms(programBinder);
                m_axisColorUniform.Set(programBinder);
                mesh.Draw(*m_context, framebufferBinder, programBinder);
            }
        }
        
        if(m_showLight && m_config.light.lightType != LightType::NONE) // maybe show light
        {
            FramebufferBinder framebufferBinder = m_context->BindFramebuffer(m_inDisplay.framebuffer);
            for(DirLightPart& lightPart: m_lightTrans.parts) {
                if(lightPart.partType != DirLightPartType::SPHERE && m_config.light.lightType == LightType::POINT) {
                    continue;
                }
                IndexedStaticMesh3D<u32>& mesh = lightPart.partType == DirLightPartType::TOP ? m_arrowTop : 
                    (lightPart.partType == DirLightPartType::BODY ? m_arrowBody : m_arrowSphere);
                m_lightTrans.dirTrans.pos = light().position;
                m_shaderTransform.Set(lightPart.transNode->worldTransform(), V, P);
                RenderProgramBinder programBinder = m_context->BindProgram(m_commonResources->MeshFillProgram());
                m_shaderTransformUniforms.SetUniforms(programBinder);
                m_lightColorUniform.Set(programBinder);
                mesh.Draw(*m_context, framebufferBinder, programBinder);
            }
        }
        if(m_blendReference != 0 && m_stylesRefrences.size() > 0)
        {
            opt<Texture2D>& maybeStyleReference = m_stylesRefrences[style()];
            if(maybeStyleReference.has_value()) {
                Texture2D& styleReference = maybeStyleReference.value();
                FramebufferBinder framebufferBinder = m_context->BindFramebuffer(m_inDisplay.framebuffer);
                RenderProgramBinder programBinder = m_context->BindProgram(m_commonResources->BlendProgram());
                TextureUnitBinder texture1UnitBinder = m_context->BindTextureUnit(m_textureSamplerUnit, m_inDisplay.texture);
                TextureUnitBinder texture2UnitBinder = m_context->BindTextureUnit(m_maskSamplerUnit, styleReference);
                m_texture1Uniform.Set(programBinder);
                m_texture2Uniform.Set(programBinder);
                m_blendFactorUniform.Set(programBinder);
                m_commonResources->ScreenQuad2DMesh().Draw(m_context, framebufferBinder, programBinder);
            }
        }
    }
}

void Scene::RenderOutput()
{
    GraphicsSettingsApplier viewportApplier = m_context->ApplySetting(
        ViewportSetting(m_textureDim)
    );
    GraphicsQueryScope<GraphicsQueryType::TIME_ELAPSED> timeQueryScope = m_displayTimer.Begin();
    DisplayTexture(m_outDisplay.framebuffer, OutputTypeToTexture(), maskOutput());
}

void Scene::DisplayTexture(Framebuffer& outFramebuffer, Texture& inTexture, bool mask)
{
    FramebufferBinder framebufferBinder = m_context->BindFramebuffer(outFramebuffer);
    framebufferBinder.Clear();
    if(mask) {
        RenderProgramBinder programBinder = m_context->BindProgram(m_commonResources->ApplyMaskProgram());
        TextureUnitBinder textureUnitBinder = m_context->BindTextureUnit(m_textureSamplerUnit, inTexture);
        TextureUnitBinder maskUnitBinder = m_context->BindTextureUnit(m_maskSamplerUnit, m_mask.texture);
        m_textureSamplerUniform.Set(programBinder);
        m_maskSamplerUniform.Set(programBinder);
        m_commonResources->ScreenQuad2DMesh().Draw(m_context, framebufferBinder, programBinder);
    } else {
        RenderProgramBinder programBinder = m_context->BindProgram(m_commonResources->ScreenFillProgram());
        TextureUnitBinder textureUnitBinder = m_context->BindTextureUnit(m_textureSamplerUnit, inTexture);
        m_textureSamplerUniform.Set(programBinder);
        m_commonResources->ScreenQuad2DMesh().Draw(m_context, framebufferBinder, programBinder);
    }
}

void Scene::Render(TextureFramebuffer& textureFramebuffer, RenderProgram& program)
{
    FramebufferBinder framebufferBinder = m_context->BindFramebuffer(textureFramebuffer.framebuffer);
    framebufferBinder.Clear();
    RenderProgramBinder programBinder = m_context->BindProgram(program);
    m_shaderTransformUniforms.SetUniforms(programBinder);
    m_pbrForwardUniforms.SetUniforms(programBinder);
    TextureUnitsBinder textureUnitsBinder = m_pbrForwardTextureUnits.Bind(m_context, programBinder);
    GetActiveMesh().Draw(m_context, framebufferBinder, programBinder);
}
void Scene::RenderFlatTexture()
{
    FramebufferBinder framebufferBinder = m_context->BindFramebuffer(m_flatTexture.framebuffer);
    framebufferBinder.Clear();
    RenderProgramBinder programBinder = m_context->BindProgram(m_commonResources->MeshTextureProgram());
    m_shaderTransformUniforms.SetUniforms(programBinder);
    opt<Texture2D>& maybeTexture = m_meshesFlatTextures[mesh()];
    Texture2D& texture = maybeTexture.has_value() ? maybeTexture.value() : m_commonResources->WhiteTexture2D();
    TextureUnitBinder textureUnitBinder = m_context->BindTextureUnit(m_textureSamplerUnit, texture);
    m_textureSamplerUniform.Set(programBinder);
    GetActiveMesh().Draw(m_context, framebufferBinder, programBinder);
}
void Scene::RenderSphereTexture()
{
    FramebufferBinder framebufferBinder = m_context->BindFramebuffer(m_sphereTexture.framebuffer);
    framebufferBinder.Clear();
    RenderProgramBinder programBinder = m_context->BindProgram(m_commonResources->SphereTextureProgram());
    m_shaderTransformUniforms.SetUniforms(programBinder);
    opt<Texture2D>& maybeTexture = m_meshesFlatTextures[mesh()];
    Texture2D& texture = maybeTexture.has_value() ? maybeTexture.value() : m_commonResources->WhiteTexture2D();
    TextureUnitBinder textureUnitBinder = m_context->BindTextureUnit(m_textureSamplerUnit, texture);
    m_textureSamplerUniform.Set(programBinder);
    GetActiveMesh().Draw(m_context, framebufferBinder, programBinder);
}


Texture2D& Scene::OutputTypeToTexture(OutputType outputType)
{
    switch (outputType)
    {
    case OutputType::STYLE:
        return m_out.texture;
    case OutputType::NORMALS:
        return m_normals.texture;
    case OutputType::LIGHT_DIR:
        return m_lightDir.texture;
    case OutputType::MASK:
        return m_mask.texture;
    case OutputType::INPUT:
        return m_in.texture;
    case OutputType::INPUT_DISPLAY:
        return m_inDisplay.texture;
    case OutputType::FLAT_TEXTURE:
        return m_flatTexture.texture;
    case OutputType::SPHERE_TEXTURE:
        return m_sphereTexture.texture;
    default:
        return m_out.texture;
    }
}

Texture2D& Scene::OutputTypeToTexture()
{
    return OutputTypeToTexture(m_outputType);
}

IndexedStaticMesh3D<u32>& Scene::GetActiveMesh()
{
    return m_meshes[mesh()];
}

Scene::TextureFramebuffer::TextureFramebuffer(GraphicsContext& context, uvec2 dim)
    : framebuffer(context.CreateFramebuffer()),
    texture(context.CreateTexture2D(dim, TextureSizedFormat::RGBA8,{
            TextureMinFilterSetting(TextureMinFilter::NEAREST),
            TextureMagFilterSetting(TextureMagFilter::NEAREST)
    })),
    renderbuffer(context.CreateRenderbuffer(dim, TextureSizedFormat::DEPTH24_STENCIL8))
{
    framebuffer.Attach(CustomFramebufferAttachment::COLOR_0, texture);
    framebuffer.Attach(CustomFramebufferAttachment::DEPTH_STENCIL, renderbuffer);
}



vector<string> Scene::CreateMeshesNames(const vector<MeshData>& meshes)
{
    vector<string> res(meshes.size());
    std::transform(meshes.begin(), meshes.end(), res.begin(), 
        [](const MeshData& meshData) { return meshData.name; }
    );
    return res;
}
vector<IndexedStaticMesh3D<u32>> Scene::CreateMeshesStorage(const vector<MeshData>& meshes)
{
    vector<IndexedStaticMesh3D<u32>> res;
    res.reserve(meshes.size());
    for(const MeshData& meshData: meshes) {
        const string& meshPath = meshData.path;
        res.push_back(IndexedStaticMesh3D<u32>(m_context,
        value_or_panic(ResourceManager::LoadMesh3D_OBJ(
        value_or_panic(m_resStorage->GetResPathOrNormalPath(meshPath), 
        "resource path {} does not exist", meshPath)), 
        "failed to open file {}", meshPath)));
    }
    return res;
}
vector<opt<Texture2D>> Scene::CreateMeshesFlatTextures(const vector<MeshData>& meshes)
{
    vector<opt<Texture2D>> res(meshes.size());
    std::transform(meshes.begin(), meshes.end(), res.begin(), 
        [&](const MeshData& meshData) {
            opt<Texture2D> res;
            if(meshData.flatTexturePath.has_value()) {
                string path = value_or_panic(
                    m_resStorage->GetResPathOrNormalPath(meshData.flatTexturePath.value()), 
                    "resource path {} does not exist", meshData.flatTexturePath.value()
                );
                Image2D image = value_or_panic(ResourceManager::LoadImage2D_PNG(path), "could not load image at {}", path);
                res = m_context->CreateTexture2D(image.dim, image.data.data(), image.format,{
                        TextureMinFilterSetting(TextureMinFilter::LINEAR),
                        TextureMagFilterSetting(TextureMagFilter::LINEAR)
                });
            }
            return res;
        }
    );
    return res;
}
vector<string> Scene::CreateStylesNames(const vector<StyleData>& styles)
{
    vector<string> res(styles.size());
    std::transform(styles.begin(), styles.end(), res.begin(), 
        [](const StyleData& styleData) { return styleData.name; }
    );
    return res;
}
vector<string> Scene::CreateStylesPaths(const vector<StyleData>& styles)
{
    vector<string> res(styles.size());
    std::transform(styles.begin(), styles.end(), res.begin(),
        [&](const StyleData& styleData) { return value_or_panic(
            m_resStorage->GetResPathOrNormalPath(styleData.path), 
            "resource path {} does not exist", styleData.path
        );}
    );
    return res;
}
vector<vector<OutputType>> Scene::CreateStylesInputs(const vector<StyleData>& styles)
{
    vector<vector<OutputType>> res(styles.size());
    std::transform(styles.begin(), styles.end(), res.begin(), 
        [](const StyleData& styleData) { return styleData.inputs; }
    );
    return res;
}
vector<opt<Texture2D>> Scene::CreateStylesReferences(const vector<StyleData>& styles)
{
    vector<opt<Texture2D>> res(styles.size());
    std::transform(styles.begin(), styles.end(), res.begin(), 
        [&](const StyleData& styleData) {
            opt<Texture2D> res;
            if(styleData.refrencePath.has_value()) {
                string path = value_or_panic(
                    m_resStorage->GetResPathOrNormalPath(styleData.refrencePath.value()), 
                    "resource path {} does not exist", styleData.refrencePath.value()
                );
                Image2D image = value_or_panic(ResourceManager::LoadImage2D_PNG(path), "could not load image at {}", path);
                res = m_context->CreateTexture2D(image.dim, image.data.data(), image.format,{
                        TextureMinFilterSetting(TextureMinFilter::LINEAR),
                        TextureMagFilterSetting(TextureMagFilter::LINEAR)
                });
            }
            return res;
        }
    );
    return res;
}

vector<ref<IndexedStaticMesh3D<u32>>> Scene::CreateMeshes()
{
    vector<ref<IndexedStaticMesh3D<u32>>> res;
    res.reserve(m_meshesStorage.size() + 1);

    for(IndexedStaticMesh3D<u32>& mesh : m_meshesStorage) {
        res.push_back(mesh);
    }
    
    m_meshesNames.push_back("cube");
    m_meshesFlatTextures.push_back({});
    res.push_back(m_commonResources->CubeMesh());
    return res;
}

vec3 Scene::LightDirToEulerRot(vec3 lightDir)
{
    lightDir = glm::rotate(glm::rotation(vec3(0,0,-1), vec3(1,0,0)), lightDir);
    vec2 xz = vec2(lightDir.x, lightDir.z);
    vec2 ty = vec2(glm::length(xz), lightDir.y);
    vec2 xzDir = glm::normalize(xz);
    vec2 tyDir = glm::normalize(ty);

    float phi = acos(xzDir.x);
    float theta = asin(tyDir.y);

    return vec3(theta, phi, 0);
}

}}