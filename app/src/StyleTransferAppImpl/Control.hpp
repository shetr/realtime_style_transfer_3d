#ifndef STYLE_TRANSFER_APP_IMPL_CONTROL_HPP
#define STYLE_TRANSFER_APP_IMPL_CONTROL_HPP

#include "Scene.hpp"
#include "GUI.hpp"

namespace Morph { namespace StyleTransferAppImpl {

class Control
{
private:
    ref<WindowManager> m_windowManager;
    ref<Window> m_window;
    ref<Scene> m_scene;
    ref<GUI> m_gui;
    ref<bool> m_updateStyle;
    ref<bool> m_forceUpdateStyle;
    bool m_ignoreKeyboardInput;
    dvec2 m_lastCursorPos;
    MethodAttacher<KeyEvent, Control> m_keyEventAttacher;
    MethodAttacher<ScrollEvent, Control> m_scrollEventAttacher;
    MethodAttacher<MouseButtonEvent, Control> m_mouseButtonEventAttacher;
    // control variables
    float m_maxUpRot = glm::pi<float>() / 2.0f;
    float m_maxDownRot = -glm::pi<float>() / 2.0f;
    bool m_isMoving = false;
    float m_moveTimeTolerance = 0;
    bool m_mouseMoving = false;
    bool m_mouseRotating = false;
    float m_scrollOffsetAccumulator = 0;
public:
    Control(
        WindowManager& windowManager,
        Window& window,
        Scene& scene,
        GUI& gui,
        bool& updateStyle,
        bool& forceUpdateStyle,
        bool ignoreKeyboardInput);
    void Update(f64 lastIterTime, f64 lastFrameTime);
private:
    dvec2 GetCursorPosEye();
    dvec2 PosToEyeCoords(dvec2 pos);
    dvec2 KeepCursorInLeftFramebuffer();
    bool IsCusorInsideLeftFramebuffer();
    bool IsCusorInsideShowGUIButton();
    void OnKeyEvent(const KeyEvent& event);
    void OnScrollEvent(const ScrollEvent& event);
    void OnMouseButtonEvent(const MouseButtonEvent& event);
};

}}


#endif // STYLE_TRANSFER_APP_IMPL_CONTROL_HPP