#include "StyleTransferApp.hpp"

#include "StyleTransferAppImpl/Utils.hpp"

namespace Morph {

StyleTransferApp::StyleTransferApp(const StyleTransferAppArgs& appArgs)
    : WindowApp(GetWindowConfig(appArgs.config)),
    m_configVar(appArgs.config),
    m_config(UnVar(appArgs.config)),
    d_setLogLevel(MORPH_VOID(spdlog::set_level(m_config->logLevel))),
    m_imguiContext(window()),
    m_graphicsSettingsApplier(ApplyGraphicsSettings()),
    d_initRender(MORPH_VOID(InitRender())),
    m_progCompiler(context(), resStorage()),
    m_commonResources(context(), resStorage(), m_progCompiler),
    m_updateStyle(true),
    m_forceUpdateStyle(false),
    m_scene(
        context(),
        resStorage(),
        m_commonResources,
        m_config->FirstScene(),
        m_config->meshes,
        m_config->styles,
        m_config->renderSize,
        m_config->ShowLight(),
        m_config->SideBySide(),
        m_config->maskInput,
        m_config->maskOutput,
        m_config->GetOutputType()
    ),
    m_gui(
        m_configVar,
        context(),
        m_config->title,
        m_config->ShowGUI(),
        window(),
        imguiContext(),
        m_scene,
        m_config->LazyUpdate(),
        m_forceUpdateStyle,
        m_config->ShowReference()
    ),
    m_control(
        windowManager(),
        window(),
        m_scene,
        m_gui,
        m_updateStyle,
        m_forceUpdateStyle,
        !m_config->IsInteractive()
    )
{
}

StyleTransferApp::~StyleTransferApp() {
    StyleTransferAppImpl::Globals::Drop();
}

void StyleTransferApp::ApplyStyle()
{
    // just copies from inTexture to outTexture
    GraphicsSettingsApplier viewportApplier = context().ApplySetting(
        ViewportSetting(m_scene.outTexture().dim())
    );
    FramebufferBinder framebufferBinder = context().BindFramebuffer(m_scene.outFramebuffer());
    framebufferBinder.Clear();
    RenderProgramBinder programBinder = context().BindProgram(m_commonResources.ScreenFillProgram());
    TextureUnitBinder textureUnitBinder = context().BindTextureUnit(
        m_scene.textureSamplerUnit(),
        m_scene.inTexture()
    );
    m_scene.textureSamplerUniform().Set(programBinder);
    m_commonResources.ScreenQuad2DMesh().Draw(context(), framebufferBinder, programBinder);
}

float StyleTransferApp::ApplyStyleTime()
{
    return 0;
}

void StyleTransferApp::RunIter(f64 lastIterTime, f64 lastFrameTime)
{
    u64 frameNumber = GetNumberOfFramesExecuted();
    if(frameNumber == 0) {
        spdlog::info("initialization finished");
    }
    match(m_configVar,
        [&](const StyleTransferAppImpl::TestConfig& config) {
            m_scene.SetConfig(config.sceneConfigs[frameNumber]);
        },
        [&](const auto& other) {}
    );
    m_control.Update(lastIterTime, lastFrameTime);
    GraphicsSettingsApplier viewportApplier = context().ApplySetting(
        ViewportSetting(context().GetDefaultFramebuffer().dim())
    );
    m_scene.RenderInputs();
    if(((m_updateStyle || !m_gui.lazyUpdate()) && !m_gui.noUpdates()) || m_forceUpdateStyle) {
        ApplyStyle();
        m_scene.RenderOutput();
        m_updateStyle = false;
        m_forceUpdateStyle = false;
    }
    match(m_configVar,
        [&](const StyleTransferAppImpl::InteractiveConfig& config) {
            m_gui.Display(ApplyStyleTime(), lastIterTime);
        },
        [&](const StyleTransferAppImpl::TestConfig& config) {
            for(StyleTransferAppImpl::OutputType outType : config.outputs) {
                m_gui.MakeScreenshot(outType);
            }
        }
    );
}


GraphicsSettingsApplier StyleTransferApp::ApplyGraphicsSettings()
{
    GraphicsSettings graphicsSettings = {
        MultisampleEnabledSetting(true),
        DepthTestEnabledSetting(true),
        CullFaceEnabledSetting(true),
        CullFaceSetting(CullFaceType::BACK),
        FrontFaceSetting(FrontFaceType::CCW),
        ClearColorSetting(0)
    };
    return context().ApplySettings(graphicsSettings);
}

void StyleTransferApp::InitRender()
{
    DefaultFramebuffer& defaultFramebuffer = context().GetDefaultFramebuffer();
    FramebufferBinder framebufferBinder = context().BindFramebuffer(defaultFramebuffer);
    GraphicsSettingsApplier viewportApplier = context().ApplySetting(
        ViewportSetting(defaultFramebuffer.dim())
    );
    framebufferBinder.Clear();
    {
        ImGuiDraw imguiDraw = m_imguiContext.Draw();

        ImGuiStyle& style = ImGui::GetStyle();
        style.WindowRounding = 0;

        ImGuiWindowFlags flags = 
        ImGuiWindowFlags_NoDecoration |
        ImGuiWindowFlags_NoMove |
        ImGuiWindowFlags_NoResize |
        ImGuiWindowFlags_NoSavedSettings |
        ImGuiWindowFlags_NoBringToFrontOnFocus;

        const ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->Pos);
        ImGui::SetNextWindowSize(viewport->Size);

        ImGui::Begin("Init screen", (bool*)0, flags);
        ImGui::Text("Loading...");
        ImGui::End();
    }
    defaultFramebuffer.SwapBuffers();
    windowManager().PollEvents();
}

WindowAppConfig StyleTransferApp::GetWindowConfig(const StyleTransferAppImpl::ConfigVar& appConfig)
{
    WindowAppConfig winConfig;
    match(appConfig,
        [&](const StyleTransferAppImpl::InteractiveConfig& config) {
            winConfig.executionType = ExecutionTypes::AdaptiveVsyncFrames();
        },
        [&](const StyleTransferAppImpl::TestConfig& config) {
            winConfig.executionType = ExecutionTypes::RunNumberOfFrames({(uint)config.sceneConfigs.size()});
            winConfig.hints.Add(WindowBoolHint(WindowBoolSetting::VISIBLE, false));
        }
    );
    const StyleTransferAppImpl::Config& config = UnVar(appConfig);
    winConfig.title = config.title;
    winConfig.size = StyleTransferAppImpl::ComputeWindowSize(config.renderSize, config.SideBySide(), config.ShowGUI());
    winConfig.hints.Add(WindowBoolHint(WindowBoolSetting::RESIZABLE, false));
    return winConfig;
}

const StyleTransferAppImpl::Config& StyleTransferApp::UnVar(const StyleTransferAppImpl::ConfigVar& appConfig)
{
    const StyleTransferAppImpl::Config* ret = nullptr;
    match(appConfig,
        [&](const StyleTransferAppImpl::Config& config) {
            ret = &config;
        }
    );
    return *ret;
}

}